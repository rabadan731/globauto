<?php

namespace backend\components;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Class Controller
 * @package backend\components
 */
class BackendController extends Controller
{
//    /**
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['administrator']
//                    ]
//                ],
//            ]
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ((Yii::$app->user->isGuest || Yii::$app->user->can('administrator')) === false) {
            $this->layout = 'main';
        } else {
            $this->layout =  'main';
        }
        parent::init();
    }
}