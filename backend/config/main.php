<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'main' => [
            'class' => 'modules\main\Module',
            'isBackend' => true
        ],
        'geo' => [
            'class' => 'modules\geo\Module',
            'isBackend' => true
        ],
        'user' => [
            'class' => 'modules\users\Module',
            'isBackend' => true
        ],
        'auto' => [
            'class' => 'modules\auto\Module',
            'isBackend' => true
        ],
        'seo' => [
            'class' => 'modules\seo\Module',
            'isBackend' => true
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/backend',
        ],
        'user' => [
            'identityClass' => 'modules\users\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/login',
            'identityCookie' => [
                'name' => '_identity-globauto',
                'httpOnly' => true,
                'path' => '/',
                'domain' => ".{$params['geo']['host']}",
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => '_session-globauto',
            'cookieParams' => [
                'domain' => ".{$params['geo']['host']}",
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'main/default/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'view' => [
            'class' => 'yii\web\View',
            'theme' => 'themes\inspinia\InspiniaTheme'
        ],
    ],
    'as access' => [
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [
                'allow' => true,
                'actions' => ['login', 'error', 'captcha'],
            ],
            [
                'allow' => true,
                'roles' => ['backend'],
            ],
        ],
    ],
    'params' => $params,
];
