<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'main/default',
    'modules' => [
        'main' => [
            'class' => 'modules\main\Module',
        ],
        'geo' => [
            'class' => 'modules\geo\Module',
        ],
        'user' => [
            'class' => 'modules\users\Module'
        ],
        'auto' => [
            'class' => 'modules\auto\Module'
        ],
    ],
    'components' => [
        'geo' => [
            'class' => 'modules\geo\components\Geo',
            'defaultHost' => $params['geo']['host']
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '',
            'csrfCookie' => [
                'domain' => ".{$params['geo']['host']}",
            ],
        ],
        'user' => [
            'identityClass' => 'modules\users\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/login',
            'identityCookie' => [
                'name' => '_identity-globauto',
                'httpOnly' => true,
                'path' => '/',
                'domain' => ".{$params['geo']['host']}",
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
            ]
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => '_session-globauto',
            'cookieParams' => [
                'domain' => ".{$params['geo']['host']}",
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'main/default/error',
        ],
        'view' => [
            'class' => 'yii\web\View',
            'theme' => 'themes\globauto\GlobAutoTheme'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'params' => $params,
];
