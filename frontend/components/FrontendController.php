<?php

namespace frontend\components;

use Yii;
use modules\seo\models\Seo;
use yii\web\Controller;

/**
 * Class Controller
 * @package frontend\components
 */
class FrontendController extends Controller
{
    public $seo;

    /**
     * @param string $default
     * @param array $replaceArray
     * @return Seo
     */
    public function getSeo($default = '', $replaceArray = [])
    {
        $replaceArray = array_merge([
            'locality' => Yii::$app->geo->title,
            'localityRp' => Yii::$app->geo->titleRp,
            'localityRu' => Yii::$app->geo->titleRu,
        ], $replaceArray);

        return Seo::parse(
            "{$this->module->id}/{$this->id}",
            $this->action->id,
            $replaceArray,
            $default
        );
    }
}
