<?php

return [
  'Add' => 'Добавить',
  'Save' => 'Сохранить',
  'Search' => 'Поиск',
  'Profile' => 'Профиль',
  'Logout' => 'Выход',
  'System' => 'Система',
  'Login' => 'Войти',
  'Forgot your password' => 'Забыли пароль',
  'Registration' => 'Регистрация',
  'Remember me' => 'Запомнить меня',
  'Articles' => 'Статьи',
  'Article Categories' => 'Категории статей',
];