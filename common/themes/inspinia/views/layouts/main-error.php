<?php
use themes\inspinia\assets\InspiniaAsset;

/* @var $this \yii\web\View */
/* @var $content string */

$asset = InspiniaAsset::register($this);

?>

<?php $this->beginContent('@themes/inspinia/views/layouts/base.php'); ?>
<div id="wrapper">

    <div class="gray-bg">
        <?php if (Yii::$app->session->hasFlash('success')) { ?>
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                A wonderful serenity has taken possession.
            </div>
        <?php } ?>

        <div class="row">
            <div class="col-lg-12">
                <?= $content; ?>
            </div>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>