<?php
use themes\inspinia\widgets\Menu;

$menuItems[] = [
    'label' => "GLOB AVTO",
    'url' => ['/'],
    'icon' => 'fa fa-car',
    'options' => [
        'class' => 'nav-header',
    ]
];

if (Yii::$app->user->can("backend")) {


    if (Yii::$app->user->can("auto")) {
        $menuItems[] = [
            'label'=>'Каталог автомобилей',
            'icon' => 'fa fa-car',
            'url' => ['/auto/auto-catalog/index']
        ];
        $menuItems[] = [
            'label'=>'База автомобилей',
            'icon' => 'fa fa-car',
            'items'=> [
                ['label' => 'Марки',                    'url' => ['/auto/car-mark/index']],
                ['label' => 'Модели',                   'url' => ['/auto/car-model/index']],
                ['label' => 'Поколение',                'url' => ['/auto/car-generation/index']],
                ['label' => 'Серия',                    'url' => ['/auto/car-serie/index']],
                ['label' => 'Модификация',              'url' => ['/auto/car-modification/index']],
                ['label' => 'Комплектации',             'url' => ['/auto/car-equipment/index']],
                ['label' => 'Виды характеристик',       'url' => ['/auto/car-characteristic/index']],
                ['label' => 'Значение характеристик',   'url' => ['/auto/car-characteristic-value/index']],
                ['label' => 'Виды опций',               'url' => ['/auto/car-option/index']],
                ['label' => 'Значение опций',           'url' => ['/auto/car-option-value/index']],
                ['label' => 'Операции с БД авто',       'url' => ['/auto/default/index']],
            ],
        ];
    }


    if (Yii::$app->user->can("geo_manage")) {
        \modules\geo\Module::registerTranslations();
        $menuItems[] = [
            'label' => Yii::t("geo", "Geo module"),
            'icon' => 'fa fa-globe',
            'items' => [
                [
                    'icon' => 'fa fa-building',
                    'label' => Yii::t("geo", "Countries"),
                    'url' => ['/geo/countries/index'],
                ],
                [
                    'icon' => 'fa fa-building',
                    'label' => Yii::t("geo", "States"),
                    'url' => ['/geo/states/index'],
                ],
                [
                    'icon' => 'fa fa-building',
                    'label' => Yii::t("geo", "Cities"),
                    'url' => ['/geo/cities/index'],
                ],
            ]
        ];
    }


    $menuItems[] = [
        'label' => Yii::t("app", "Articles"),
        'icon' => 'fa fa-globe',
        'items' => [
            [
                'icon' => 'fa fa-building',
                'label' => Yii::t("app", "Articles"),
                'url' => ['/main/article/index'],
            ],
            [
                'icon' => 'fa fa-building',
                'label' => Yii::t("app", "Article Categories"),
                'url' => ['/main/article-category/index'],
            ],
        ]
    ];


    if (Yii::$app->user->can("seo")) {
        \modules\seo\Module::registerTranslations();
        $menuItems[] = [
            'label' => Yii::t("seo", "Seo module"),
            'icon' => 'fa fa-globe',
            'items' => [
                [
                    'icon' => 'fa fa-building',
                    'label' => Yii::t("seo", "Seo manager"),
                    'url' => ['/seo/seo/index'],
                ],
            ]
        ];
    }


    if (Yii::$app->user->can("userManage") || Yii::$app->user->can("rbacManage")) {
        \modules\users\Module::registerTranslations();
        $menuItems[] = [
            'label' => Yii::t("user", "Users"),
            'icon' => 'fa fa-users',
            'options' => ['class' => 'treeview'],
            'items' => [
                [
                    'icon' => 'fa fa-user',
                    'label' => Yii::t("user", "Users"),
                    'url' => ['/user/user/index'],
                    'visible' => Yii::$app->user->can("userManage")
                ],
                [
                    'icon' => 'fa fa-key',
                    'label' => Yii::t("user", "User roles"),
                    'url' => ['/user/rbac/index'],
                    'visible' => Yii::$app->user->can("rbacManage")
                ],
                [
                    'icon' => 'fa fa-key',
                    'label' => Yii::t("user", "Rules file"),
                    'url' => ['/user/rbac/index-rule-file'],
                    'visible' => Yii::$app->user->can("rbacManage")
                ],
            ],
        ];
    }




    $menuItems[] = [
        'label' => Yii::t("app", "System"),
        'icon' => 'fa fa-gears',
        'items' => [
            [
                'icon' => 'fa fa-file-text-o',
                'label' => Yii::t("app", "Robots.txt"),
                'url' => ['/main/default/robots'],
            ],
        ]
    ];
}

?>


<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <?= Menu::widget([
            'items' => $menuItems,
            'options' => [
                'id' => 'side-menu',
                'class' => 'nav metismenu',
            ]
        ]); ?>
    </div>
</nav>
