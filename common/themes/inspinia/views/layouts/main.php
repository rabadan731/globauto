<?php
use themes\inspinia\assets\InspiniaAsset;

/* @var $this \yii\web\View */
/* @var $content string */

$asset = InspiniaAsset::register($this);

?>

<?php $this->beginContent('@themes/inspinia/views/layouts/base.php'); ?>
<div id="wrapper">
    <?= $this->render("main-nav-sidebar");?>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?= $this->render("main-nav-top", ['asset' => $asset]);?>
        <?= $this->render("main-header");?>

        <?php foreach(Yii::$app->session->getAllFlashes() as $type => $messages): ?>
            <?php foreach($messages as $message): ?>
                <div class="alert alert-<?= $type ?>" role="alert">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?= $message ?>
                </div>
            <?php endforeach ?>
        <?php endforeach ?>

        <div class="row">
            <div class="col-lg-12">
                <?= $content; ?>
                <br />
                <br />
                <?= $this->render("footer");?>
            </div>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>