<div class="row border-bottom white-bg dashboard-header">
    <div class="col-md-12">
        <h2><?=isset(Yii::$app->params['h1'])?Yii::$app->params['h1']:$this->title;?></h2>
        <!-- breadcrumb -->
        <?php echo \yii\widgets\Breadcrumbs::widget([
            'tag'=>'ol',
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <!-- end breadcrumb -->
    </div>
</div>
<br />
