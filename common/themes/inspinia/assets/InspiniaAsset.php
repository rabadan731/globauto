<?php

namespace themes\inspinia\assets;

use yii\web\AssetBundle;

class InspiniaAsset extends AssetBundle
{
    public $sourcePath = '@themes/inspinia/src';

    public $css = [
        "font-awesome/css/font-awesome.css",
        "css/plugins/toastr/toastr.min.css",
        "js/plugins/gritter/jquery.gritter.css",
        "css/animate.css",
        "css/style.css",
        "css/my.css",
    ];

    public $js = [
        "js/plugins/metisMenu/jquery.metisMenu.js",
        "js/plugins/slimscroll/jquery.slimscroll.min.js",
        "js/plugins/flot/jquery.flot.js",
        "js/plugins/flot/jquery.flot.tooltip.min.js",
        "js/plugins/flot/jquery.flot.spline.js",
        "js/plugins/flot/jquery.flot.resize.js",
        "js/plugins/flot/jquery.flot.pie.js",
        "js/plugins/peity/jquery.peity.min.js",
        "js/demo/peity-demo.js",
        "js/inspinia.js",
        "js/plugins/pace/pace.min.js",
        "js/plugins/jquery-ui/jquery-ui.min.js",
        "js/plugins/gritter/jquery.gritter.min.js",
        "js/plugins/sparkline/jquery.sparkline.min.js",
        "js/demo/sparkline-demo.js",
        "js/plugins/chartJs/Chart.min.js",
        "js/plugins/toastr/toastr.min.js",
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
