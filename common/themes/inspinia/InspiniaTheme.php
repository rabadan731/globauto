<?php

namespace themes\inspinia;

use Yii;
use yii\bootstrap\Html;

class InspiniaTheme extends \yii\base\Theme
{
    /**
     * @inheritdoc
     */
    public $pathMap = [
        '@frontend/views' => '@themes/inspinia/views',
        '@backend/views' => '@themes/inspinia/views',
        '@modules' => '@themes/inspinia/modules'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::$container->set('yii\grid\ActionColumn', [
            'buttons' => [
                'ajax-update' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-edit"></i>', "javascript:void(0);", [
                        'class' => 'btn btn-xs btn-primary',
                        'title' => 'Изменить',
                        'data-pjax' => 0,
                        'data-url' => $url,
                        'onclick' => '
                            $.get($(this).data("url"), function(data, status){
                                $("#editBlock").html(data);
                                $("body,html").animate({scrollTop: 186}, 300);
                            });
                        '
                    ]);
                },
                'view' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-eye"></i>', $url, [
                        'class' => 'btn btn-xs btn-success',
                        'title' => 'Просмотр',
                        'data-pjax' => 0,
                    ]);
                },
                'update' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-edit"></i>', $url, [
                        'class' => 'btn btn-xs btn-primary',
                        'title' => 'Изменить',
                        'data-pjax' => 0,
                    ]);
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-trash-o"></i>', $url, [
                        'class' => 'btn btn-xs btn-danger', 'data-method' => 'post',
                        'title' => 'Удалить',
                        'data' => [
                            'pjax' => 0,
                            'confirm' => 'Вы уверены, что хотите удалить этот элемент?'
                        ],
                    ]);
                }
            ],
        ]);

        $bundles = Yii::$app->assetManager->bundles;
        if (is_array($bundles)) {
            Yii::$app->assetManager->bundles = array_merge($bundles, $this->getThemeBundles());
        } else {
            Yii::$app->assetManager->bundles = $this->getThemeBundles();
        }
    }

    public function getThemeBundles()
    {
        $bundles = [];

//        $bundles['yii\web\JqueryAsset'] = [
//            'sourcePath' => '@themes/inspinia/src',
//            'js' => [
//                'js/jquery-3.1.1.min.js'
//            ],
//        ];

        $bundles['yii\bootstrap\BootstrapAsset'] = [
            'sourcePath' => '@themes/inspinia/src',
            'css' => [
                'css/bootstrap.min.css'
            ],
        ];

        $bundles['yii\bootstrap\BootstrapPluginAsset'] = [
            'sourcePath' => '@themes/inspinia/src',
            'js' => [
                'js/bootstrap.min.js'
            ],
            'depends' => [
                'yii\web\JqueryAsset',
                'yii\bootstrap\BootstrapAsset',
            ]
        ];

        return $bundles;
    }
}
