<?php

namespace themes\base;

use Yii;
use yii\helpers\Html;

class Theme extends \yii\base\Theme
{
    /**
     * @inheritdoc
     */
    public $pathMap = [
        '@frontend/views' => '@themes/base/views',
        '@modules' => '@themes/base/modules'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
