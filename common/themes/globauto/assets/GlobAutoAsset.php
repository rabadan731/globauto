<?php

namespace themes\globauto\assets;

use yii\web\AssetBundle;

class GlobAutoAsset extends AssetBundle
{
    public $sourcePath = '@themes/globauto/source';
    public $css = [
        'css/main.css',
        'css/custom.css',
    ];
    public $js = [
        'js/main.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'themes\globauto\assets\plugins\OwlCarousel',
        'themes\globauto\assets\plugins\FancyBoxAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
