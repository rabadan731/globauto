<?php

namespace themes\globauto\assets\plugins;

use yii\web\AssetBundle;

class OwlCarousel extends AssetBundle
{
    /**
     * Unique value to set an empty asset via Yii AssetManager configuration.
     */
    public $sourcePath = '@themes/globauto/assets/plugins/owl-carousel';

    public $js = [
        'owl.carousel.min.js'
    ];
    
    public $css = [
        'owl.carousel.css',
        'owl.theme.css',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

} 
