<?php

namespace themes\globauto\assets\plugins;

use yii\web\AssetBundle;

class FancyBoxAsset extends AssetBundle
{
    /**
     * Unique value to set an empty asset via Yii AssetManager configuration.
     */
    public $sourcePath = '@themes/globauto/assets/plugins/fancybox';

    public $js = [
        'lib/jquery.mousewheel-3.0.6.pack.js',
        'source/jquery.fancybox.pack.js',
        'source/helpers/jquery.fancybox-buttons.js',
        'source/helpers/jquery.fancybox-media.js',
        'source/helpers/jquery.fancybox-thumbs.js',
    ];

    public $css = [
        'source/jquery.fancybox.css',
        'source/helpers/jquery.fancybox-thumbs.css',
        'source/helpers/jquery.fancybox-buttons.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];

} 
