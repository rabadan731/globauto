<?php

namespace themes\globauto;

use Yii;

class GlobAutoTheme extends \yii\base\Theme
{
    /**
     * @inheritdoc
     */
    public $pathMap = [
        '@frontend/views' => '@themes/globauto/views',
        '@modules' => '@themes/globauto/modules'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $bundles = Yii::$app->assetManager->bundles;
        if (is_array($bundles)) {
            Yii::$app->assetManager->bundles = array_merge($bundles, $this->getThemeBundles());
        } else {
            Yii::$app->assetManager->bundles = $this->getThemeBundles();
        }
    }


    public function getThemeBundles()
    {
        $bundles = [];
        $bundles['yii\web\JqueryAsset'] = [
            'sourcePath' => '@themes/globauto/source',
            'js' => [
                'js/jquery-2.1.3.min.js'
            ],
        ];
        return $bundles;
    }
}
