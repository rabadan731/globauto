<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= Yii::$app->urlManager->createUrl(['/']) ?>"><?= Yii::$app->name ?></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right navbar-user">
                <li>
                    <a href="<?= Yii::$app->urlManager->createUrl(['/auto/default/index']) ?>">
                        Характеристики авто
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::$app->urlManager->createUrl(['/company/default/index']) ?>">
                        Авто компании
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::$app->urlManager->createUrl(['/selling/default/index']) ?>">
                        Автообъявления
                    </a>
                </li>
                <?php if (Yii::$app->user->isGuest): ?>

                    <li>
                        <a href="<?= Yii::$app->urlManager->createUrl(['/users/user/login']) ?>"><i
                                class="fa fa-lock"></i> Вход</a>
                    </li>

                <?php else: ?>

                    <li class="dropdown user-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                class="fa fa-user"></i> <?= Yii::$app->user->identity->username ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <?php if (Yii::$app->user->can('administrator')): ?>
                                <li>
                                    <a href="<?= Yii::$app->urlManager->createUrl(['/backend']) ?>"><i
                                            class="fa fa-th-large"></i> Админка</a>
                                </li>
                            <?php endif; ?>
                            <li><a href="<?= Yii::$app->urlManager->createUrl(['/profile']) ?>"><i
                                        class="fa fa-gears"></i> Настройки</a></li>
                            <li class="divider"></li>
                            <li><a href="<?= Yii::$app->urlManager->createUrl(['/user/user/logout']) ?>"><i
                                        class="fa fa-power-off"></i> Выход</a></li>
                        </ul>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <?php if (isset($this->params['pageTitle'])): ?>
                <h1><?= Html::encode($this->params['pageTitle']); ?></h1>
            <?php endif; ?>
            <?= Breadcrumbs::widget([
                'encodeLabels' => false,
                'homeLink' => [
                    'label' => 'Главная',
                    'url' => ['/']
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
            ]) ?>

            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="jarviswidget">
                    <div>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?= Yii::$app->session->getFlash('success') ?>
                        </div>
                    </div>
                </div>
            <?php elseif (Yii::$app->session->hasFlash('error')): ?>
                <div class="jarviswidget">
                    <div>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?= Yii::$app->session->getFlash('error') ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?= $content ?>
        </div>
    </div>
    <!-- /.row -->

</div>
<!-- /.container -->