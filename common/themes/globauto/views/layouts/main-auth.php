<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use modules\users\models\forms\LoginForm;

$model = new LoginForm;

?>

<div class="popup" id="registration">
    <div class="popup_title">АВТОРИЗАЦИЯ</div>
    <?php $form = ActiveForm::begin(['id' => 'login-form', 'action' => '/login']); ?>
    <?= $form->field($model, 'email')
        ->textInput(['class' => 'popup_form_input', 'placeholder' => 'Email'])
        ->label(false)
    ?>
    <?= $form->field($model, 'password')
        ->passwordInput(['class' => 'popup_form_input', 'placeholder' => 'Password'])
        ->label(false)
    ?>
    <?= $form->field($model, 'rememberMe')->checkbox(['class' => 'remember_me_class']) ?>

    <div class="popup_form_link">
        <a href="<?=Url::to(['/user/user/request-password-reset']);?>">
            <?=Yii::t('app', 'Forgot your password');?>
        </a>
        <a href="<?=Url::to(['/user/user/signup']);?>">
            <?=Yii::t('app', 'Registration');?>
        </a>
    </div>


    <?= Html::submitButton(Yii::t('app', 'Login'), [
            'class' => 'popup_form_submit', 'name' => 'login-button']
    ) ?>

    <?php ActiveForm::end(); ?>
</div>


<div class="popup" id="authorization">
     [добавление объявления]
</div>