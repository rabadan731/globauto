<?php
use common\modules\main\models\KeyStorageItem;
/**
 * @var $asset \yii\web\AssetManager
 */
?>
<!--—BEGIN FOOTER -->
<footer class="footer hidden-xs row">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-lg-9">
                <ul class="footer_navigation row">
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Автомобильные Новости</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Продажа авто с пробегом</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Мотоциклы с пробегом</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Справочник автомобилиста</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Водный транспорт</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Новые авто</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Штрафы онлайн</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Автосалоны в России</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Компании в России</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Тест драйвы</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Бу Спецтехника</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Отзывы на авто</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Каталог авто</a>
                    </li>
                    <li class="footer_navigation_item col-sm-6 col-lg-4">
                        <a href="#" class="footer_navigation_item_link">Каталог мото</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-4 col-lg-3">
                <a href="#" class="footer_logotip">
                    <img src="<?= $asset->baseUrl; ?>/img/logotip.png" alt="" class="footer_logotip_img" />
                </a>
                <div class="footer_copyright" itemprop="description" role="alert">
                    <?= KeyStorageItem::getValue(
                        'footer.default',
                        'text',
                        '© Globauto — портал объявлений продажа всех видов наземного транспорта в России.'
                    ); ?>
                </div>
            </div>
        </div>

    </div>
</footer>
<!--—END FOOTER -->
