<?php

use yii\helpers\Html;
use yii\helpers\Url;
/**
 * Created by PhpStorm.
 * User: rabadan731
 * Date: 08.08.2017
 * Time: 22:34
 */
?>
<!--—BEGIN HEADER -->
<header class="header row">
    <div class="container">
        <div class="row pos-l">
            <div class="header_one">
                <a href="<?= Yii::$app->homeUrl; ?>" class="header_logo_link">
                    <img src="<?= $asset->baseUrl; ?>/img/logotip.png" alt="" class="header_logo">
                </a>
                <button class="header_button"></button>
            </div>
            <div class="header_two clearfix">
                <a class="header_city popup_box" href="#city">
                    <span><?= Yii::$app->geo->titleRu; ?></span>
                </a>

                <?php if (Yii::$app->user->isGuest) { ?>
                    <a href="#registration" class="header_sign_in popup_box">Вход/Регистрация</a>
                <?php } else { ?>
                    <?= Html::a(
                        Yii::t('app', 'Profile'),
                        ['/main/default/profile'],
                        ['class' => 'header_sign_in']
                    ); ?>
                <?php } ?>
                <a href="#authorization" class="header_add_announcement popup_box">Добавить объявление</a>
            </div>
            <nav class="header_navigation">
                <a href="/new" class="header_navigation_link"><span>Новые авто</span></a>
                <a href="#" class="header_navigation_link"><span>Объявления</span></a>
                <a href="#" class="header_navigation_link"><span>Компании</span></a>
                <a href="#" class="header_navigation_link"><span>Автосалоны</span></a>
            </nav>
        </div>
    </div>
</header>
<!--—END HEADER -->
