<?php
use yii\helpers\Url;
use modules\geo\widgets\SelectCity;
?>
<div class="popup" id="city">
    <div class="popup_title">ВЫБОР ГОРОДА</div>
    <form class="popup_form">
        <?= SelectCity::widget(); ?>
    </form>
    <ul>
        <li>
            <a href="<?=Url::to(['/geo/default/setregion', 'city' => 'moscow']);?>">
                <strong>Москва</strong>
            </a>
        </li>
        <li>
            <a href="<?=Url::to(['/geo/default/setregion', 'city' => 'spb']);?>">
                <strong>Санкт-Петербург</strong>
            </a>
        </li>
    </ul>
    <ul>
        <li>
            <a href="<?=Url::to(['/geo/default/setregion', 'city' => 'ufa']);?>">Уфа</a>
        </li>
        <li>
            <a href="<?=Url::to(['/geo/default/setregion', 'city' => 'makhachkala']);?>">Махачкала</a>
        </li>
    </ul>
</div>