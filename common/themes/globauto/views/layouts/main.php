<?php

use yii\helpers\Html;
use themes\globauto\assets\GlobAutoAsset;
use yii\widgets\Breadcrumbs;
use modules\geo\widgets\SelectCity;

/* @var $this \yii\web\View */
/* @var $content string */
$asset = GlobAutoAsset::register($this);

$this->params['mainClass'] = isset($this->params['mainClass'])?$this->params['mainClass']:"main row";

$this->params['themeAsset'] = $asset;
Yii::$app->geo->setMetaTags();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <?= Html::csrfMetaTags() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- css3-mediaqueries.js for IE less than 9 -->
    <!-- [if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>
<body>
<?php $this->beginBody(); ?>


<div class="page-wrapper container-fluid">
    <?= $this->render('main-header', ['asset' => $asset]); ?>

    <main class="<?=$this->params['mainClass'];?>">

        <?php $alert = Yii::$app->session->getAllFlashes(); ?>
        <?php if (count($alert)) { ?>s
            <div class="container">
                <?php foreach ($alert as $type => $messages): ?>
                    <?php foreach ($messages as $message): ?>
                        <div class="alert alert-<?= $type ?>" role="alert">
                            <?= $message ?>
                        </div>
                    <?php endforeach ?>
                <?php endforeach ?>
            </div>
        <?php } ?>

        <div class="container">
            <div class="row">
                <!-- breadcrumb -->
                <?php echo \yii\widgets\Breadcrumbs::widget([
                    'options'=> [
                        'class' => 'breadcrumbs hidden-xs col-xs-12',
                        'itemprop' => 'breadcrumb',
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <!-- end breadcrumb -->
            </div>
        </div>
        <?= $content ?>
    </main>

    <?= $this->render('main-footer', ['asset' => $asset]); ?>
</div>

<?= SelectCity::widget(); ?>

<?= $this->render('main-auth', ['asset' => $asset]); ?>

<?php $this->endBody() ?>
<?= Yii::$app->geo->textCounters; ?>
</body>
</html>
<?php $this->endPage() ?>
