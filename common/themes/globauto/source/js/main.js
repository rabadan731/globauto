$(document).ready(function(){
	//кнопка меню в адаптиве
    $(".header_button").click(function () { 
		$(this).toggleClass("header_button_active");
		$(".header_navigation").toggleClass("header_navigation_slide");
    });
    
	// слайдеры
	var facade = $("#owl-facade-news-list");
 
    facade.owlCarousel({
     
        itemsCustom : [
			[0, 1],
			[450, 2],
			[600, 2],
			[700, 3],
			[1000, 3],
			[1200, 4],
			[1400, 4],
			[1600, 5]
        ],
        navigation : true,
    	navigationText : ["",""]
 
  });
    
    
	// всплывалка для картинок и всего)
   $(".popup_box").fancybox({
				 
	   	maxWidth	: 320,
		maxHeight	: '100%',
		minWidth	: 250,
		minHeight	: 330,
		width		: '88%',
		height		: 'auto',
		fitToView	: false,		
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		mouseWheel  : false
   });
	
	   $(".popup_box3").fancybox({
				 
	    maxWidth	: 450,
		maxHeight	: '125%',
		minWidth	: 250,
		minHeight	: '100%',
		width		: '95%',
		height		: 'auto',
		padding: 20,
		fitToView	: false,		
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		mouseWheel  : false
   });
	
	$(".popup_box2").fancybox({
		maxWidth	: '70%',		
		minWidth	: '70%',		
		width		: '70%',
		height		: 'auto',
		fitToView	: false,		
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		mouseWheel  : false
   });
	
	$(".popup_box2").fancybox({
		maxWidth	: '320px',		
		minWidth	: '280px',		
		width		: '100%',
		minHeight	: 893,
		height		: '100%',
		fitToView	: false,		
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		mouseWheel  : false
   });
	
	// меню по якорям
	$('a.smoothScroll[href^="#"], a.smoothScroll[href^="."]').click( function(){ // если в href начинается с # или ., то ловим клик
	    var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
	    $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 1000); // анимируем скроолинг к элементу scroll_el
        }
	    return false; // выключаем стандартное действие
    });
	
	
	//вывод контента на бренде
	$(".brand_content_more").click(function () {
        $(".brand_content_full").slideToggle(1000);              
        var text = $('.brand_content_more').text();
        $('.brand_content_more').text(
        text === "Свернуть" ? "Читать полностью" : "Свернуть");
		return false;
		
    });
	
	//вывод контента на субьекте
	$(".subsection_list_item_more").click(function () {
        $(this).siblings(".subsection_list_item_content_inner").slideToggle(800);              
        var text = $(this).text();
        $(this).text(
        text === "Свернуть" ? "Полная информация" : "Свернуть");
		return false;
		
    });
	
	//слайдер на модели
	var sync1 = $("#sync1");
	var sync2 = $("#sync2");

	  sync1.owlCarousel({
		singleItem : true,
		slideSpeed : 1000,
		navigation: true,
		navigationText : false,
		pagination:false,
		afterAction : syncPosition,
		responsiveRefreshRate : 200,
	  });

	  sync2.owlCarousel({
		 itemsCustom : [
			[0, 0],
			[450, 0],
			[600, 0],
			[700, 4],
			[1000, 5],
			[1200, 7],
			[1400, 7],
			[1600, 7]
        ],		 
		pagination:false,
		responsiveRefreshRate : 100,
		afterInit : function(el){
		  el.find(".owl-item").eq(0).addClass("synced");
		}
	  });

	  function syncPosition(el){
		var current = this.currentItem;
		$("#sync2")
		  .find(".owl-item")
		  .removeClass("synced")
		  .eq(current)
		  .addClass("synced")
		if($("#sync2").data("owlCarousel") !== undefined){
		  center(current)
		}
	  }

	  $("#sync2").on("click", ".owl-item", function(e){
		e.preventDefault();
		var number = $(this).data("owlItem");
		sync1.trigger("owl.goTo",number);
	  });

	  function center(number){
		var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
		var num = number;
		var found = false;
		for(var i in sync2visible){
		  if(num === sync2visible[i]){
			var found = true;
		  }
		}

		if(found===false){
		  if(num>sync2visible[sync2visible.length-1]){
			sync2.trigger("owl.goTo", num - sync2visible.length+2)
		  }else{
			if(num - 1 === -1){
			  num = 0;
			}
			sync2.trigger("owl.goTo", num);
		  }
		} else if(num === sync2visible[sync2visible.length-1]){
		  sync2.trigger("owl.goTo", sync2visible[1])
		} else if(num === sync2visible[0]){
		  sync2.trigger("owl.goTo", num-1)
		}

	  }
	
	
	//слайдер в публикации
	
	$("#owl-publication").owlCarousel({ 
        
        slideSpeed : 300,
        paginationSpeed : 400,
		navigation: true,
		navigationText : false,
		pagination:false,
        singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  });
	
	
	//показать телефон на странице автомобиля
	$('.car_info_phone').click(function(e){
		e.preventDefault();

		var $t = $(this);
		$t.toggleClass('show');

		($t.is('.show'))?$t.text($t.data('phone')):$t.text('Показать телефон')
	});
	
	
	//табы
	
	$('.tabs .tabs>.tabs-box > div:first-child').addClass('active');
	$('.tabs .tabs>.tabs-nav>li:first-child').addClass('active');
	
	$('.tabs-nav li').click(function(e) {
	  var a = $(this),
		  parent = a.parents('.tabs'),
		  nav = parent.children('.tabs-nav').children('li'),
		  box = parent.children('.tabs-box').children('div');

	  if (!a.hasClass('active')) {
		a.addClass('active')
		  .siblings().removeClass('active');

		box.eq(a.index()).addClass('active')
		  .siblings().removeClass('active');
	  }

	  e.preventDefault();
	});
	
	
    $(".ads_list_item_control_more_button").click(function () {
        $(this).next(".control_more_list").toggle();              
        var text = $(this).text();
        $(this).text(
        text === "Свернуть" ? "Еще" : "Свернуть");
		return false; 
    });
	
	$(".control_more_list_item_link").click(function () {
        $(this).parent().parent(".control_more_list").toggle();
		var text = $(this).parent().parent(".control_more_list").prev().text();
        $(this).parent().parent(".control_more_list").prev().text(
        text === "Свернуть" ? "Еще" : "Свернуть");
    });
   
});


