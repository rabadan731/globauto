<?php
/**
 * Created by PhpStorm.
 * User: rabadan
 * Date: 14.08.2017
 * Time: 13:09
 */

namespace common\components;

use Yii;

class Selects
{
    /**
     * @return array
     */
    public static function bollean()
    {
        $items = [];
        $items[null] = Yii::t("common", "-- select --");
        $items[0] = Yii::t("common", "No");
        $items[1] = Yii::t("common", "Yes");

        return $items;
    }
}
