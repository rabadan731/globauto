<?php

namespace common\components\taggable;

use yii\web\AssetBundle;

class TagEditorAsset extends AssetBundle {
    public $sourcePath = '@bower/jquery-tag-editor';
    public $css = [
        'jquery.tag-editor.css'
    ];
    public $js = [
        'jquery.caret.min.js'
    ];
    public $depends = [
        'yii\jui\JuiAsset',
    ];
    public $publishOptions = [
        'except' => [ '*.html', '*.md', '*.json' ]
    ];

    public function init()    {
        parent::init();

        $this->js[] = YII_DEBUG ? 'jquery.tag-editor.js' : 'jquery.tag-editor.min.js';
    }
}