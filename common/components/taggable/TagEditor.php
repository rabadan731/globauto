<?php

namespace common\components\taggable;

use yii\widgets\InputWidget;
use yii\helpers\Html;
use yii\helpers\Json;

class TagEditor extends InputWidget {

    /**
     * @var array
     * The Javascript options of the jQuery tagEditor widget.
     */
    public $tagEditorOptions = [];

    public function run()   {
        $view = $this->getView();

        $asset = new TagEditorAsset();
        $asset->register($view);

        $id = $this->getId();
        $this->options['id'] = $id;

        $teOpts = count($this->tagEditorOptions) ? Json::encode($this->tagEditorOptions) : '';
        $view->registerJs("jQuery('#$id').tagEditor($teOpts);console.log('test');");

        return $this->hasModel() ? Html::activeTextInput($this->model, $this->attribute, $this->options)
            : Html::textInput($this->name, $this->value, $this->options);
    }
}