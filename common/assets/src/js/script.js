$( document ).ready(function() {
    $("body").on('beforeSubmit', 'form#form-ajax', function(e) {
        var form = $(this);
        $.ajax({
            url    : form.attr('action'),
            type   : 'POST',
            data   : form.serialize(),
            success: function (response)
            {
                $("#editBlock").html(response.statusText);
                if (!response.status) {
                    console.log(response.errors);
                }
            },
            error  : function ()
            {
                console.log('internal server error');
            }
        });
        $("body,html").animate({scrollTop: 186}, 300);
        return false;
    });
});