<?php

namespace common\assets;

use yii\web\AssetBundle;

class AjaxFormAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/src';

    public $css = [
    ];

    public $js = [
        "js/script.js",
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
