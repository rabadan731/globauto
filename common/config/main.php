<?php
return [
    'name' => 'GlobAuto',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'sourceLanguage'=>'en-US',
    'language'=>'ru-RU',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'translations' => [
                'app'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@common/messages',
                ],
                '*'=> [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@common/messages',
                    'fileMap'=>[
                        'app'=>'app.php',
                    ],
                ],
            ],
        ],
    ],
    'bootstrap' => [
        'modules\main\Bootstrap',
        'modules\geo\Bootstrap',
        'modules\users\Bootstrap',
        'modules\auto\Bootstrap',
    ],
];
