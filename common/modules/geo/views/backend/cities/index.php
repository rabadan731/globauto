<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\assets\AjaxFormAsset;

/* @var $this yii\web\View */
/* @var $searchModel modules\geo\models\search\GeoCitiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

AjaxFormAsset::register($this);

$this->title = Yii::t('geo', 'Cities');
$this->params['breadcrumbs'][] = ['label' => Yii::t("geo", "Geo module")];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-cities-index">
    <div class="row">
        <div class="col-md-6">
            <p>
                <?= $this->render('_search', ['model' => $searchModel]); ?>
                <?= Html::a(Yii::t('geo', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?php if (!empty($searchModel->stateID)) { ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        'cityID',
                        'cityRuName',
                        'cityDomain',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{ajax-update} {delete}',
                            'options' => ['style' => 'width:70px']
                        ],
                    ],
                ]); ?>
            <?php } else {
                echo Html::tag('p', Yii::t('geo', 'Please select state...'));
            } ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
