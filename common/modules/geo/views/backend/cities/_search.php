<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use modules\geo\models\GeoStates;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\search\GeoCitiesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="geo-cities-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'countryID')->dropDownList(
                $model->getCountriesList(),
                ['prompt' => '-- select --']
            ) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'stateID')->widget(DepDrop::classname(), [
                'data' => ($model->stateID===null?[]:GeoStates::arrayList($model->countryID)),
                'pluginOptions'=>[
                    'depends'=>['geocitiessearch-countryid'],
                    'placeholder' => '-- select --',
                    'url' => Url::to(['list-states'])
                ]
            ]); ?>
        </div>
        <div class="col-md-2">
            <label>&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
