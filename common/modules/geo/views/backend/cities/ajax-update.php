<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoCities */

$this->title = "{$model->cityRuName} <span> #{$model->cityID}</span>";

?>
<div class="geo-cities-update">

    <h3><?= $this->title; ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
