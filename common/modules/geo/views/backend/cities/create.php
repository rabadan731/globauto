<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoCities */

$this->title = Yii::t('geo', 'Create Geo Cities');
$this->params['breadcrumbs'][] = ['label' => Yii::t('geo', 'Geo Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-cities-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
