<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoCities */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('geo', 'Geo Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-cities-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('geo', 'Update'), ['update', 'id' => $model->cityID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('geo', 'Delete'), ['delete', 'id' => $model->cityID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('geo', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cityID',
            'cityName',
            'cityRuName',
            'cityRpName',
            'cityAlias',
            'cityDomain',
            'stateID',
            'countryID',
            'latitude',
            'longitude',
            'oldId',
            'active',
            'title',
            'desc',
            'keyw',
            'text:ntext',
            'footer:ntext',
            'marked',
            'YandexCode',
            'GoogleCode',
            'redirect',
        ],
    ]) ?>

</div>
