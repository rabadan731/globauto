<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoCities */

$this->title = "#{$model->cityID} - {$model->cityDomain}";
$this->params['breadcrumbs'][] = ['label' => Yii::t('geo', 'Geo Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->cityID]];
$this->params['breadcrumbs'][] = Yii::t('geo', 'Update');
?>
<div class="geo-cities-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
