<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoCountries */

$this->title = "{$model->countryRuName} <span> #{$model->countryID}</span>";

?>
<div class="geo-cities-update">

    <h3><?= $this->title; ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
