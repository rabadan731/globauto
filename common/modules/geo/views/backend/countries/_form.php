<?php

use yii\helpers\Html;
use yii\helpers\Url;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoCountries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="geo-countries-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-ajax'
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'countryName')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'countryAlias')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'countryRuName')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'countryRpName')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'countryDomain')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'localName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'webCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'continent')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'latitude')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'longitude')->textInput() ?>
        </div>
    </div>

    <?= $form->field($model, 'surfaceArea')->textInput() ?>

    <?= $form->field($model, 'population')->textInput() ?>

    <?= $form->field($model, 'active')->textInput() ?>

    <?= $form->field($model, 'currencyType')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keyw')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'h1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'footer')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'text')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'imageUpload' => Url::to(['default/image-upload']),
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ]
        ]
    ]); ?>

    <?= $form->field($model, 'langPack')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'langDefault')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'YandexCode')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'GoogleCode')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'textCounter')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'LiCounter')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'GoogleCounter')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('geo', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
