<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\geo\models\GeoCountries;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\search\GeoCountriesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="geo-countries-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'countryID')->dropDownList(GeoCountries::arrayList(), [
                'prompt' => '-- select --'
            ]) ?>
        </div>
        <div class="col-md-6">
            <label>&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('geo', 'Search'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php // echo $form->field($model, 'countryName') ?>

    <?php // echo $form->field($model, 'countryRuName') ?>

    <?php // echo $form->field($model, 'countryRpName') ?>

    <?php // echo $form->field($model, 'countryAlias') ?>

    <?php // echo $form->field($model, 'countryDomain') ?>

    <?php // echo $form->field($model, 'localName') ?>

    <?php // echo $form->field($model, 'webCode') ?>

    <?php // echo $form->field($model, 'region') ?>

    <?php // echo $form->field($model, 'continent') ?>

    <?php // echo $form->field($model, 'latitude') ?>

    <?php // echo $form->field($model, 'longitude') ?>

    <?php // echo $form->field($model, 'surfaceArea') ?>

    <?php // echo $form->field($model, 'population') ?>

    <?php // echo $form->field($model, 'active') ?>

    <?php // echo $form->field($model, 'currencyType') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'desc') ?>

    <?php // echo $form->field($model, 'keyw') ?>

    <?php // echo $form->field($model, 'h1') ?>

    <?php // echo $form->field($model, 'footer') ?>

    <?php // echo $form->field($model, 'text') ?>

    <?php // echo $form->field($model, 'langPack') ?>

    <?php // echo $form->field($model, 'langDefault') ?>

    <?php // echo $form->field($model, 'YandexCode') ?>

    <?php // echo $form->field($model, 'GoogleCode') ?>

    <?php // echo $form->field($model, 'textCounter') ?>

    <?php // echo $form->field($model, 'LiCounter') ?>

    <?php // echo $form->field($model, 'GoogleCounter') ?>

    <?php ActiveForm::end(); ?>

</div>
