<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\assets\AjaxFormAsset;

/* @var $this yii\web\View */
/* @var $searchModel modules\geo\models\search\GeoCountriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

AjaxFormAsset::register($this);

$this->title = Yii::t('geo', 'Countries');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-countries-index">
    <div class="row">
        <div class="col-md-6">
            <p>
                <?= Html::a(Yii::t('geo', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'countryID',
                    'countryRuName',
                    'countryAlias',
                    // 'countryDomain',
                    // 'localName',
                    // 'webCode',
                    // 'region',
                    // 'continent',
                    // 'latitude',
                    // 'longitude',
                    // 'surfaceArea',
                    // 'population',
                    // 'active',
                    // 'currencyType',
                    // 'title',
                    // 'desc',
                    // 'keyw',
                    // 'h1',
                    // 'footer:ntext',
                    // 'text:ntext',
                    // 'langPack',
                    // 'langDefault',
                    // 'YandexCode',
                    // 'GoogleCode',
                    // 'textCounter:ntext',
                    // 'LiCounter:ntext',
                    // 'GoogleCounter:ntext',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{ajax-update} {delete}',
                        'options' => ['style' => 'width:70px']
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
