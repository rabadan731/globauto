<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoCountries */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('geo', 'Geo Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-countries-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('geo', 'Update'), ['update', 'id' => $model->countryID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('geo', 'Delete'), ['delete', 'id' => $model->countryID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('geo', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'countryID',
            'countryName',
            'countryRuName',
            'countryRpName',
            'countryAlias',
            'countryDomain',
            'localName',
            'webCode',
            'region',
            'continent',
            'latitude',
            'longitude',
            'surfaceArea',
            'population',
            'active',
            'currencyType',
            'title',
            'desc',
            'keyw',
            'h1',
            'footer:ntext',
            'text:ntext',
            'langPack',
            'langDefault',
            'YandexCode',
            'GoogleCode',
            'textCounter:ntext',
            'LiCounter:ntext',
            'GoogleCounter:ntext',
        ],
    ]) ?>

</div>
