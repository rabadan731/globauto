<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoCountries */

$this->title = Yii::t('geo', 'Create Geo Countries');
$this->params['breadcrumbs'][] = ['label' => Yii::t('geo', 'Geo Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-countries-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
