<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoCountries */

$this->title = Yii::t('geo', 'Update {modelClass}: ', [
    'modelClass' => 'Geo Countries',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('geo', 'Geo Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->countryID]];
$this->params['breadcrumbs'][] = Yii::t('geo', 'Update');
?>
<div class="geo-countries-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
