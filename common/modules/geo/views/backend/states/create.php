<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoStates */

$this->title = Yii::t('geo', 'Create Geo States');
$this->params['breadcrumbs'][] = ['label' => Yii::t('geo', 'Geo States'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-states-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
