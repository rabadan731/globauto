<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\geo\models\GeoCountries;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\search\GeoStatesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="geo-states-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'countryID')->dropDownList(GeoCountries::arrayList(), [
                'prompt' => '-- select --'
            ]) ?>
        </div>
        <div class="col-md-6">
            <label>&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('geo', 'Search'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php // echo $form->field($model, 'stateID') ?>

    <?php // echo $form->field($model, 'stateName') ?>

    <?php // echo $form->field($model, 'stateRuName') ?>

    <?php // echo $form->field($model, 'stateRpName') ?>

    <?php // echo $form->field($model, 'stateAlias') ?>

    <?php // echo $form->field($model, 'stateDomain') ?>

    <?php // echo $form->field($model, 'countryID') ?>

    <?php // echo $form->field($model, 'latitude') ?>

    <?php // echo $form->field($model, 'longitude') ?>

    <?php // echo $form->field($model, 'oldId') ?>

    <?php // echo $form->field($model, 'active') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'desc') ?>

    <?php // echo $form->field($model, 'keyw') ?>

    <?php // echo $form->field($model, 'text') ?>

    <?php // echo $form->field($model, 'footer') ?>

    <?php // echo $form->field($model, 'YandexCode') ?>

    <?php // echo $form->field($model, 'GoogleCode') ?>

    <?php ActiveForm::end(); ?>

</div>
