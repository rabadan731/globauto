<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\assets\AjaxFormAsset;

/* @var $this yii\web\View */
/* @var $searchModel modules\geo\models\search\GeoStatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

AjaxFormAsset::register($this);

$this->title = Yii::t('geo', 'States');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-states-index">

    <div class="row">
        <div class="col-md-6">
            <p>
                <?= $this->render('_search', ['model' => $searchModel]); ?>
                <?= Html::a(Yii::t('geo', 'Create Geo States'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'stateID',
                    'stateRuName',
                    //'stateAlias',
                    'stateDomain',
                    // 'countryID',
                    // 'latitude',
                    // 'longitude',
                    // 'oldId',
                    // 'active',
                    // 'title',
                    // 'desc',
                    // 'keyw',
                    // 'text:ntext',
                    // 'footer:ntext',
                    // 'YandexCode',
                    // 'GoogleCode',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{ajax-update} {delete}',
                        'options' => ['style' => 'width:70px']
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>


</div>
