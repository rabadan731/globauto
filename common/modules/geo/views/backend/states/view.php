<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoStates */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('geo', 'Geo States'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geo-states-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('geo', 'Update'), ['update', 'id' => $model->stateID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('geo', 'Delete'), ['delete', 'id' => $model->stateID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('geo', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'stateID',
            'stateName',
            'stateRuName',
            'stateRpName',
            'stateAlias',
            'stateDomain',
            'countryID',
            'latitude',
            'longitude',
            'oldId',
            'active',
            'title',
            'desc',
            'keyw',
            'text:ntext',
            'footer:ntext',
            'YandexCode',
            'GoogleCode',
        ],
    ]) ?>

</div>
