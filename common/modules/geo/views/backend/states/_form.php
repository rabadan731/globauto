<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use modules\geo\models\GeoCountries;
use vova07\imperavi\Widget as ImperaviWidget;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoStates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="geo-states-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-ajax'
    ]); ?>

    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-1">Заголовки</a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#tab-2">Разное</a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#tab-3">Текст</a>
            </li>
        </ul>
        <div class="tab-content ">
            <div id="tab-1" class="tab-pane active">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'stateName')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'stateAlias')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'stateRuName')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'stateRpName')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'desc')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'keyw')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                    <?= $form->field($model, 'countryID')->label(Yii::t('geo', 'Country'))->dropDownList(
                        GeoCountries::arrayList(), [
                            'prompt' => '-- select --'
                        ]
                    ) ?>

                    <?= $form->field($model, 'stateDomain')->textInput(['maxlength' => true]) ?>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'latitude')->textInput() ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'longitude')->textInput() ?>
                        </div>
                    </div>

                    <?= $form->field($model, 'oldId')->textInput() ?>

                    <?= $form->field($model, 'active')->textInput() ?>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'YandexCode')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'GoogleCode')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-3" class="tab-pane">
                <div class="panel-body">

                    <?= $form->field($model, 'text')->widget(ImperaviWidget::className(), [
                        'settings' => [
                            'lang' => 'ru',
                            'imageUpload' => Url::to(['default/image-upload']),
                            'minHeight' => 200,
                            'plugins' => [
                                'clips',
                                'fullscreen'
                            ]
                        ]
                    ]); ?>

                    <?= $form->field($model, 'footer')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('geo', 'Save'), ['class'=>'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
