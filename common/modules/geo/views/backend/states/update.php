<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\geo\models\GeoStates */

$this->title = Yii::t('geo', 'Update {modelClass}: ', [
    'modelClass' => 'Geo States',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('geo', 'Geo States'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->stateID]];
$this->params['breadcrumbs'][] = Yii::t('geo', 'Update');
?>
<div class="geo-states-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
