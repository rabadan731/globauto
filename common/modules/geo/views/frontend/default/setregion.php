<?php

use yii\helpers\Url;
use modules\geo\models\GeoStates;
/*
?>

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-md-12">

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title"><?= Yii::$app->geo->title; ?></h1>
                <div class="separator-2"></div>
                <!-- page-title end -->
                <div class="row">
                    <div class="col-md-4">
                        <?php foreach (GeoStates::find()->all() as $region) { ?>
                        <h3><?=$region->title;?></h3>
                        <ul class="list">
                            <?php foreach ($region->getCities()->all() as $city) { ?>
                                <li>
                                    <a href="<?= Url::to(['setregion', 'city' => $city->slug]);?>">
                                        <i class="icon-right-open"></i>
                                        <?= $city->title; ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </div>
                </div>

            </div>
            <!-- main end -->

        </div>
    </div>
</section>
<!-- main-container end -->
*/ ?>