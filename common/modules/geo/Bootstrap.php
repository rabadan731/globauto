<?php

namespace modules\geo;

use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package modules\seo
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->urlManager->addRules(
            [
                'setregion/<city>' => 'geo/default/setregion',
            ],
            false
        );
    }
}
