<?php

use yii\db\Migration;

class m160714_112826_geo extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%geo_countries}}', [
            'countryID' => $this->primaryKey(),
            'countryName' => $this->string(55)->notNull()->unique(),
            'countryRuName' => $this->string(55)->notNull(),
            'countryRpName' => $this->string(55)->notNull(),
            'countryAlias' => $this->string(55)->notNull(),
            'countryDomain' => $this->string(55)->notNull(),
            'localName' => $this->string(55)->notNull(),
            'webCode' => $this->string(2)->notNull()->unique(),
            'region' => $this->string(26)->notNull(),
            'continent' => $this->string(55),
            'latitude' => $this->double()->defaultValue(0),
            'longitude' => $this->double()->defaultValue(0),
            'surfaceArea' => $this->float()->defaultValue(0.00),
            'population' => $this->integer()->notNull()->defaultValue(0),
            'active' => $this->boolean()->notNull()->defaultValue(0),
            'currencyType' => $this->integer(),
            'title' => $this->string(255)->notNull(),
            'desc' => $this->string(512),
            'keyw' => $this->string(255),
            'h1' => $this->string(255),
            'footer' => $this->text(),
            'text' => $this->text(),
            'langPack' => $this->string(255)->notNull(),
            'langDefault' => $this->string(2)->notNull(),
            'YandexCode' => $this->string(255),
            'GoogleCode' => $this->string(255),
            'textCounter' => $this->text(),
            'LiCounter' => $this->text(),
            'GoogleCounter' => $this->text(),
        ], $tableOptions);
        $this->execute(file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'data_countries.sql'));

        $this->createTable('{{%geo_states}}', [
            'stateID' => $this->primaryKey(),
            'stateName' => $this->string(50)->notNull(),
            'stateRuName' => $this->string(50)->notNull(),
            'stateRpName' => $this->string(50)->notNull(),
            'stateAlias' => $this->string(50)->notNull(),
            'stateDomain' => $this->string(50)->notNull(),
            'countryID' => $this->integer()->notNull(),
            'latitude' => $this->double()->defaultValue(0),
            'longitude' => $this->double()->defaultValue(0),
            'oldId' => $this->integer()->notNull(),
            'active' => $this->boolean()->notNull()->defaultValue(0),
            'title' => $this->string(255)->notNull(),
            'desc' => $this->string(512)->notNull(),
            'keyw' => $this->string(255)->notNull(),
            'text' => $this->text(),
            'footer' => $this->text(),
            'YandexCode' => $this->string(255)->notNull(),
            'GoogleCode' => $this->string(255)->notNull(),
        ], $tableOptions);
        $this->execute(file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'data_states.sql'));

        $this->createTable('{{%geo_cities}}', [
            'cityID' => $this->primaryKey(),
            'cityName' => $this->string(50)->notNull(),
            'cityRuName' => $this->string(50)->notNull(),
            'cityRpName' => $this->string(50)->notNull(),
            'cityAlias' => $this->string(50)->notNull(),
            'cityDomain' => $this->string(50)->notNull(),
            'stateID' => $this->integer()->notNull()->defaultValue(0),
            'countryID' => $this->integer()->notNull(),
            'latitude' => $this->double()->defaultValue(0),
            'longitude' => $this->double()->defaultValue(0),
            'oldId' => $this->integer()->notNull(),
            'active' => $this->boolean()->notNull()->defaultValue(0),
            'title' => $this->string(255)->notNull(),
            'desc' => $this->string(512)->notNull(),
            'keyw' => $this->string(255)->notNull(),
            'text' => $this->text(),
            'footer' => $this->text(),
            'marked' => $this->integer(1)->notNull(),
            'YandexCode' => $this->string(255)->notNull(),
            'GoogleCode' => $this->string(255)->notNull(),
            'redirect' => $this->integer(1)->notNull(),
        ], $tableOptions);

        $this->createIndex('idx_region', '{{%geo_countries}}', 'region');
        $this->createIndex('idx_continent', '{{%geo_countries}}', 'continent');
        $this->createIndex('idx_population', '{{%geo_countries}}', 'population');
        $this->createIndex('idx_surfaceArea', '{{%geo_countries}}', 'surfaceArea');

        $this->createIndex('idx_stateName', '{{%geo_states}}', 'stateName');
        $this->createIndex('idx_countryID', '{{%geo_states}}', 'countryID');
        $this->createIndex('idx_stateDomain', '{{%geo_states}}', 'stateDomain');
        $this->createIndex('idx_countryStateName', '{{%geo_states}}', ['countryID', 'stateName']);

        $this->execute("ALTER TABLE `geo_cities` ADD UNIQUE( `cityID`, `stateID`, `countryID`)");
        $this->createIndex('idx_cityName', '{{%geo_cities}}', 'cityName');
        $this->createIndex('idx_stateID', '{{%geo_cities}}', 'stateID');
        $this->createIndex('idx_countryID', '{{%geo_cities}}', 'countryID');
        $this->createIndex('idx_countrystate', '{{%geo_cities}}', ['countryID','stateID']);
        $this->createIndex('idx_countrycity', '{{%geo_cities}}', ['countryID','cityID']);
        $this->createIndex('idx_cityDomain', '{{%geo_cities}}', 'cityDomain');
        $this->createIndex('idx_oldId', '{{%geo_cities}}', 'oldId');

        $this->execute(file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'data_cities.sql'));

        $this->batchInsert('{{%auth_item}}', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['geo_manage', \yii\rbac\Item::TYPE_PERMISSION, "Доступ к Гео модулю", NULL, time(), time()],
        ]);

        $this->batchInsert('{{%auth_item_child}}', ['parent', 'child'], [
            ['administrator', 'geo_manage'],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%geo_cities}}');
        $this->dropTable('{{%geo_states}}');
        $this->dropTable('{{%geo_countries}}');
    }
}
