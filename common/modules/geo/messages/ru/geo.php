<?php
/**
 * Created by PhpStorm.
 * User: rabadan731
 * Date: 10.08.2017
 * Time: 23:11
 */

return [
    "Geo module" => "Гео модуль",
    "Country" => "Страна",
    "Countries" => "Страны",
    "States" => "Регионы",
    "Cities" => "Города",
    "Create" => "Добавить",
    "City ID" => "ID Города",
    "City Name" => "Город EN",
    "City Ru Name" => "Город RU",
    "City Rp Name" => "Город RU-RP",
    "City Alias" => "Алиас города",
    "City Domain" => "Домен города",

    "State ID" => "ID Региона",
    "State Name" => "Регион EN",
    "State Ru Name" => "Регион RU",
    "State Rp Name" => "Регион RU-RP",
    "State Alias" => "Алиас региона",
    "State Domain" => "Домен региона",

    "Country ID" => "ID Страны",
    "Country Name" => "Страна EN",
    "Country Ru Name" => "Страна RU",
    "Country Rp Name" => "Страна RU-RP",
    "Country Alias" => "Алиас страны",
    "Country Domain" => "Домен страны",
    "Local Name" => "Локальное название",
    "Population" => "Население",
    "Lang Default" => "Язык по умолчанию",

    "Latitude" => "Широта",
    "Longitude" => "Долгота",
    "Old ID" => "Страый ID",
    "Active" => "Активность",
    "Title" => "Заголовок",
    "Desc" => "META [DESC]",
    "Keyw" => "META [KEY]",
    "Text" => "Текст страницы",
    "Footer" => "Текст футера",
    "Marked" => "",
    "Yandex Code" => "Yandex код",
    "Google Code" => "Google код",
    "Redirect" => "Редирект",
];