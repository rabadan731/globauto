<?php

namespace modules\geo\models;

use Yii;
use modules\geo\models\query\GeoCountriesQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "geo_countries".
 *
 * @property integer $countryID
 * @property string $countryName
 * @property string $countryRuName
 * @property string $countryRpName
 * @property string $countryAlias
 * @property string $countryDomain
 * @property string $localName
 * @property string $webCode
 * @property string $region
 * @property string $continent
 * @property double $latitude
 * @property double $longitude
 * @property double $surfaceArea
 * @property integer $population
 * @property integer $active
 * @property integer $currencyType
 * @property string $title
 * @property string $desc
 * @property string $keyw
 * @property string $h1
 * @property string $footer
 * @property string $text
 * @property string $langPack
 * @property string $langDefault
 * @property string $YandexCode
 * @property string $GoogleCode
 * @property string $textCounter
 * @property string $LiCounter
 * @property string $GoogleCounter
 */
class GeoCountries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['countryName', 'countryRuName', 'countryRpName', 'countryAlias', 'countryDomain', 'localName', 'webCode', 'region', 'title', 'langPack', 'langDefault'], 'required'],
            [['latitude', 'longitude', 'surfaceArea'], 'number'],
            [['population', 'active', 'currencyType'], 'integer'],
            [['footer', 'text', 'textCounter', 'LiCounter', 'GoogleCounter'], 'string'],
            [['countryName', 'countryRuName', 'countryRpName', 'countryAlias', 'countryDomain', 'localName', 'continent'], 'string', 'max' => 55],
            [['webCode', 'langDefault'], 'string', 'max' => 2],
            [['region'], 'string', 'max' => 26],
            [['title', 'keyw', 'h1', 'langPack', 'YandexCode', 'GoogleCode'], 'string', 'max' => 255],
            [['desc'], 'string', 'max' => 512],
            [['countryName'], 'unique'],
            [['webCode'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'countryID' => Yii::t('geo', 'Country ID'),
            'countryName' => Yii::t('geo', 'Country Name'),
            'countryRuName' => Yii::t('geo', 'Country Ru Name'),
            'countryRpName' => Yii::t('geo', 'Country Rp Name'),
            'countryAlias' => Yii::t('geo', 'Country Alias'),
            'countryDomain' => Yii::t('geo', 'Country Domain'),
            'localName' => Yii::t('geo', 'Local Name'),
            'webCode' => Yii::t('geo', 'Web Code'),
            'region' => Yii::t('geo', 'Region'),
            'continent' => Yii::t('geo', 'Continent'),
            'latitude' => Yii::t('geo', 'Latitude'),
            'longitude' => Yii::t('geo', 'Longitude'),
            'surfaceArea' => Yii::t('geo', 'Surface Area'),
            'population' => Yii::t('geo', 'Population'),
            'active' => Yii::t('geo', 'Active'),
            'currencyType' => Yii::t('geo', 'Currency Type'),
            'title' => Yii::t('geo', 'Title'),
            'desc' => Yii::t('geo', 'Desc'),
            'keyw' => Yii::t('geo', 'Keyw'),
            'h1' => Yii::t('geo', 'H1'),
            'footer' => Yii::t('geo', 'Footer'),
            'text' => Yii::t('geo', 'Text'),
            'langPack' => Yii::t('geo', 'Lang Pack'),
            'langDefault' => Yii::t('geo', 'Lang Default'),
            'YandexCode' => Yii::t('geo', 'Yandex Code'),
            'GoogleCode' => Yii::t('geo', 'Google Code'),
            'textCounter' => Yii::t('geo', 'Text Counter'),
            'LiCounter' => Yii::t('geo', 'Li Counter'),
            'GoogleCounter' => Yii::t('geo', 'Google Counter'),
        ];
    }

    /**
     * @inheritdoc
     * @return GeoCountriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GeoCountriesQuery(get_called_class());
    }

    public static function arrayList()
    {
        $items = GeoCountries::find()
            ->select('countryID, countryRuName')
            ->asArray()
            ->all();

        return ArrayHelper::map($items, 'countryID', 'countryRuName');
    }
}
