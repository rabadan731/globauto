<?php

namespace modules\geo\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "geo_states".
 *
 * @property integer $stateID
 * @property string $stateName
 * @property string $stateRuName
 * @property string $stateRpName
 * @property string $stateAlias
 * @property string $stateDomain
 * @property integer $countryID
 * @property double $latitude
 * @property double $longitude
 * @property integer $oldId
 * @property integer $active
 * @property string $title
 * @property string $desc
 * @property string $keyw
 * @property string $text
 * @property string $footer
 * @property string $YandexCode
 * @property string $GoogleCode
 */
class GeoStates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_states';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stateName', 'stateRuName', 'stateRpName', 'stateAlias', 'stateDomain', 'countryID', 'oldId', 'title', 'desc', 'keyw', 'YandexCode', 'GoogleCode'], 'required'],
            [['countryID', 'oldId', 'active'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['text', 'footer'], 'string'],
            [['stateName', 'stateRuName', 'stateRpName', 'stateAlias', 'stateDomain'], 'string', 'max' => 50],
            [['title', 'keyw', 'YandexCode', 'GoogleCode'], 'string', 'max' => 255],
            [['desc'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'stateID' => Yii::t('geo', 'State ID'),
            'stateName' => Yii::t('geo', 'State Name'),
            'stateRuName' => Yii::t('geo', 'State Ru Name'),
            'stateRpName' => Yii::t('geo', 'State Rp Name'),
            'stateAlias' => Yii::t('geo', 'State Alias'),
            'stateDomain' => Yii::t('geo', 'State Domain'),
            'countryID' => Yii::t('geo', 'Country ID'),
            'latitude' => Yii::t('geo', 'Latitude'),
            'longitude' => Yii::t('geo', 'Longitude'),
            'oldId' => Yii::t('geo', 'Old ID'),
            'active' => Yii::t('geo', 'Active'),
            'title' => Yii::t('geo', 'Title'),
            'desc' => Yii::t('geo', 'Desc'),
            'keyw' => Yii::t('geo', 'Keyw'),
            'text' => Yii::t('geo', 'Text'),
            'footer' => Yii::t('geo', 'Footer'),
            'YandexCode' => Yii::t('geo', 'Yandex Code'),
            'GoogleCode' => Yii::t('geo', 'Google Code'),
        ];
    }

    /**
     * @inheritdoc
     * @return \modules\geo\models\query\GeoStatesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \modules\geo\models\query\GeoStatesQuery(get_called_class());
    }

    public static function arrayList($countryID=null)
    {
        $find = GeoStates::find()
            ->select('stateID, stateRuName')
            ->orderBy('stateRuName');
        if (!is_null($countryID)) {
            $find->andWhere(['countryID' => $countryID]);
        }
        return ArrayHelper::map($find->asArray()->all(), 'stateID', 'stateRuName');
    }

    /**
     * @return ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(GeoCities::className(), ['stateID' => 'stateID'])->orderBy('cityRuName');
    }
}
