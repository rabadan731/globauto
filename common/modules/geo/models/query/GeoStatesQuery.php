<?php

namespace modules\geo\models\query;

/**
 * This is the ActiveQuery class for [[\modules\geo\models\GeoStates]].
 *
 * @see \modules\geo\models\GeoStates
 */
class GeoStatesQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['active' => 1]);
    }

    /**
     * @inheritdoc
     * @return \modules\geo\models\GeoStates[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \modules\geo\models\GeoStates|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
