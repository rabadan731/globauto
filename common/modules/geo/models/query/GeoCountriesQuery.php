<?php

namespace modules\geo\models\query;

/**
 * This is the ActiveQuery class for [[\modules\geo\models\GeoCountries]].
 *
 * @see \modules\geo\models\GeoCountries
 */
class GeoCountriesQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['active' => 1]);
    }

    /**
     * @inheritdoc
     * @return \modules\geo\models\GeoCountries[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \modules\geo\models\GeoCountries|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
