<?php

namespace modules\geo\models\query;

/**
 * This is the ActiveQuery class for [[\modules\geo\models\GeoCities]].
 *
 * @see \modules\geo\models\GeoCities
 */
class GeoCitiesQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['active' => 1]);
    }

    public function marked()
    {
        return $this->andWhere(['marked' => 1]);
    }

    /**
     * @inheritdoc
     * @return \modules\geo\models\GeoCities[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \modules\geo\models\GeoCities|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
