<?php

namespace modules\geo\models;

use Yii;
use modules\geo\models\query\GeoCitiesQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "geo_cities".
 *
 * @property integer $cityID
 * @property string $cityName
 * @property string $cityRuName
 * @property string $cityRpName
 * @property string $cityAlias
 * @property string $cityDomain
 * @property integer $stateID
 * @property integer $countryID
 * @property double $latitude
 * @property double $longitude
 * @property integer $oldId
 * @property integer $active
 * @property string $title
 * @property string $desc
 * @property string $keyw
 * @property string $text
 * @property string $footer
 * @property integer $marked
 * @property string $YandexCode
 * @property string $GoogleCode
 * @property integer $redirect
 *
 *
 * @property GeoStates $state
 */
class GeoCities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cityName', 'cityRuName', 'cityRpName', 'cityAlias', 'cityDomain', 'countryID', 'oldId', 'title', 'desc', 'keyw', 'marked', 'redirect'], 'required'],
            [['stateID', 'countryID', 'oldId', 'active', 'marked', 'redirect'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['text', 'footer'], 'string'],
            [['cityName', 'cityRuName', 'cityRpName', 'cityAlias', 'cityDomain'], 'string', 'max' => 50],
            [['title', 'keyw', 'YandexCode', 'GoogleCode'], 'string', 'max' => 255],
            [['desc'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cityID' => Yii::t('geo', 'City ID'),
            'cityName' => Yii::t('geo', 'City Name'),
            'cityRuName' => Yii::t('geo', 'City Ru Name'),
            'cityRpName' => Yii::t('geo', 'City Rp Name'),
            'cityAlias' => Yii::t('geo', 'City Alias'),
            'cityDomain' => Yii::t('geo', 'City Domain'),
            'stateID' => Yii::t('geo', 'State ID'),
            'countryID' => Yii::t('geo', 'Country ID'),
            'latitude' => Yii::t('geo', 'Latitude'),
            'longitude' => Yii::t('geo', 'Longitude'),
            'oldId' => Yii::t('geo', 'Old ID'),
            'active' => Yii::t('geo', 'Active'),
            'title' => Yii::t('geo', 'Title'),
            'desc' => Yii::t('geo', 'Desc'),
            'keyw' => Yii::t('geo', 'Keyw'),
            'text' => Yii::t('geo', 'Text'),
            'footer' => Yii::t('geo', 'Footer'),
            'marked' => Yii::t('geo', 'Marked'),
            'YandexCode' => Yii::t('geo', 'Yandex Code'),
            'GoogleCode' => Yii::t('geo', 'Google Code'),
            'redirect' => Yii::t('geo', 'Redirect'),
        ];
    }

    /**
     * @inheritdoc
     * @return GeoCitiesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GeoCitiesQuery(get_called_class());
    }

    public static function findFullName($cityID)
    {
        if (is_null($cityID)) {
            return null;
        }
        $find = self::find()->andWhere(['cityID' => $cityID])->one();
        $cityName = "[Неизвестный город]";
        if (!is_null($find)) {
            $cityName = $find->cityRuName;
            if (!is_null($find->state)) {
                $cityName = "{$cityName}, {$find->state->stateRuName}";
            }
        }
        return $cityName;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(GeoStates::className(), ['stateID' => 'stateID']);
    }

    public static function arrayList($active=true)
    {
        $find = GeoCities::find()
            ->select('cityID, cityRuName')
            ->orderBy('cityRuName');

        if ($active) {
            $find->active();
        }

        return ArrayHelper::map($find->asArray()->all(), 'cityID', 'cityRuName');
    }
}
