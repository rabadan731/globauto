<?php

namespace modules\geo\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\geo\models\GeoCountries;

/**
 * GeoCountriesSearch represents the model behind the search form about `modules\geo\models\GeoCountries`.
 */
class GeoCountriesSearch extends GeoCountries
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['countryID', 'population', 'active', 'currencyType'], 'integer'],
            [['countryName', 'countryRuName', 'countryRpName', 'countryAlias', 'countryDomain', 'localName', 'webCode', 'region', 'continent', 'title', 'desc', 'keyw', 'h1', 'footer', 'text', 'langPack', 'langDefault', 'YandexCode', 'GoogleCode', 'textCounter', 'LiCounter', 'GoogleCounter'], 'safe'],
            [['latitude', 'longitude', 'surfaceArea'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GeoCountries::find();

        $query->orderBy('countryRuName');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'countryID' => $this->countryID,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'surfaceArea' => $this->surfaceArea,
            'population' => $this->population,
            'active' => $this->active,
            'currencyType' => $this->currencyType,
        ]);

        $query->andFilterWhere(['like', 'countryName', $this->countryName])
            ->andFilterWhere(['like', 'countryRuName', $this->countryRuName])
            ->andFilterWhere(['like', 'countryRpName', $this->countryRpName])
            ->andFilterWhere(['like', 'countryAlias', $this->countryAlias])
            ->andFilterWhere(['like', 'countryDomain', $this->countryDomain])
            ->andFilterWhere(['like', 'localName', $this->localName])
            ->andFilterWhere(['like', 'webCode', $this->webCode])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'continent', $this->continent])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'keyw', $this->keyw])
            ->andFilterWhere(['like', 'h1', $this->h1])
            ->andFilterWhere(['like', 'footer', $this->footer])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'langPack', $this->langPack])
            ->andFilterWhere(['like', 'langDefault', $this->langDefault])
            ->andFilterWhere(['like', 'YandexCode', $this->YandexCode])
            ->andFilterWhere(['like', 'GoogleCode', $this->GoogleCode])
            ->andFilterWhere(['like', 'textCounter', $this->textCounter])
            ->andFilterWhere(['like', 'LiCounter', $this->LiCounter])
            ->andFilterWhere(['like', 'GoogleCounter', $this->GoogleCounter]);

        return $dataProvider;
    }
}
