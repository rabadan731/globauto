<?php

namespace modules\geo\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\geo\models\GeoStates;

/**
 * GeoStatesSearch represents the model behind the search form about `modules\geo\models\GeoStates`.
 */
class GeoStatesSearch extends GeoStates
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stateID', 'countryID', 'oldId', 'active'], 'integer'],
            [['stateName', 'stateRuName', 'stateRpName', 'stateAlias', 'stateDomain', 'title', 'desc', 'keyw', 'text', 'footer', 'YandexCode', 'GoogleCode'], 'safe'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GeoStates::find();

        $query->orderBy('stateRuName');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'stateID' => $this->stateID,
            'countryID' => $this->countryID,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'oldId' => $this->oldId,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'stateName', $this->stateName])
            ->andFilterWhere(['like', 'stateRuName', $this->stateRuName])
            ->andFilterWhere(['like', 'stateRpName', $this->stateRpName])
            ->andFilterWhere(['like', 'stateAlias', $this->stateAlias])
            ->andFilterWhere(['like', 'stateDomain', $this->stateDomain])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'keyw', $this->keyw])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'footer', $this->footer])
            ->andFilterWhere(['like', 'YandexCode', $this->YandexCode])
            ->andFilterWhere(['like', 'GoogleCode', $this->GoogleCode]);

        return $dataProvider;
    }
}
