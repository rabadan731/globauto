<?php

namespace modules\geo\models\search;

use modules\geo\models\GeoCountries;
use modules\geo\models\GeoStates;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\geo\models\GeoCities;
use yii\helpers\ArrayHelper;

/**
 * GeoCitiesSearch represents the model behind the search form about `modules\geo\models\GeoCities`.
 */
class GeoCitiesSearch extends GeoCities
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cityID', 'stateID', 'countryID', 'oldId', 'active', 'marked', 'redirect'], 'integer'],
            [['cityName', 'cityRuName', 'cityRpName', 'cityAlias', 'cityDomain', 'title', 'desc', 'keyw', 'text', 'footer', 'YandexCode', 'GoogleCode'], 'safe'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GeoCities::find();

        $query->orderBy('cityRuName');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cityID' => $this->cityID,
            'stateID' => $this->stateID,
            'countryID' => $this->countryID,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'oldId' => $this->oldId,
            'active' => $this->active,
            'marked' => $this->marked,
            'redirect' => $this->redirect,
        ]);

        $query->andFilterWhere(['like', 'cityName', $this->cityName])
            ->andFilterWhere(['like', 'cityRuName', $this->cityRuName])
            ->andFilterWhere(['like', 'cityRpName', $this->cityRpName])
            ->andFilterWhere(['like', 'cityAlias', $this->cityAlias])
            ->andFilterWhere(['like', 'cityDomain', $this->cityDomain])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'keyw', $this->keyw])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'footer', $this->footer])
            ->andFilterWhere(['like', 'YandexCode', $this->YandexCode])
            ->andFilterWhere(['like', 'GoogleCode', $this->GoogleCode]);

        return $dataProvider;
    }

    public function getCountriesList()
    {
        return GeoCountries::arrayList();
    }

    public function getStatesList()
    {
        return GeoStates::arrayList($this->countryID);
    }

}
