<?php

namespace modules\geo\controllers\backend;

use Yii;
use modules\geo\models\GeoCities;
use modules\geo\models\GeoStates;
use modules\geo\models\search\GeoCitiesSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CitiesController implements the CRUD actions for GeoCities model.
 */
class CitiesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GeoCities models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GeoCitiesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GeoCities model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GeoCities model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GeoCities();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cityID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing GeoCities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cityID]);
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing GeoCities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionAjaxUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {

            $post = Yii::$app->request->post("GeoCities", null);

            if (is_null($post)) {
                return $this->renderAjax('ajax-update', [
                    'model' => $model,
                ]);
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            $status = ($model->load(Yii::$app->request->post()) && $model->save());

            $res = array(
                'success' => $status,
                'statusText' => $status?Yii::t('app', 'Saved'):Yii::t('app', 'Errors'),
                'errors' => $model->errors
            );
            return $res;
        }

        throw new NotFoundHttpException('The requested page does not exist. Please Ajax.');
    }

    /**
     * Deletes an existing GeoCities model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GeoCities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GeoCities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GeoCities::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionListStates()
    {
        $countryID = Yii::$app->request->post('depdrop_all_params', null);
        if (!is_null($countryID) && isset($countryID['geocities-countryid'])) {
            $countryID = $countryID['geocities-countryid'];
        }
        $subCatList = [];
        foreach (GeoStates::arrayList($countryID) as $id => $name) {
            $subCatList[] = ['id' => $id, 'name' => $name];
        }

        echo Json::encode(['output'=>$subCatList, 'selected'=>'']);
        return;
    }
}
