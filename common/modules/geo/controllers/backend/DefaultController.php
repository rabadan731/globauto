<?php

namespace modules\geo\controllers\backend;

use Yii;
use backend\components\BackendController;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class DefaultController
 * @package modules\geo\controllers\backend
 */
class DefaultController extends BackendController
{
//    /**
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => ['error']
//                    ],
//                ],
//            ]
//        ]);
//    }

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/files/images/', // Directory URL address, where files are stored.
                'path' => '@root/storage/images' // Or absolute path to directory where files are stored.
            ],
        ];
    }
 


    public function actionIndex()
    {
        return $this->renderContent('seo INDEX');
    }

}