<?php

namespace modules\geo\widgets;

use modules\geo\models\GeoCities;
use modules\geo\models\GeoStates;
use yii\base\Widget;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

class SelectCity extends Widget
{
    public $locationList;
    public $locationMarkedList;

    public function init()
    {
        parent::init();
        if ($this->locationList === null) {
            $dataCity = GeoCities::find()
                ->select(['cityRuName as value', 'cityDomain as id'])
                ->andWhere(['or', ['active' => 1], ['redirect'=> 1]])
                ->asArray()
                ->all();
            $dataState = GeoStates::find()
                ->select(['stateRuName as value', 'stateDomain as id'])
                ->active()
                ->asArray()
                ->all();

            $this->locationList = array_merge($dataCity, $dataState);
        }

        if ($this->locationMarkedList === null) {
            $this->locationMarkedList = GeoCities::find()
                ->select(['cityRuName', 'cityDomain'])
                ->andWhere(['or', ['active' => 1], ['redirect'=> 1]])
                ->marked()
                ->all();
        }
    }

    public function run()
    {
        return $this->render('select-city', [
            'locationList' => $this->locationList,
            'locationMarkedList' => $this->locationMarkedList,
            'urlSetRegion' => Url::to(['/geo/default/setregion'])
        ]);
    }
}