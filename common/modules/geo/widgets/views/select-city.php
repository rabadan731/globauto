<?php

use yii\web\JsExpression;
use yii\jui\AutoComplete;
use yii\helpers\Url;
use modules\geo\models\GeoCities;

/** @var $urlSetRegion string */
/** @var $locationMarkedList GeoCities [] */
/** @var $locationList [] */

?>
<div class="popup" id="city">
    <div class="popup_title">ВЫБОР ГОРОДА</div>
    <form class="popup_form">
        <label>Введите название вашего города</label>
        <?= AutoComplete::widget([
            'clientOptions' => [
                'source' => $locationList,
                'minLength' => '2',
                'autoFill' => true,
                'select' => new JsExpression("function( event, ui ) {
                    $.ajax({
                      url: '{$urlSetRegion}',
                      type: 'GET',
                      data: {city: ui.item.id}
                    }); 
                }"),
            ],
        ]); ?>
    </form>
    <ul>
        <?php foreach ($locationMarkedList as $item) { ?>
            <li>
                <a href="<?=Url::to(['/geo/default/setregion', 'city' => $item->cityDomain]);?>">
                    <strong><?=$item->cityRuName;?></strong>
                </a>
            </li>
        <?php } ?>
    </ul>
</div>