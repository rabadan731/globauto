<?php

namespace modules\seo;

use Yii;

/**
 * Class Module
 * @package modules\seo
 */
class Module extends \yii\base\Module
{
    /**
     * @var bool
     */
    public $isBackend = false;
    const MESSAGE_CATEGORY = 'seo';

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->isBackend === true) {
            $this->setViewPath('@modules/' . $this->id . '/views/backend');
            $this->setLayoutPath('@backend/views/layouts');
            $this->controllerNamespace = $this->controllerNamespace === null ?
                '\modules\\' . $this->id . '\controllers\backend' : $this->controllerNamespace;
        } else {
            $this->setViewPath('@modules/' . $this->id . '/views/frontend');
            $this->setLayoutPath('@frontend/views/layouts');
            $this->controllerNamespace = $this->controllerNamespace === null ?
                '\modules\\' . $this->id . '\controllers\frontend' : $this->controllerNamespace;
        }

        self::registerTranslations();
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function registerTranslations()
    {
        $mc = self::MESSAGE_CATEGORY;
        $translations = Yii::$app->i18n->translations;
        if (!isset($translations[$mc]) && !isset($translations["{$mc}/*"])) {
            Yii::$app->i18n->translations[$mc] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => "@common/modules/seo/messages",
                'forceTranslation' => true,
                'fileMap' => [
                    $mc => "{$mc}.php"
                ]
            ];
        }
    }
}
