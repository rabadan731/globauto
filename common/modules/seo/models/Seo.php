<?php

namespace modules\seo\models;

use Yii;
use common\modules\seo\models\query\SeoQuery;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "seo".
 *
 * @property integer $seo_id
 * @property string $controller
 * @property string $action
 * @property string $comment
 * @property string $title
 * @property string $h1
 * @property string $h2
 * @property string $seo_key
 * @property string $seo_desc
 * @property string $body
 * @property string $footer
 * @property string $breadcrumb
 * @property integer $status_edit
 * @property integer $created_at
 * @property integer $updated_at
 * @property $replaceData []
 */
class Seo extends \yii\db\ActiveRecord
{
    public $imgDir = 'seo';

    public $replaceData = [];

    CONST EDIT_STATUS_NO = 0;
    CONST EDIT_STATUS_SAFE = 4;
    CONST EDIT_STATUS_YES = 9;


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo';
    }

    /**
     * The id of the current or next object id
     *
     * @return int
     */
    public function getThisId()
    {
        if ($this->isNewRecord) {
            return self::find()->max('seo_id') + 1;
        } else {
            return $this->seo_id;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['controller', 'action', 'title'], 'required'],
            [['seo_key', 'seo_desc', 'body', 'footer'], 'string'],
            [['status_edit', 'created_at', 'updated_at'], 'integer'],
            [
                ['controller', 'action', 'comment', 'title', 'h1', 'h2', 'breadcrumb'],
                'string',
                'max' => 512
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'seo_id' => Yii::t('seo', 'ID'),
            'action' => Yii::t('seo', 'Action'),
            'controller' => Yii::t('seo', 'Controller'),
            'comment' => Yii::t('seo', 'Comment'),
            'created_at' => Yii::t('seo', 'Created At'),
            'updated_at' => Yii::t('seo', 'Updated At'),

            'title' => Yii::t('seo', 'Title'),
            'seo_key' => Yii::t('seo', 'Seo Key'),
            'seo_desc' => Yii::t('seo', 'Seo Desc'),
            'seo_h1' => Yii::t('seo', 'Title H1'),
            'seo_h2' => Yii::t('seo', 'Title H2'),
            'body' => Yii::t('seo', 'Body'),
            'footer' => Yii::t('seo', 'Footer text'),
            'breadcrumb' => Yii::t('seo', 'Breadcrumb'),
        ];
    }

    /**
     * @inheritdoc
     * @return SeoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SeoQuery(get_called_class());
    }

    /**
     * @param $controller
     * @param $action
     * @param array $replace
     * @param string $default
     * @return string
     */
    public static function br($controller, $action, $replace = [], $default = "")
    {
        $model = self::find_or_create($controller, $action, 'breadcrumb');
        $model->replaceData = $replace;
        $model->replace($replace);
        return $model->breadcrumb;
    }

    /**
     * Displays a single Page model.
     * @param string $controller
     * @param string $action
     * @param [] $replace
     * @param string $default
     * @return Seo
     */
    public static function parse($controller, $action, $replace = [], $default = "")
    {
        $model = Seo::find_or_create($controller, $action, $default);

        $model->replaceData = $replace;
        $model->replace($replace);
        $model->setMetaTags();
        return $model;
    }


    public function setMetaTags()
    {
        Yii::$app->view->title = $this->title;
        Yii::$app->params['footerText'] = $this->footer;
        Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $this->seo_desc]);
        Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $this->seo_key]);
    }

    /**
     * @param $replace
     */
    public function replace($replace)
    {
        if (sizeof($replace)) {
            $search = array_map(function ($item) {
                return "{{{$item}}}";
            }, array_keys($replace));

            $this->attributes = array_map(function ($item) use ($search, $replace) {
                return str_replace($search, $replace, $item);
            }, $this->attributes);
        }
    }

    /**
     * @return array
     */
    public static function ddControlersList()
    {
        $categories = self::find()->select(['controller'])->distinct();
        return ArrayHelper::map($categories->all(), 'controller', 'controller');
    }

    /**
     * @param $controller
     * @param $action
     * @param $default
     * @return Seo
     */
    public static function find_or_create(
        $controller,
        $action,
        $default,
        $select = null,
        $status_edit = self::EDIT_STATUS_SAFE
    ) {
        /** @var $model Seo */
        $find = self::find()->ca($controller, $action);
        if ($select !== null) {
            $find->select($select);
        }

        $model = $find->one();

        if (is_null($model)) {
            $model = new Seo();
            $model->controller  = $controller;
            $model->action      = $action;
            $model->title       = $default;
            $model->comment     = "Seo by: {$default}";
            $model->body        = '';
            $model->h1          = $default;
            $model->h2          = $default;
            $model->seo_desc    = $default;
            $model->seo_key     = $default;
            $model->breadcrumb  = $default;
            $model->status_edit = $status_edit;
            $model->save();
        }
        return $model;
    }
}
