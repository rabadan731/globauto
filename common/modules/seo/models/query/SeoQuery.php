<?php

namespace common\modules\seo\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Seo]].
 *
 * @see \common\models\Seo
 */
class SeoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Seo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Seo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function ca($controller,$action)
    {
        $this->andWhere(['controller'=>$controller])->andWhere(['action'=>$action]);
        return $this;
    }

    public function action($action)
    {
        $this->andWhere(['action'=>$action]);
        return $this;
    }

    public function controller($controller)
    {
        $this->andWhere(['controller'=>$controller]);
        return $this;
    }
}