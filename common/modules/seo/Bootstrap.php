<?php

namespace modules\seo;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package modules\seo
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $rules = [];

//        if (Yii::$app->id == 'app-backend') {
//
//        }

        $app->urlManager->addRules($rules, false);
    }
}
