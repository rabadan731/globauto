<?php

return [
    'Seo module' => 'SEO Модуль',
    'Seo manager' => 'Управлене SEO',

    'Title' => '<title>',
    'Comment' => 'Коментарий',
    'Breadcrumb' => 'Глубиномер',
    'Body' => 'Текст страницы',
    'Footer text' => 'Текст футера',

];
