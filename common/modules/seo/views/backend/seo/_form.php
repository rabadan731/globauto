<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\seo\models\Seo;
use yii\helpers\Url;
use common\widgets\ck\CKEditor;

/* @var $this yii\web\View */
/* @var $model modules\seo\models\Seo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seo-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'controller')->textInput([
                'maxlength' => true,
                'disabled' => $model->status_edit < Seo::EDIT_STATUS_YES
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'action')->textInput([
                'maxlength' => $model->status_edit < Seo::EDIT_STATUS_YES,
                'disabled' => $model->status_edit < Seo::EDIT_STATUS_YES
            ]) ?>
        </div>
    </div>

    <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'h1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'h2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'breadcrumb')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_key')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'body')->widget(CKEditor::className(), [
        'preset' => 'full',
        'clientOptions' => [
            'filebrowserUploadUrl' => Url::to(['/ckloader'])
        ]
    ]) ?>
    <?= $form->field($model, 'footer')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$js = <<< JS
    $(document).ready(function() {
     
        $('input, textarea').focusout(function() {
          $('.rep-root').data('last-focus', this.id);
        });
        
    });
JS;




$this->registerJs($js);
?>