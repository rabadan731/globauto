<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\seo\models\Seo */

$this->title = Yii::t('seo', 'Update {modelClass}: ', [
    'modelClass' => 'Seo',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('seo', 'Seos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->seo_id]];
$this->params['breadcrumbs'][] = Yii::t('seo', 'Update');
?>
<div class="seo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
