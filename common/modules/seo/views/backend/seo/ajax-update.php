<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\seo\models\Seo */

$this->title = Yii::t('seo', 'Update {modelClass}: ', [
    'modelClass' => 'Seo',
]) . $model->title;
?>
<div class="seo-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
