<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\seo\models\Seo */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('seo', 'Seos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('seo', 'Update'), ['update', 'id' => $model->seo_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('seo', 'Delete'), ['delete', 'id' => $model->seo_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('seo', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'seo_id',
            'controller',
            'action',
            'comment',
            'title',
            'h1',
            'h2',
            'seo_key:ntext',
            'seo_desc:ntext',
            'body:ntext',
            'footer:ntext',
            'breadcrumb',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
