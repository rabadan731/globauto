<?php
$mas = [
    'locality' => 'Город/Регион en',
    'localityRp' => 'Город/Регион ru Род.Падеж',
    'localityRu' => 'Город/Регион ru',
    'mark' => 'Марка en',
    'mark_rus' => 'Марка ru',
];
?>
<ul class="rep-root" data-last-focus="">
    <?php foreach ($mas as $tag => $title) { ?>
        <li style="list-style: none">
            <a style="font-size: 110%" href="javascript:void(0)" class="rep-item">
                {{<?=$tag;?>}}
            </a>
            - <span style="font-size: 120%"><?=$title;?></span>
        </li>
    <?php } ?>
</ul>
