<?php

use yii\helpers\Html;
use common\assets\AjaxFormAsset;
use yii\grid\GridView;
use yii\widgets\Pjax;
use modules\seo\models\Seo;

/* @var $this yii\web\View */
/* @var $searchModel modules\seo\models\search\SeoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('seo', 'Seos');
$this->params['breadcrumbs'][] = $this->title;

AjaxFormAsset::register($this);

?>
<div class="seo-index">
    <div class="row">
        <div class="col-md-4">
            <p>
                <?= Html::a(Yii::t('seo', 'Create Seo'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?php Pjax::begin(); ?>    <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [

                    [
                        'attribute' => 'controller',
                        'filter' => Seo::ddControlersList()
                    ],
                    'action',
                    'comment',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{ajax-update} {delete}',
                        'options' => ['style' => 'width:70px']
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>        </div>
        <div class="col-md-5">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
        <div class="col-md-3">
            <div class="navigation">
                <div id="templates">
                    <div class="white-bg padding-10">
                        <?= $this->render('_templates'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .navigation {
        position: relative;
    }

    .navigation.fixed {
        position: fixed;
        top: 0;
    }
</style>

<?php
$js = <<< JS
    $(document).ready(function() {
        var windowHeight = $(window).height();
        $('.navigation, #templates').css('height', windowHeight);
        var __window = $(window),
            __navigation = $(".navigation");

        __window.scroll(function() {
            if (!__navigation.hasClass("fixed") && (__window.scrollTop() > __navigation.offset().top)) {
                __navigation.addClass("fixed").data("top", __navigation.offset().top);
            }
            else if (__navigation.hasClass("fixed") && (__window.scrollTop() < __navigation.data("top"))) {
                __navigation.removeClass("fixed");
            }
        });
        
        $('.rep-item').click(function(item) {
           elem = $("#"+$('.rep-root').data('last-focus'));
           elem.val(elem.val()+" "+this.text.trim()+" ");
           elem.focus();
        });
    });
JS;




$this->registerJs($js);
?>