<?php

use yii\db\Migration;
use modules\seo\models\Seo;

class m140703_123106_page_seo extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%seo}}', [
            'seo_id'        => $this->primaryKey(),
            'controller'    => $this->string(512)->notNull(),
            'action'        => $this->string(512)->notNull(),
            'comment'       => $this->string(512),

            'title'         => $this->string(512)->notNull(),
            'h1'            => $this->string(512),
            'h2'            => $this->string(512),
            'seo_key'       => $this->text(),
            'seo_desc'      => $this->text(),
            'body'          => $this->text(),
            'footer'        => $this->text()->defaultValue(null),
            'breadcrumb'    => $this->string(512),

            'status_edit'   => $this->integer()->defaultValue(Seo::EDIT_STATUS_YES),
            'created_at'    => $this->integer(),
            'updated_at'    => $this->integer(),
        ], $tableOptions);


        // Если у нас есть в базе RBAC
        if ($this->existRBAC()) {

            $this->batchInsert(
                '{{%auth_item}}',
                ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'],
                [['seo', 2, "Управление SEO", NULL, time(), time()]]
            );

            $this->batchInsert(
                '{{%auth_item_child}}',
                ['parent', 'child'],
                [['administrator', 'seo']]
            );
        }
    }

    public function safeDown()
    {
        $this->dropTable('{{%seo}}');
        // Если у нас есть в базе RBAC
        if ($this->existRBAC()) {
            \modules\users\models\AuthItem::deleteAll(['name'=>'seo']);
            \modules\users\models\AuthItemChild::deleteAll(['parent'=>'seo']);
            \modules\users\models\AuthItemChild::deleteAll(['child'=>'seo']);
        }
    }


    private function existRBAC()
    {
        return (
            (\Yii::$app->db->getTableSchema('{{%auth_item}}', true) !== null) &&
            (\Yii::$app->db->getTableSchema('{{%auth_item_child}}', true) !== null)
        );
    }
}
