<?php

use yii\db\Migration;
use modules\seo\models\Seo;

class m140703_143116_tags extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tag}}', [
            'id'     => $this->primaryKey(),
            'name'   => $this->string(1024)->notNull(),
            'count'  => $this->integer(),
        ], $tableOptions);

        $this->createTable('{{%article_tag}}', [
            'model_id' => $this->integer()->notNull(),
            'tag_id'   => $this->integer()->notNull(),
            'ord'      => $this->integer(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%article_tag}}');
        $this->dropTable('{{%tag}}');
    }
}
