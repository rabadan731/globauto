<?php

use yii\db\Migration;

/**
 * Class m171103_075003_articles
 */
class m171103_075003_articles extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci';
        }

        $this->createTable('{{%article_category}}', [
            'article_category_id' => $this->primaryKey(),
            'slug'                => $this->string(512)->notNull(),
            'icon'                => $this->string(1024),
            'image'               => $this->string(1024),
            'status'              => $this->integer(),
        ], $tableOptions);

        $this->createTable('{{%article}}', [
            'article_id'          => $this->primaryKey(),
            'article_category_id' => $this->integer(),
            'image'               => $this->string(1024),
            'status'              => $this->integer(),
            'slug'                => $this->string(512)->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx_article_category_item_key',
            '{{%article}}',
            'article_category_id'
        );
        $this->addForeignKey(
            'frx_article_category_item_key',
            '{{%article}}',
            'article_category_id',
            '{{%article_category}}',
            'article_category_id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%article}}');
        $this->dropTable('{{%article_category}}');
    }
}
