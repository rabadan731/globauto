<?php

/** @var $model \common\modules\main\models\Article */
/** @var $seo \modules\seo\models\Seo */
$seo = $model->seo;

?>
<a class="facade_news_list_item" href="<?= $model->url; ?>">
    <img src="<?= $model->image; ?>"
         alt=""
         class="facade_news_list_item_photo" />
    <span class="facade_news_list_item_description">
        <span class="facade_news_list_item_name">
            <?= $seo->title; ?>
        </span>
        <span class="facade_news_list_item_info">
            <span class="facade_news_list_item_info_date">
                <?= Yii::$app->formatter->asDate($seo->created_at, 'short'); ?>
            </span>
            <span class="facade_news_list_item_info_comments">21</span>
        </span>
    </span>
</a>