<?php

use common\modules\main\models\Article;

/** @var $model \common\modules\main\models\Article */
/** @var $seo \modules\seo\models\Seo */

$this->params['mainClass'] = "main publication main_bg row";
$seo = $model->seo;
?>

<div class="container">
    <div class="row">
        <div class="clearfix">
            <h1 class="pagetitle">
                <?= $seo->h1; ?>
            </h1>
        </div>
        <div class="content col-xs-12"  itemprop="mainContentOfPage">

            <div class="publication_content clearfix">
                <?= $seo->body; ?>
            </div>

            <section class="brand_news hidden-xs  row">
                <div class="col-xs-12">
                    <h2 class="title">ПУБЛИКАЦИИ FORD</h2>
                </div>
                <ul class="brand_news_list">
                    <?php foreach ($articles as $article) { ?>
                        <li class="brand_news_list_item col-sm-6 col-md-4">
                            <?= $this->render('_article', [
                                'model' => $article
                            ]); ?>
                        </li>
                    <?php } ?>
                </ul>
            </section>
        </div>
    </div>
</div>

