<?php

use yii\helpers\Html;
use modules\geo\models\GeoCities;
use yii\widgets\ActiveForm;
use modules\users\models\User;

/** @var $this \yii\web\View */
/** @var $user \modules\users\models\User */
?>

<div class="row">
    <div class="col-sm-9">
        <div class="longtitle clearfix">Личная информация</div>
        <ul class="account_info_list">
            <li class="account_info_list_item clearfix">
                <div class="account_info_list_item_value col-xs-12 col-sm-6">ФИО:</div>
                <div class="account_info_list_item_description col-xs-12 col-sm-6">
                    <?= $user->username; ?>
                </div>
            </li>
            <li class="account_info_list_item clearfix">
                <div class="account_info_list_item_value col-xs-12 col-sm-6">Город:</div>
                <div class="account_info_list_item_description col-xs-12 col-sm-6">
                    <?= GeoCities::findFullName($user->cityID); ?>
                </div>
            </li>
            <li class="account_info_list_item clearfix">
                <div class="account_info_list_item_value col-xs-12 col-sm-6">Контактный телефон:</div>
                <div class="account_info_list_item_description col-xs-12 col-sm-6">
                    <?= $user->phone; ?>
                </div>
            </li>
            <li class="account_info_list_item clearfix">
                <div class="account_info_list_item_value col-xs-12 col-sm-6">Почта:</div>
                <div class="account_info_list_item_description col-xs-12 col-sm-6">
                    <a href="mailto:<?= $user->email; ?>"><?= $user->email; ?></a>
                </div>
            </li>
        </ul>
        <div class="clearfix">
            <a href="#account_info_edit" class="button lilac popup_box3">ИЗМЕНИТЬ АНКЕТУ</a>
        </div>


        <div id="account_info_edit">

            <div class="longtitle clearfix">РЕДАКТИРОВАНИЕ ПРОФИЛЯ</div>
            <div class="clearfix">
                <?php $form = ActiveForm::begin(['class' => 'company']); ?>

                <?= $form->field($user, 'username', ['options' => ['class' => 'form_line']])
                    ->textInput(['class' => 'company_input']); ?>

                <?= $form->field($user, 'phone', ['options' => ['class' => 'form_line']])
                    ->textInput(['class' => 'company_input']); ?>

                <?= $form->field($user, 'sex', ['options' => ['class' => 'form_line']])
                    ->dropDownList(User::getSexArray(), ['class' => 'company_input', 'prompt' => '-- select --']); ?>

                <?= $form->field($user, 'type', ['options' => ['class' => 'form_line']])
                    ->dropDownList(User::getTypeArray(), ['class' => 'company_input']); ?>

                <?= $form->field($user, 'cityID', ['options' => ['class' => 'form_line']])
                    ->dropDownList(GeoCities::arrayList(), ['prompt'=> '--select--', 'class' => 'company_input']) ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), [
                        'class' => 'company_submit button lilac',
                        'name' => 'profile-button',
                    ]); ?>
                </div>
                <?php ActiveForm::end(); ?>

                <?php /**
                <form class="company">

                    <div class="form_line">
                        <label class="company_description">Дата рождения:</label>
                        <ul class="birthday">
                            <li class="birthday_item col-xs-4">
                                <select class="company_select">
                                    <option value="" selected>День</option>
                                    <option value="Легковой автомобиль">1</option>
                                    <option value="Грузовой автомобиль">2</option>
                                </select>
                            </li>
                            <li class="birthday_item col-xs-4">
                                <select class="company_select">
                                    <option value="" selected>Месяц</option>
                                    <option value="Легковой автомобиль">1</option>
                                    <option value="Грузовой автомобиль">2</option>
                                </select>
                            </li>
                            <li class="birthday_item col-xs-4">
                                <select class="company_select">
                                    <option value="" selected>Год</option>
                                    <option value="Легковой автомобиль">1</option>
                                    <option value="Грузовой автомобиль">2</option>
                                </select>
                            </li>
                        </ul>
                    </div>


                    <div class="form_line">
                        <label class="company_description">Почта:</label>
                        <input type="text" class="company_input" placeholder="maksim@nabiullin.com">
                    </div>
                    <div class="form_line">
                        <label class="company_description">Уведомления:</label>
                        <div class="filter_body">
                            <div class="col-xs-12">
                                <input type="checkbox" id="11" name="11">
                                <label for="11">Ответы на комментарии</label>
                            </div>
                            <div class="col-xs-12">
                                <input type="checkbox" id="22" name="22">
                                <label for="22">Комментарии к объявлению</label>
                            </div>
                        </div>
                    </div>
                    <div class="form_line">
                        <label class="company_description">Текущий пароль:</label>
                        <input type="text" class="company_input" placeholder="******">
                        <div><a href="#" class="filter_add">Показать пароль</a>&nbsp;&nbsp;&nbsp;<a href="#" class="filter_add">Изменить пароль</a></div>
                    </div>
                    <div class="form_line">
                        <label class="company_description">Новый пароль:</label>
                        <input type="text" class="company_input" placeholder="">
                    </div>
                    <div class="form_line">
                        <label class="company_description">Еще раз:</label>
                        <input type="text" class="company_input" placeholder="">
                    </div>

                    <div class="form_line">
                        <input type="submit" class="company_submit button lilac" value="Сохранить изменения">
                    </div>
                </form>
 *  */ ?>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <img src="img/photo2.png" alt="" class="account_photo">
    </div>
</div>
