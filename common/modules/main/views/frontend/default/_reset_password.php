<?php

use yii\helpers\Html;
use modules\geo\models\GeoCities;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/** @var $this \yii\web\View */
/** @var $user \modules\users\models\User */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $changePasswordForm \modules\users\models\forms\ChangePasswordForm */

$changePasswordForm = new \modules\users\models\forms\ChangePasswordForm();

?>

<?php $form = ActiveForm::begin([
    'id' => 'form-password',
    'action' => Url::to(['/user/user/update-password']),
    'class' => 'company'
]); ?>

<?php if ($user->password_hash != '') : ?>
    <?= $form->field($changePasswordForm, 'old_password', ['options' => ['class' => 'form_line']])
        ->passwordInput(['class' => 'company_input']); ?>
    <br/>
<?php endif; ?>
<?= $form->field($changePasswordForm, 'new_password', ['options' => ['class' => 'form_line']])
    ->passwordInput(['class' => 'company_input']); ?>
<br/>
<?= $form->field($changePasswordForm, 'new_password_repeat', ['options' => ['class' => 'form_line']])
    ->passwordInput(['class' => 'company_input']); ?>
<br/>
<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), [
        'class' => 'company_submit button lilac',
        'name' => 'password-button'
    ]) ?>
</div>
<?php ActiveForm::end(); ?>
