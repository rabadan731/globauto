<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use themes\globauto\assets\GlobAutoAsset;
use themes\globauto\assets\plugins\OwlCarousel;
use themes\globauto\assets\plugins\FancyBoxAsset;
use common\modules\main\models\Article;

//$this->title = $seo->title;
$this->params['breadcrumbs'] = false;
$asset = $asset = GlobAutoAsset::register($this);
$assetOwlCarousel = OwlCarousel::register($this);
$assetFancyBoxAsset = FancyBoxAsset::register($this);

?>

<div class="container">
    <div class="facade_news row">
        <div id="owl-facade-news-list" class="facade_news_list owl-carousel owl-theme">
            <?php foreach (Article::find()->active()->all() as $article) { ?>
                <div class="facade_news_list_block">
                    <?= $this->render('../article/_article', ['model' => $article]); ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="content col-xs-12" itemprop="mainContentOfPage">
            <section class="brands_catalog phone_button">
                <div class="clearfix">
                    <h1 class="title">Каталог автомобилей <?=Yii::$app->geo->titleRp;?></h1>
                    <a href="#" class="more">Все марки</a>
                </div>
                <ul class="brands_catalog_list clearfix">
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                    <li class="brands_catalog_list_item">
                        <a href="#" class="brands_catalog_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Land Rover"
                                 class="brands_catalog_list_item_logotip">
                            <span class="brands_catalog_list_item_name">Land Rover</span>
                        </a>
                    </li>
                </ul>
            </section>

            <ul class="important_list hidden-xs clearfix">
                <li class="important_list_item col-sm-4">
                    <a href="#" class="important_list_item_link">
                        <img src="<?= $asset->baseUrl; ?>/img/review-icon.png" alt="Отзывы на авто"
                             class="important_list_item_icon">
                        <span class="important_list_item_name">Отзывы на авто</span>
                        <span class="important_list_item_meaning">2513 отзывов</span>
                    </a>
                </li>
                <li class="important_list_item col-sm-4">
                    <a href="#" class="important_list_item_link">
                        <img src="<?= $asset->baseUrl; ?>/img/map-icon.png" alt=""
                             class="important_list_item_icon">
                        <span class="important_list_item_name">Автосалоны <?=Yii::$app->geo->titleRp;?></span>
                        <span class="important_list_item_meaning">17 автосалонов</span>
                    </a>
                </li>
                <li class="important_list_item col-sm-4">
                    <a href="#" class="important_list_item_link">
                        <img src="<?= $asset->baseUrl; ?>/img/percentage-icon.png" alt=""
                             class="important_list_item_icon">
                        <span class="important_list_item_name">Акции и скидки</span>
                        <span class="important_list_item_meaning">5 предложений</span>
                    </a>
                </li>
            </ul>

            <section class="new_catalog phone_button">
                <div class="clearfix">
                    <h2 class="title">НОВЫЕ АВТОМОБИЛИ В АВТОСАЛОНАХ <?=Yii::$app->geo->titleRp;?></h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="new_catalog_list clearfix">
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                </ul>
            </section>

            <ul class="interesting_list hidden-xs clearfix">
                <li class="interesting_list_item col-sm-4">
                    <img src="<?= $asset->baseUrl; ?>/img/photo-1.png"
                         alt="Ford EcoSport от 979 000 руб. в наличии в «АВТОФАНе»"
                         class="interesting_list_item_photo">
                    <a href="#" class="interesting_list_item_title">Ford EcoSport от 979 000 руб. в наличии в
                        «АВТОФАНе»</a>
                </li>
                <li class="interesting_list_item col-sm-4">
                    <img src="<?= $asset->baseUrl; ?>/img/photo-1.png"
                         alt="Ford EcoSport от 979 000 руб. в наличии в «АВТОФАНе»"
                         class="interesting_list_item_photo">
                    <a href="#" class="interesting_list_item_title">Ford EcoSport от 979 000 руб. в наличии в
                        «АВТОФАНе»</a>
                </li>
                <li class="interesting_list_item col-sm-4">
                    <img src="<?= $asset->baseUrl; ?>/img/photo-1.png"
                         alt="Ford EcoSport от 979 000 руб. в наличии в «АВТОФАНе»"
                         class="interesting_list_item_photo">
                    <a href="#" class="interesting_list_item_title">Ford EcoSport от 979 000 руб. в наличии в
                        «АВТОФАНе»</a>
                </li>
            </ul>

            <section class="discounts hidden-xs">
                <div class="clearfix">
                    <h2 class="title">акции и скидки на новые автомобили в <?=Yii::$app->geo->titleRp;?></h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="important_list clearfix">
                    <li class="important_list_item col-sm-4">
                        <a href="#" class="important_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Отзывы на авто"
                                 class="important_list_item_icon">
                            <span class="important_list_item_name">Запчасти со скидкой на Mitsubishi</span>
                            <span class="important_list_item_meaning">До 30 июня</span>
                        </a>
                    </li>
                    <li class="important_list_item col-sm-4">
                        <a href="#" class="important_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt=""
                                 class="important_list_item_icon">
                            <span class="important_list_item_name">Запчасти со скидкой
на Mitsubishi</span>
                            <span class="important_list_item_meaning">До 30 июня</span>
                        </a>
                    </li>
                    <li class="important_list_item col-sm-4">
                        <a href="#" class="important_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt=""
                                 class="important_list_item_icon">
                            <span class="important_list_item_name">Запчасти со скидкой
на Mitsubishi</span>
                            <span class="important_list_item_meaning">До 30 июня</span>
                        </a>
                    </li>
                </ul>
            </section>

            <section class="used_catalog hidden-xs">
                <div class="clearfix">
                    <h2 class="title">Автомобили с пробегом в <?=Yii::$app->geo->titleRp;?></h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="used_catalog_list clearfix">
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png"
                             alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                </ul>
            </section>

            <section class="company_new hidden-xs">
                <div class="clearfix">
                    <h2 class="title">НОВЫЕ КОМПАНИИ В КАТАЛОГЕ</h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="important_list clearfix">
                    <li class="important_list_item">
                        <a href="#" class="important_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/new_company.png" alt="Отзывы на авто"
                                 class="important_list_item_icon">
                            <span class="important_list_item_name">JCB</span>
                            <span class="important_list_item_meaning">Спецтехника</span>
                        </a>
                    </li>
                    <li class="important_list_item">
                        <a href="#" class="important_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/new_company.png" alt="Отзывы на авто"
                                 class="important_list_item_icon">
                            <span class="important_list_item_name">JCB</span>
                            <span class="important_list_item_meaning">Спецтехника</span>
                        </a>
                    </li>
                    <li class="important_list_item">
                        <a href="#" class="important_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/new_company.png" alt="Отзывы на авто"
                                 class="important_list_item_icon">
                            <span class="important_list_item_name">JCB</span>
                            <span class="important_list_item_meaning">Спецтехника</span>
                        </a>
                    </li>
                    <li class="important_list_item">
                        <a href="#" class="important_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/new_company.png" alt="Отзывы на авто"
                                 class="important_list_item_icon">
                            <span class="important_list_item_name">JCB</span>
                            <span class="important_list_item_meaning">Спецтехника</span>
                        </a>
                    </li>
                    <li class="important_list_item">
                        <a href="#" class="important_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/new_company.png" alt="Отзывы на авто"
                                 class="important_list_item_icon">
                            <span class="important_list_item_name">JCB</span>
                            <span class="important_list_item_meaning">Спецтехника</span>
                        </a>
                    </li>
                    <li class="important_list_item visible-lg-inline-block">
                        <a href="#" class="important_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/new_company.png" alt="Отзывы на авто"
                                 class="important_list_item_icon">
                            <span class="important_list_item_name">JCB</span>
                            <span class="important_list_item_meaning">Спецтехника</span>
                        </a>
                    </li>
                </ul>
            </section>
            <section class="company hidden-xs">
                <div class="clearfix">
                    <h2 class="title">АВТОМОБИЛЬНЫЕ КОМПАНИИ В <?=Yii::$app->geo->titleRp;?></h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <div class="clearfix">
                    <div class="col-sm-4">
                        <div class="company_subjects">Автоуслуги</div>
                        <ul class="company_subjects_list">
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автомойки</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Авторемонт и
                                    техобслуживание</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автоэкспертиза</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Антикоррозийная
                                    обработка</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Замена масла</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Заправочные станции</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Кузовной ремонт</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Оформление купли-продажи
                                    автомобилей</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Развал / Схождение</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Ремонт автокондиционеров</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Ремонт автоэлектрики</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Ремонт АКПП</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Ремонт грузовых
                                    автомобилей</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Ремонт двигателей</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Ремонт карбюраторов /
                                    инжекторов</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Ремонт спецавтотехники</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Ремонт ходовой части
                                    автомобиля</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Ремонт электронных систем
                                    управления автомобиля</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Технический осмотр
                                    транспорта</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Установка автостёкол</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Шиномонтаж</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="company_subjects">Автотовары</div>
                        <ul class="company_subjects_list">
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автоаксессуары</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автозапчасти</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автозвук</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автосигнализации</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автостекло</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автохимия / Масла</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автоэмали</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Аккумуляторы</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Газовое оборудование</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Запчасти для спецтехники</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Шины / Диски</a>
                            </li>
                        </ul>
                        <div class="company_subjects">Транспортные средства</div>
                        <ul class="company_subjects_list">
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автосалоны</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автосалоны подержанных
                                    авто</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Катера и лодки</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Мототехника</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Спецтехника</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="company_subjects">Перевозки</div>
                        <ul class="company_subjects_list">
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Грузоперевозки</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Заказ спецтехники</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Прокат автотранспорта</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Такси</a>
                            </li>
                        </ul>
                        <div class="company_subjects">Другое</div>
                        <ul class="company_subjects_list">
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автостоянки</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Автошколы</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Гаражные кооперативы</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Оборудование для
                                    автосервиса</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Тонировка</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Тюнинг</a>
                            </li>
                        </ul>
                        <div class="company_subjects">Автоуслуги</div>
                        <ul class="company_subjects_list">
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Страхование</a>
                            </li>
                            <li class="company_subjects_list_item">
                                <a href="#" class="company_subjects_list_item_link">Эвакуация автомобилей</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>

            <ul class="index_photo_list hidden-xs clearfix">
                <li class="index_photo_list_item col-sm-6">
                    <a href="<?= $asset->baseUrl; ?>/img/index_photo.jpg"
                       class="index_photo_list_item_link popup_box2"
                       title="111"
                       rel="index_gallery"
                       style="background:url(<?= $asset->baseUrl; ?>/img/index_photo.jpg) center no-repeat;backgriund-size:cover;"></a>
                </li>
                <li class="index_photo_list_item col-sm-6">
                    <a href="<?= $asset->baseUrl; ?>/img/index_photo.jpg"
                       class="index_photo_list_item_link popup_box2"
                       title="111"
                       rel="index_gallery"
                       style="background:url(<?= $asset->baseUrl; ?>/img/index_photo.jpg) center no-repeat;backgriund-size:cover;"></a>
            </ul>

        </div>
        <div class="sidebar visible-lg">
            <div class="sidebar_banner"><img src="<?= $asset->baseUrl; ?>/img/banner-backing.png" alt=""></div>
            <div class="sidebar_banner"><img src="<?= $asset->baseUrl; ?>/img/banner-backing.png" alt=""></div>
        </div>
    </div>

</div>
