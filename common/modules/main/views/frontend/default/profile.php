<?php
use yii\helpers\Html;

/** @var $this \yii\web\View */
/** @var $user \modules\users\models\User */

?>

<div class="container">
    <div class="row">
        <ul class="breadcrumbs hidden-xs clearfix" itemprop="breadcrumb">
            <li><a href="#">Главная</a></li>
            <li><span>Личный кабинет</span></li>
        </ul>
        <div class="clearfix">
            <h1 class="pagetitle">ЛИЧНАЯ ИНФОРМАЦИЯ</h1>
        </div>
        <div class="clearfix tabs">

            <ul class="tabs-nav col-sm-3 cab_menu" id="cab_menu">
               <li class="button orange">Добавить обьявление</li><!-- 1 -->
                <li class="active">Профиль</li><!-- 2 -->
                <?php if ($user->type == \modules\users\models\User::TYPE_COMPANY) { ?>
                    <li>Компания</li><!-- 6 -->
                <?php } ?>
             <?php /*   <li>Обьявления</li><!-- 3 -->
                <li>Отзывы</li><!-- 4 -->
                <li>Коментарии</li><!-- 5 --> */ ?>
                <li>Настройки</li><!-- 7 -->
                <?= Html::a(
                    Yii::t('app', 'Logout'),
                    ['/user/user/logout'],
                    [
                        'class' => 'logout',
                        'title' => Yii::t('app', 'Logout'),
                        'data' => [
                            'pjax' => 0,
                            'confirm' => 'Вы уверены, что хотите выйти?'
                        ],
                    ]
                ); ?>
            </ul>

            <div class="tabs-box col-xs-12 col-sm-9">

                <div><!-- 1 -->

                    <div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">

                        <div class="longtitle row">Добавление объявления</div>

                        <div class="row">
                            <form class="company">

                                <div class="form_line">
                                    <label class="company_description">Раздел<span class="necessarily">*</span>:</label>
                                    <select class="company_select">
                                        <option value="Легковой автомобиль">Легковой автомобиль</option>
                                        <option value="Грузовой автомобиль">Грузовой автомобиль</option>
                                    </select>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Марка:</label>
                                    <select class="company_select">
                                        <option value="" selected>Выберите марку</option>
                                        <option value="Легковой автомобиль">Легковой автомобиль</option>
                                        <option value="Грузовой автомобиль">Грузовой автомобиль</option>
                                    </select>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Модель:</label>
                                    <select class="company_select">
                                        <option value="" selected>Введите модель</option>
                                        <option value="Легковой автомобиль">Легковой автомобиль</option>
                                        <option value="Грузовой автомобиль">Грузовой автомобиль</option>
                                    </select>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Тип кузова<span class="necessarily">*</span>:</label>
                                    <select class="company_select">
                                        <option value="" selected>Выберите тип кузова</option>
                                        <option value="Легковой автомобиль">Легковой автомобиль</option>
                                        <option value="Грузовой автомобиль">Грузовой автомобиль</option>
                                    </select>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Год выпуска:</label>
                                    <input type="text" class="company_input" placeholder="">
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Пробег:</label>
                                    <input type="text" class="company_input" placeholder="">
                                </div>

                                <div class="form_line">
                                    <label class="company_description">Тип двигателя<span class="necessarily">*</span>:</label>
                                    <ul class="radio_list">
                                        <li class="radio_list_item">
                                            <input id="rfirst" type="radio" name="radio" checked hidden />
                                            <label for="rfirst">Бензин</label>
                                        </li>
                                        <li class="radio_list_item">
                                            <input id="rsecond" type="radio" name="radio" hidden />
                                            <label for="rsecond">Дизель</label>
                                        </li>
                                        <li class="radio_list_item">
                                            <input id="rthird" type="radio" name="radio" hidden />
                                            <label for="rthird">Гибрид</label>
                                        </li>
                                        <li class="radio_list_item">
                                            <input id="rfo" type="radio" name="radio" hidden />
                                            <label for="rfo">Электрический</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Объем двигателя(л.)<span class="necessarily">*</span>:</label>
                                    <input type="text" class="company_input" placeholder="">
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Мощность(л.с.):</label>
                                    <input type="text" class="company_input" placeholder="">
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Тип привода<span class="necessarily">*</span>:</label>
                                    <ul class="radio_list">
                                        <li class="radio_list_item">
                                            <input id="rfirst1" type="radio" name="radio1" checked hidden />
                                            <label for="rfirst1">Передний</label>
                                        </li>
                                        <li class="radio_list_item">
                                            <input id="rsecond1" type="radio" name="radio1" hidden />
                                            <label for="rsecond1">Задний</label>
                                        </li>
                                        <li class="radio_list_item">
                                            <input id="rthird1" type="radio" name="radio1" hidden />
                                            <label for="rthird1">Полный</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Тип трансмиссии<span class="necessarily">*</span>:</label>
                                    <ul class="radio_list">
                                        <li class="radio_list_item">
                                            <input id="rfirst2" type="radio" name="radio2" checked hidden />
                                            <label for="rfirst2">Механическая</label>
                                        </li>
                                        <li class="radio_list_item">
                                            <input id="rsecond2" type="radio" name="radio2" hidden />
                                            <label for="rsecond2">Механическая</label>
                                        </li>
                                        <li class="radio_list_item">
                                            <input id="rthird2" type="radio" name="radio2" hidden />
                                            <label for="rthird2">Роботизировонная</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Расположение руля<span class="necessarily">*</span>:</label>
                                    <ul class="radio_list">
                                        <li class="radio_list_item">
                                            <input id="rfirst3" type="radio" name="radio3" checked hidden />
                                            <label for="rfirst3">Слева</label>
                                        </li>
                                        <li class="radio_list_item">
                                            <input id="rsecond3" type="radio" name="radio3" hidden />
                                            <label for="rsecond3">Справа</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">VIN-код:</label>
                                    <input type="text" class="company_input" placeholder="">
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Цвет:</label>
                                    <div class="filter_color_inner">
                                        <div class="filter_color_inner_box">
                                            <input type="checkbox" id="c1" name="c1">
                                            <label for="c1"></label>
                                        </div>
                                        <div class="filter_color_inner_box">
                                            <input type="checkbox" id="c2" name="c2">
                                            <label for="c2"></label>
                                        </div>
                                        <div class="filter_color_inner_box">
                                            <input type="checkbox" id="c3" name="c3">
                                            <label for="c3"></label>
                                        </div>
                                        <div class="filter_color_inner_box">
                                            <input type="checkbox" id="c4" name="c4">
                                            <label for="c4"></label>
                                        </div>
                                        <div class="filter_color_inner_box">
                                            <input type="checkbox" id="c5" name="c5">
                                            <label for="c5"></label>
                                        </div>
                                        <div class="filter_color_inner_box">
                                            <input type="checkbox" id="c6" name="c6">
                                            <label for="c6"></label>
                                        </div>
                                        <div class="filter_color_inner_box">
                                            <input type="checkbox" id="c7" name="c7">
                                            <label for="c7"></label>
                                        </div>
                                        <div class="filter_color_inner_box">
                                            <input type="checkbox" id="c8" name="c8">
                                            <label for="c8"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Отделка салона<span class="necessarily">*</span>:</label>
                                    <select class="company_select">
                                        <option value="" selected>Выберите отделку салона</option>
                                        <option value="Легковой автомобиль">Легковой автомобиль</option>
                                        <option value="Грузовой автомобиль">Грузовой автомобиль</option>
                                    </select>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Другие сведения:</label>
                                    <textarea class="company_textarea"></textarea>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Добавление фото:</label>
                                    <div class="company_gallery">
                                        <p class="clearfix">Вы можете добавить до 12 фотографий.<br />
                                            Для этого переместите их в поле или воспользуйтесь кнопкой</p>
                                        <div class="clearfix">
                                            <img src="img/10th-photo.png" alt="" class="col-xs-6">
                                            <img src="img/10th-photo.png" alt="" class="col-xs-6">
                                            <img src="img/10th-photo.png" alt="" class="col-xs-6">
                                        </div>
                                        <div class="clearfix">
                                            <a href="#" class="button lilac">Добавить фото</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Город<span class="necessarily">*</span>:</label>
                                    <select class="company_select">
                                        <option value="Уфа">Уфа</option>
                                        <option value="Питер">Питер</option>
                                    </select>
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Контактное лицо:</label>
                                    <input type="text" class="company_input" placeholder="">
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Цена(руб)<span class="necessarily">*</span>:</label>
                                    <input type="text" class="company_input" placeholder="">
                                </div>
                                <div class="form_line">
                                    <label class="company_description">Телефон<span class="necessarily">*</span>:</label>
                                    <div class="pull-right">
                                        <div class="col-xs-6">
                                            <div class="row">
                                                <input type="text" class="company_input" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <a href="#" class="filter_add">Удалить</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="row">
                                                <input type="text" class="company_input" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <a href="#" class="filter_add">Удалить</a>
                                        </div>
                                        <div class="col-xs-6">
                                            <a href="#" class="filter_add">Добавить телефон</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="form_line">
                                    <input type="submit" class="company_submit button lilac" value="разместить обьявление">
                                </div>

                            </form>
                        </div>

                    </div>

                </div><!-- end 1 -->

                <div class="active"><!-- 2 -->

                    <?= $this->render('_user', [
                        'user' => $user
                    ]); ?>

                </div><!-- end 2 -->
                <?php if ($user->type == \modules\users\models\User::TYPE_COMPANY) { ?>
                <div><!-- 6 -->
                    <?= $this->render('_company', [
                        'user' => $user,
                        'userCompanyProfile' => $userCompanyProfile,
                    ]); ?>
                </div><!-- end 6 -->
                <?php } ?>
                <?php /*
                <div><!-- 3 -->
                    <div class="longtitle clearfix">Ваши обьявления</div>
                    <div class="car_model clearfix">
                        <a href="#" class="car_model_button">Все</a>
                        <a href="#" class="car_model_button">Открытые</a>
                        <a href="#" class="car_model_button">Закрытые</a>
                    </div>
                    <ul class="ads_list clearfix">



                        <li class="ads_list_item clearfix">
                            <div class="hidden-xs col-sm-4"><div class="row"><img src="img/photo.png" alt="" class="ads_list_item_photo"></div></div>
                            <div class="col-xs-9 col-sm-6">
                                <div class="ads_list_item_title">
                                    Audi A6  Уфа
                                    <span class="ads_list_item_cost">1 380 000 руб. </span>
                                </div>
                                <div class="ads_list_item_control">
                                    <a href="#" class="ads_list_item_control_main">Редактировать </a>
                                    <div class="ads_list_item_control_more">
                                        <a href="#" class="ads_list_item_control_more_button">Еще</a>
                                        <ul class="control_more_list">
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Активировать обьявление
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Редактировать
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Сделать приоритетным
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Сделать VIP-обьявлением
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Снятьс продажи
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Сделать выделенным
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Статистика просмотров
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="ads_list_item_duration">
                                    <div class="ads_list_item_duration_visually">
                                        <div class="ads_list_item_duration_visually_inner"></div>
                                    </div>
                                    <div class="ads_list_item_duration_left">
                                        Осталось 29 дней
                                        <a href="#" class="ads_list_item_duration_extend">Продлить</a>
                                    </div>
                                </div>

                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <a href="#" class="ads_list_item_pick_up">Поднять</a>
                                <div class="ads_list_item_clarification" title="Пояснение Пояснение Пояснение
Пояснение Пояснение Пояснение Пояснение
Пояснение Пояснение Пояснение"></div>
                            </div>
                        </li>



                        <li class="ads_list_item clearfix">
                            <div class="hidden-xs col-sm-4"><div class="row"><img src="img/photo.png" alt="" class="ads_list_item_photo"></div></div>
                            <div class="col-xs-9 col-sm-6">
                                <div class="ads_list_item_title">
                                    Audi A6  Уфа
                                    <span class="ads_list_item_cost">1 380 000 руб. </span>
                                </div>
                                <div class="ads_list_item_control">
                                    <a href="#" class="ads_list_item_control_main">Редактировать </a>
                                    <div class="ads_list_item_control_more">
                                        <a href="#" class="ads_list_item_control_more_button">Еще</a>
                                        <ul class="control_more_list">
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Активировать обьявление
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Редактировать
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Сделать приоритетным
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Сделать VIP-обьявлением
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Снятьс продажи
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Сделать выделенным
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Статистика просмотров
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="ads_list_item_duration">
                                    <div class="ads_list_item_duration_visually">
                                        <div class="ads_list_item_duration_visually_inner"></div>
                                    </div>
                                    <div class="ads_list_item_duration_left">
                                        Осталось 29 дней
                                        <a href="#" class="ads_list_item_duration_extend">Продлить</a>
                                    </div>
                                </div>

                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <a href="#" class="ads_list_item_pick_up">Поднять</a>
                                <div class="ads_list_item_clarification" title="Пояснение Пояснение Пояснение
Пояснение Пояснение Пояснение Пояснение
Пояснение Пояснение Пояснение"></div>
                            </div>
                        </li>



                        <li class="ads_list_item clearfix">
                            <div class="hidden-xs col-sm-4"><div class="row"><img src="img/photo.png" alt="" class="ads_list_item_photo"></div></div>
                            <div class="col-xs-9 col-sm-6">
                                <div class="ads_list_item_title">
                                    Audi A6  Уфа
                                    <span class="ads_list_item_cost">1 380 000 руб. </span>
                                </div>
                                <div class="ads_list_item_control">
                                    <a href="#" class="ads_list_item_control_main">Редактировать </a>
                                    <div class="ads_list_item_control_more">
                                        <a href="#" class="ads_list_item_control_more_button">Еще</a>
                                        <ul class="control_more_list">
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Активировать обьявление
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Редактировать
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Сделать приоритетным
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Сделать VIP-обьявлением
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Снятьс продажи
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Сделать выделенным
                                                </a>
                                            </li>
                                            <li class="control_more_list_item">
                                                <a href="#" class="control_more_list_item_link">
                                                    Статистика просмотров
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="ads_list_item_duration">
                                    <div class="ads_list_item_duration_visually">
                                        <div class="ads_list_item_duration_visually_inner"></div>
                                    </div>
                                    <div class="ads_list_item_duration_left">
                                        Осталось 29 дней
                                        <a href="#" class="ads_list_item_duration_extend">Продлить</a>
                                    </div>
                                </div>

                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <a href="#" class="ads_list_item_pick_up">Поднять</a>
                                <div class="ads_list_item_clarification" title="Пояснение Пояснение Пояснение
Пояснение Пояснение Пояснение Пояснение
Пояснение Пояснение Пояснение"></div>
                            </div>
                        </li>



                    </ul>
                </div><!-- end 3 -->

                <div><!-- 4 -->Отзывы</div><!-- end 4 -->

                <div><!-- 5 -->
                    <div class="longtitle row">Ваши коментарии</div>
                    <div class="comments_list col-xs-12">

                        <div class="row comments_inner">
                            <div class="col-sm-12"><a href="#" class="comments_location">Статья 1</a></div>
                            <div class="col-sm-2 col-lg-1 hidden-xs">
                                <img src="http://placehold.it/75x75" alt="" class="comments_autor_photo">
                            </div>
                            <div class="col-sm-10 col-lg-11">
                                <div class="comments_block">
                                    <span class="comments_autor_name">vy2bshilov</span>
                                    <span class="comments_date">10 июня в 00:55</span>
                                </div>
                                <div class="comments_block">
                                    <p>
                                        F вы знали, что 80% людей заражены паразитами и токсинами, вот почему мы так быстро устаем, стареем и часто болеем. Но теперь можно легко очистить свой организм от этой заразы. Вот почитайте чем грозит пренебрежение очищением организма-- <a href="http://zumlink.de/Bacteforte">http://zumlink.de/Bacteforte</a>
                                    </p>
                                </div>
                                <div class="comments_block">
												<span class="comments_rating">
													<span class="comments_rating_worse"> <span> + </span> </span>
													<span class="comments_rating_position comments_rating_position_positive">+4</span>
													<span class="comments_rating_better"> <span> - </span> </span>
												</span>
                                    <a class="comments_reply" href="#"><img src="img/pencil-icon.png" alt="Редактировать"><span>Редактировать</span></a>
                                    <a class="comments_reply" href="#"><img src="img/paper-bucket-icon.png" alt="Удалить"><span>Удалить</span></a>
                                </div>
                            </div>
                        </div>

                        <div class="row comments_inner">
                            <div class="col-sm-12"><a href="#" class="comments_location">Статья 1</a></div>
                            <div class="col-sm-2 col-lg-1 hidden-xs">
                                <img src="http://placehold.it/75x75" alt="" class="comments_autor_photo">
                            </div>
                            <div class="col-sm-10 col-lg-11">
                                <div class="comments_block">
                                    <span class="comments_autor_name">vy2bshilov</span>
                                    <span class="comments_date">10 июня в 00:55</span>
                                </div>
                                <div class="comments_block">
                                    <p>
                                        F вы знали, что 80% людей заражены паразитами и токсинами, вот почему мы так быстро устаем, стареем и часто болеем. Но теперь можно легко очистить свой организм от этой заразы. Вот почитайте чем грозит пренебрежение очищением организма-- <a href="http://zumlink.de/Bacteforte">http://zumlink.de/Bacteforte</a>
                                    </p>
                                </div>
                                <div class="comments_block">
												<span class="comments_rating">
													<span class="comments_rating_worse"> <span> + </span> </span>
													<span class="comments_rating_position comments_rating_position_positive">+4</span>
													<span class="comments_rating_better"> <span> - </span> </span>
												</span>
                                    <a class="comments_reply" href="#"><img src="img/pencil-icon.png" alt="Редактировать"><span>Редактировать</span></a>
                                    <a class="comments_reply" href="#"><img src="img/paper-bucket-icon.png" alt="Удалить"><span>Удалить</span></a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div><!-- end 5 -->
                */ ?>
                <div><!-- 7 -->

                    <div class="longtitle clearfix">Настройки аккаунта</div>


                    <div class="row">
                        <div class="col-sm-9">
                            <?= $this->render('_reset_password', [
                                'user' => $user
                            ]); ?>
                        </div>
                        <div class="col-sm-3">
                            <img src="img/photo2.png" alt="" class="account_photo">
                        </div>
                    </div>
                </div><!-- end 7 -->
            </div>
        </div>
    </div>
</div>
