<?php
use yii\helpers\Html;
use modules\geo\models\GeoCities;
use yii\widgets\ActiveForm;
use modules\users\models\User;

/** @var $this \yii\web\View */
/** @var $user User */
/** @var $userCompanyProfile \modules\users\models\UserCompanyProfile */

/*
?>





<?= $form->field($user, 'phone', ['options' => ['class' => 'form_line']])
    ->textInput(['class' => 'company_input']); ?>

<?= $form->field($user, 'sex', ['options' => ['class' => 'form_line']])
    ->dropDownList(User::getSexArray(), ['class' => 'company_input', 'prompt' => '-- select --']); ?>

<?= $form->field($user, 'type', ['options' => ['class' => 'form_line']])
    ->dropDownList(User::getTypeArray(), ['class' => 'company_input']); ?>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), [
        'class' => 'company_submit button lilac',
        'name' => 'profile-button',
    ]); ?>
</div>
*/ ?>

<div class="col-xs-12 col-sm-11 col-md-10 col-lg-9">

    <div class="longtitle row">Ваша компания</div>

    <div class="row">
        <?php $form = ActiveForm::begin(['class' => 'company']); ?>
            <div class="form_line">
                <?=Html::activeLabel($userCompanyProfile, 'title', ['class' => 'company_description']);?>
                <?= $form->field($userCompanyProfile, 'title')->textInput([
                    'class' => 'company_input'
                ])->label(false); ?>
            </div>
            <div class="form_line">
                <?=Html::activeLabel($userCompanyProfile, 'cityID', ['class' => 'company_description']);?>
                <?= $form->field($userCompanyProfile, 'cityID')->dropDownList(GeoCities::arrayList(), [
                    'class' => 'company_select',
                    'prompt' => '--select--'
                ])->label(false); ?>
            </div>
            <div class="form_line">
                <?=Html::activeLabel($userCompanyProfile, 'address', ['class' => 'company_description']);?>
                <?= $form->field($userCompanyProfile, 'address')->textInput([
                    'class' => 'company_input',
                    'placeholder' => 'ул. Шоссейная, 46'
                ])->label(false); ?>
            </div>
            <div class="form_line">
                <?=Html::activeLabel($userCompanyProfile, 'service', ['class' => 'company_description']);?>
                <?= $form->field($userCompanyProfile, 'service')->dropDownList(['Ремонт авто', 'Ремонт авто2'], [
                    'class' => 'company_select',
                    'prompt' => '--select--'
                ])->label(false); ?>
            </div>
            <div class="form_line">
                <?=Html::activeLabel($userCompanyProfile, 'category', ['class' => 'company_description']);?>
                <?= $form->field($userCompanyProfile, 'category')->dropDownList(['Кузовной авто', 'Кузовной авто2'], [
                    'class' => 'company_select',
                    'prompt' => '--select--'
                ])->label(false); ?>
            </div>
            <?php /*
            <div class="form_line">
                <div class="company_description">Время работы:</div>
                <div class="col-xs-12 col-sm-6">
                    <div class="row">
                        <label class="company_description">ПН-ПТ:</label>
                        <input type="text" class="company_input company_input_border" placeholder="9:00 — 18:00">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="row">
                        <label class="company_description">СБ-ВС:</label>
                        <input type="text" class="company_input" placeholder="10:00 — 15:00">
                    </div>
                </div>
            </div>*/ ?>
            <div class="form_line">
                <?=Html::activeLabel($userCompanyProfile, 'text', ['class' => 'company_description']);?>
                <?= $form->field($userCompanyProfile, 'text')->textarea([
                    'class' => 'company_textarea',
                    'placeholder' => '«Автосервис» позаботится о вашем автомобиле и проведет весь комплекс технических мероприятий с целью выявить неполадки. У нас работают профессиональные автомастера, они имеют большой опыт работы с автомобилями как отечественных, так и зарубежных марок."'
                ])->label(false); ?>
            </div>
            <div class="form_line">
                <?=Html::activeLabel($userCompanyProfile, 'website', ['class' => 'company_description']);?>
                <?= $form->field($userCompanyProfile, 'website')->textInput([
                    'class' => 'company_input',
                    'placeholder' => 'www.site.ru'
                ])->label(false); ?>
            </div>
            <div class="form_line">
                <?=Html::activeLabel($userCompanyProfile, 'email', ['class' => 'company_description']);?>
                <?= $form->field($userCompanyProfile, 'email')->textInput([
                    'class' => 'company_input',
                    'placeholder' => 'info@site.ru'
                ])->label(false); ?>
            </div>
            <div class="form_line">
                <?=Html::activeLabel($userCompanyProfile, 'phone', ['class' => 'company_description']);?>
                <?= $form->field($userCompanyProfile, 'phone')->textInput([
                    'class' => 'company_input',
                    'placeholder' => '8 (8482) 78-34-86'
                ])->label(false); ?>
            </div>

            <div class="form_line">
                <input type="submit" class="company_submit button lilac" value="Сохранить изменения">
            </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>