<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\main\models\Article */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Article',
]) . $model->article_id;
?>
<div class="article-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'seo' => $seo,
    ]) ?>

</div>
