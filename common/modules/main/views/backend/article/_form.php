<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\ck\CKEditor;
use yii\helpers\Url;
use common\modules\main\models\ArticleCategory;
use common\components\taggable\TagEditor;

/* @var $this yii\web\View */
/* @var $seo \modules\seo\models\Seo */
/* @var $model common\modules\main\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin([
        'action' => $model->isNewRecord?Url::to(['create']):Url::to(['update', 'id' => $model->article_id]),
        'id' => 'form-ajax',
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->errorSummary($seo) ?>
    <?= $form->errorSummary($model) ?>
    <?= $form->field($seo, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'article_category_id')->dropDownList(ArticleCategory::arrayList()) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($seo, 'h1')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($seo, 'h2')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <div>
        <?php if (!$model->isNewRecord && !empty($model->image)) {
            echo Html::img($model->image, ['style'=> 'width:320px;']);
        } ?>
    </div>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?= $form->field($seo, 'breadcrumb')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($seo, 'seo_key')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($seo, 'seo_desc')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <?= $form->field($seo, 'body')->widget(CKEditor::className(), [
        'preset' => 'full',
        'clientOptions' => [
            'filebrowserUploadUrl' => Url::to(['/ckloader'])
        ]
    ]) ?>

    <?= $form->field($seo, 'footer')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <?= $form->field($model, 'editorTags')->widget(TagEditor::className(), [
        'id' => 'tags_ajax_test',
        'tagEditorOptions' => [
            'autocomplete' => [
                'source' => Url::toRoute(['tag/suggest'])
            ],
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
