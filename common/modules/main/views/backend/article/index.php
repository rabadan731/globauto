<?php

use yii\helpers\Html;
use common\assets\AjaxFormAsset;
use yii\grid\GridView;
use common\modules\main\models\ArticleCategory;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\main\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Articles');
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="article-index">
    <div class="row">
        <div class="col-md-5">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'article_id',
            [
                'attribute' => 'article_category_id',
                'filter' => ArticleCategory::arrayList(),
                'value' => function($data) {
                    if (!empty($data->category)) {
                        return $data->category->seo->title;
                    }
                    return 'no-category';
                }
            ],
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($data) {
                    if (!empty($data->image)) {
                        return Html::img($data->image, ['style' => 'max-width: 200px; height: 80px;']);
                    }
                    return 'no-image';
                }
            ],
            'status',
            'slug',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{ajax-update} {delete}',
                'options' => ['style' => 'width:70px']
            ],
        ],
    ]); ?>
        </div>
        <div class="col-md-7">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
