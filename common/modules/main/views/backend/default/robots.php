<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\main\models\KeyStorageItem */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'robots.txt';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="key-storage-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'value')->textarea(['rows' => 6]) ?>

    <p>
        <span>{host}</span> - Текущий домен
    </p>

    <div class="form-group">
        <?= Html::submitButton(Yii::t("app", "Save"), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>