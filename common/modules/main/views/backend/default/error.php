<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>

<div class="middle-box text-center animated fadeInDown">
    <h1><?=$exception->statusCode;?></h1>
    <h3 class="font-bold"><?= $name; ?></h3>

    <div class="error-desc">
        <?= $message; ?>
    </div>
</div>
<br />
<br />
<br />