<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\main\models\ArticleCategory */

$this->title = Yii::t('app', 'Article Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Article Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Add');
?>
<div class="article-category-create">

    <?= $this->render('_form', [
        'model' => $model,
        'seo' => $seo,
    ]) ?>

</div>
