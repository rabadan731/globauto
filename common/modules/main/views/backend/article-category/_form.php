<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\ck\CKEditor;
use yii\widgets\ActiveForm;
use modules\seo\models\Seo;

/* @var $this yii\web\View */
/* @var $model common\modules\main\models\ArticleCategory */
/* @var $seo Seo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-category-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <?= $form->field($seo, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($seo, 'h1')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($seo, 'h2')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($seo, 'breadcrumb')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($seo, 'seo_key')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($seo, 'seo_desc')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <?= $form->field($seo, 'body')->widget(CKEditor::className(), [
        'preset' => 'full',
        'clientOptions' => [
            'filebrowserUploadUrl' => Url::to(['/ckloader'])
        ]
    ]) ?>

    <?= $form->field($seo, 'footer')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
