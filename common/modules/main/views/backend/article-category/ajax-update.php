<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\main\models\ArticleCategory */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Article Category',
]) . $model->article_category_id;
?>
<div class="article-category-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'seo' => $seo,
    ]) ?>

</div>
