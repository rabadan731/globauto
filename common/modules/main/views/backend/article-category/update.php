<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\main\models\ArticleCategory */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Article Category',
]) . $model->article_category_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Article Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->article_category_id, 'url' => ['view', 'id' => $model->article_category_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="article-category-update">
    <?= $this->render('_form', [
        'model' => $model,
        'seo' => $seo,
    ]) ?>
</div>
