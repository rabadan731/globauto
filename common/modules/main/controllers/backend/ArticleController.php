<?php

namespace modules\main\controllers\backend;

use modules\seo\models\Seo;
use Yii;
use common\modules\main\models\Article;
use common\modules\main\models\search\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();
        $seo = new Seo();
        $seo->controller = Article::SEO_CONTROLLER;
        $seo->status_edit = Seo::EDIT_STATUS_NO;

        if ($model->load(Yii::$app->request->post())) {
            $seo->load(Yii::$app->request->post());
            if (empty($model->slug)) {
                $model->setSlug($seo->title);
            }
            $seo->action = $model->slug;

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->uploadImage();

            if ($model->save()) {
                $seo->save();
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'seo' => $seo,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $seo = $model->getSeoModel();

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->uploadImage();
            $seo->load(Yii::$app->request->post());
            if (empty($model->slug)) {
                $model->setSlug($seo->title);
            }
            $seo->action = $model->slug;

            if ($model->save()) {
                $seo->save();
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'seo' => $seo,
        ]);
    }

    /**
     * @param $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionAjaxUpdate($id)
    {
        $model = $this->findModel($id);
        $seo = $model->getSeoModel();

        if (Yii::$app->request->isAjax) {

            $post = Yii::$app->request->post("Article", null);

            if (is_null($post)) {
                return $this->renderAjax('ajax-update', [
                    'model' => $model,
                    'seo' => $seo,
                ]);
            }

//            Yii::$app->response->format = Response::FORMAT_JSON;
//            $status = false;
//            if ($model->load(Yii::$app->request->post())) {
//
//                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
//                $model->uploadImage();
//
//                $seo->load(Yii::$app->request->post());
//                if (empty($model->slug)) {
//                    $model->setSlug($seo->title);
//                }
//                $seo->action = $model->slug;
//
//                $status = ($model->save() && $seo->save());
//            }
//
//            $res = array(
//                'success' => $status,
//                'statusText' => $status?Yii::t('app', 'Saved'):Yii::t('app', 'Errors'),
//                'errors' => $model->errors
//            );
//            return $res;
        }

        throw new NotFoundHttpException('The requested page does not exist. Please Ajax.');
    }




    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
