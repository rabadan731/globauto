<?php

namespace modules\main\controllers\backend;

use yii\web\Controller;
use common\modules\main\models\Tag;
use common\components\taggable\TagSuggestAction;

class TagController extends Controller	{

    public function actions()    {
        return [
            'suggest' => [
                'class' => TagSuggestAction::className(),
                'tagClass' => Tag::className(),
            ],
        ];
    }
}