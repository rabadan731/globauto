<?php

namespace modules\main\controllers\backend;

use Yii;
use backend\components\BackendController;
use common\modules\main\models\KeyStorageItem;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 * Class DefaultController
 * @package modules\main\controllers\backend
 */
class DefaultController extends BackendController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'modules\main\components\ErrorAction',
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRobots()
    {
        $key = 'robots.txt';
        KeyStorageItem::getValue($key);
        $model = KeyStorageItem::find()->andWhere(['key' => $key])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        }

        return $this->render('robots', [
            'model' => $model
        ]);
    }

    /**
     * @param string $dir
     */
    public function actionCkload($dir = "ckimages")
    {
        //Создаем объект загрузки
        $uploadedFile = UploadedFile::getInstanceByName('upload');

        //Тип изображения
        $mime = \yii\helpers\FileHelper::getMimeType($uploadedFile->tempName);

        //Генерируем имя изображения
        $file_name = Inflector::slug($uploadedFile->baseName) . "_" . time() . ".{$uploadedFile->extension}";

        // системный урл
        $file_folder = Yii::getAlias("@storage") . DIRECTORY_SEPARATOR . $dir;

        //публичный урл
        $url = "/files/{$dir}/{$file_name}";

        //создаем папку, если таковой нет
        FileHelper::createDirectory($file_folder);

        //полный путь нового файла
        $uploadPath = $file_folder . DIRECTORY_SEPARATOR . $file_name;

        //обширная проверка пригодности, прежде чем делать что-нибудь с файлом ...
        if ($uploadedFile == null) {
            $message = Yii::t("common", "No file uploaded.");
        } else if ($uploadedFile->size == 0) {
            $message = Yii::t("common", "The file is of zero length.");
        } else if ($mime != "image/jpeg" && $mime != "image/png") {
            $message = Yii::t("common", "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.");
        } else if ($uploadedFile->tempName == null) {
            $message = Yii::t("common", "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.");
        } else {
            //если все ок, то грузим файл
            $message = "";
            $move = $uploadedFile->saveAs($uploadPath);
            if (!$move) {
                $message = Yii::t("common", "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.");
            }
        }
        $funcNum = $_GET['CKEditorFuncNum'];
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction({$funcNum}, '$url', '{$message}');</script>";
    }


}