<?php

namespace modules\main\controllers\frontend;

use modules\seo\models\Seo;
use Yii;
use common\modules\main\models\Article;
use common\modules\main\models\search\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param string $slug
     * @return mixed
     */
    public function actionView($slug)
    {
        $model = $this->findModel($slug);
        $model->seo->setMetaTags();
        $articles = Article::find()
            ->active()
            ->andWhere(['<>', 'article_id', $model->article_id])
            ->all();

        return $this->render('view', [
            'model' => $model,
            'articles' => $articles,
        ]);
    }


    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = Article::find()->andWhere(['slug'=>$slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
