<?php

namespace modules\main\controllers\frontend;

use modules\users\models\User;
use modules\users\Module;
use Yii;
use frontend\components\FrontendController;
use common\modules\main\models\KeyStorageItem;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * Class DefaultController
 * @package modules\main\controllers\frontend
 */
class DefaultController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['error', 'index', 'robots'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['profile'],
                        'roles' => ['@'],
                    ],
                ],
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'seo' => []
        ]);
    }


    /**
     * Файл robots.txt
     * @return string
     */
    public function actionRobots()
    {
        $host = Yii::$app->request->serverName;
        $host = preg_replace("/w+\./", '', $host);
        $host = str_replace("http://", "", $host);

        header("Content-Type: text/plain");

        $replace = [
            "{host}" => $host,
        ];

        $robots = KeyStorageItem::getValue('robots.txt', null, "Host: {host}");
        $robots = str_replace(array_keys($replace), array_values($replace), $robots);

        echo $robots;
        exit();
    }


    public function actionProfile()
    {
        Module::registerTranslations();
        $user = Yii::$app->user->identity;
        $userCompanyProfile = null;

        /** @var User $user */
        if ($user->load(Yii::$app->request->post()) && $user->save()) {
            Yii::$app->getSession()->addFlash('success', Yii::t('users', 'CHANGES_WERE_SAVED'));
            return $this->refresh();
        }

        if ($user->type == User::TYPE_COMPANY) {
            $userCompanyProfile = $user->loadCompanyProfile();
            if ($userCompanyProfile->load(Yii::$app->request->post()) && $userCompanyProfile->validate()) {
                $userCompanyProfile->update();
                Yii::$app->getSession()->addFlash('success', Yii::t('users', 'CHANGES_WERE_SAVED'));
                return $this->refresh();
            }
        }

        $seo = $this->getSeo('Мой профиль');


        return $this->render('profile', [
            'user' => $user,
            'userCompanyProfile' => $userCompanyProfile,
            'seo' => $seo,
        ]);
    }
}
