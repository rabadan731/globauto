<?php

namespace common\modules\main\models;

use modules\seo\models\Seo;
use Yii;
use common\modules\main\models\query\ArticleQuery;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use common\components\taggable\TaggableBehavior;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "article".
 *
 * @property integer $article_id
 * @property integer $article_category_id
 * @property string $image
 * @property integer $status
 * @property string $slug
 *
 * @property ArticleCategory $articleCategory
 */
class Article extends \yii\db\ActiveRecord
{
    const SEO_CONTROLLER = 'main/article-category';

    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function behaviors()
    {
        return [
            'taggable' => [
                'class' => TaggableBehavior::className(),
                'tagClass' => Tag::className(),
                'junctionTable' => 'article_tag',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_category_id', 'status'], 'integer'],
            [['image'], 'string', 'max' => 1024],
            [['slug'], 'string', 'max' => 512],
            [
                ['article_category_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ArticleCategory::className(),
                'targetAttribute' => ['article_category_id' => 'article_category_id']
            ],
            [['editorTags'], 'safe'],
            [['imageFile'], 'safe'],
        ];
    }

    /**
     * @return bool
     */
    public function uploadImage()
    {
        if ($this->validate() && !empty($this->imageFile)) {
            $nameFile = md5($this->imageFile->baseName.time().rand());
            $nameFileFull = "{$nameFile}.{$this->imageFile->extension}";
            $path = Yii::getAlias("@storage");
            $dir = "articles";
            FileHelper::createDirectory("{$path}/{$dir}");

            $this->imageFile->saveAs("${path}/{$dir}/{$nameFileFull}");
            $this->image = "/files/{$dir}/{$nameFileFull}";
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'article_id' => Yii::t('app', 'ID'),
            'article_category_id' => Yii::t('app', 'Category ID'),
            'category' => Yii::t('app', 'Category'),
            'category.title' => Yii::t('app', 'Category'),
            'image' => Yii::t('app', 'Image'),
            'status' => Yii::t('app', 'Status'),
            'slug' => Yii::t('app', 'Slug'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ArticleCategory::className(), [
            'article_category_id' => 'article_category_id'
        ]);
    }

    /**
     * @inheritdoc
     * @return ArticleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticleQuery(get_called_class());
    }

    /**
     * @param $text
     */
    public function setSlug($text)
    {
        $this->slug = Inflector::slug($text);
    }

    /**
     * @return Seo
     */
    public function getSeoModel()
    {
        return Seo::find_or_create(
            self::SEO_CONTROLLER,
            $this->slug,
            $this->slug,
            null,
            Seo::EDIT_STATUS_NO
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeo()
    {
        return $this->hasOne(Seo::className(), ['action' => 'slug'])
            ->andWhere(['controller' => self::SEO_CONTROLLER]);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Url::to(['/main/article/view', 'slug' => $this->slug]);
    }


}
