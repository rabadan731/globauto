<?php

namespace common\modules\main\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\main\models\Article]].
 *
 * @see \common\modules\main\models\Article
 */
class ArticleQuery extends \yii\db\ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['status' => 1]);
    }

    /**
     * @inheritdoc
     * @return \common\modules\main\models\Article[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\main\models\Article|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
