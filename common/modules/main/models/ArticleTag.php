<?php

namespace common\modules\main\models;

use Yii;

/**
 * This is the model class for table "article_tag".
 *
 * @property integer $model_id
 * @property integer $tag_id
 * @property integer $ord
 */
class ArticleTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_id', 'tag_id'], 'required'],
            [['model_id', 'tag_id', 'ord'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('app', 'Model ID'),
            'tag_id' => Yii::t('app', 'Tag ID'),
            'ord' => Yii::t('app', 'Ord'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\modules\main\models\query\ArticleTagQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\main\models\query\ArticleTagQuery(get_called_class());
    }
}
