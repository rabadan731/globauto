<?php

namespace modules\main;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package modules\main
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $rules = [];
        $rules[''] = 'main/default/index';

        if (Yii::$app->id == 'app-backend') {
            $rules['main/robots'] = 'main/default/robots';
            $rules['ckloader'] = 'main/default/ckload';
        }

        if (Yii::$app->id == 'app-frontend') {
            $rules['robots.txt'] = 'main/default/robots';
            $rules['profile'] = 'main/default/profile';
            $rules['article'] = 'main/article/index';
            $rules['article/<slug>'] = 'main/article/view';
        }

        $app->urlManager->addRules($rules, false);
    }
}
