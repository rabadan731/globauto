<?php

use yii\db\Migration;

class m171026_194517_auto_catalog extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%auto_catalog}}', [
            'auto_catalog_id'   => $this->primaryKey(),
            'slug'              => $this->string(512)->notNull(),
            'country_id'        => $this->integer(),
            'type_body_id'      => $this->integer(),
            'engine_id'         => $this->integer(),
            'gear_id'           => $this->integer(),
            'transmission_id'   => $this->integer(),
            'color'             => $this->string(256),
            'price_start'       => $this->integer()->unsigned(),
            'price_end'         => $this->integer()->unsigned(),
            'run_start'         => $this->integer()->unsigned(),
            'run_end'           => $this->integer()->unsigned(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%auto_catalog}}');
    }
}
