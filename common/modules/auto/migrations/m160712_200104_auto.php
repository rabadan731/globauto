<?php

use yii\db\Migration;
use yii\helpers\Console;

class m160712_200104_auto extends Migration
{

    public function safeUp()
    {
        if (!$this->existAutoBase()) {
            $red = Console::renderColoredString("%R ********************************************************");
            Console::output("\n{$red}");
            Console::output(" *** No car base in DB. Please load DB your AUTO BASE ***");
            Console::output("{$red}\n");
            return false;
        }

        $this->addColumn('{{%car_mark}}','slug',$this->string(256));
        $this->addColumn('{{%car_mark}}','active', $this->integer(1)->defaultValue(1));

        $find = \modules\auto\models\CarMark::find();
        \yii\helpers\Console::output(" > GENERATE SLUG MARKS");
        foreach ($find->all() as $item) {
            $item->save();
        }


        $this->addColumn('{{%car_model}}','slug',$this->string(256));
        $this->addColumn('{{%car_model}}','active', $this->integer(1)->defaultValue(1));
        \yii\helpers\Console::output(" > GENERATE SLUG MODELS");
        foreach (\modules\auto\models\CarModel::find()->all() as $item) {
            $item->save();
        }

        $this->addForeignKey('frx_car_model_id_car_type', '{{%car_model}}', 'id_car_type', '{{%car_type}}',  'id_car_type', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_model_id_car_mark', '{{%car_model}}', 'id_car_mark', '{{%car_mark}}',  'id_car_mark', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_mark_id_car_type', '{{%car_mark}}', 'id_car_type', '{{%car_type}}',  'id_car_type', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_characteristic_id_car_type', '{{%car_characteristic}}', 'id_car_type', '{{%car_type}}',  'id_car_type', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_characteristic_value_id_car_type', '{{%car_characteristic_value}}', 'id_car_type', '{{%car_type}}',  'id_car_type', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_characteristic_value_id_car_characteristic', '{{%car_characteristic_value}}', 'id_car_characteristic', '{{%car_characteristic}}',  'id_car_characteristic', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_generation_id_car_type', '{{%car_generation}}', 'id_car_type', '{{%car_type}}',  'id_car_type', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_generation_id_car_model', '{{%car_generation}}', 'id_car_model', '{{%car_model}}',  'id_car_model', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_serie_id_car_type',  '{{%car_serie}}', 'id_car_type', '{{%car_type}}',  'id_car_type', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_serie_id_car_model', '{{%car_serie}}', 'id_car_model', '{{%car_model}}',  'id_car_model', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_modification_id_car_type', '{{%car_modification}}', 'id_car_type', '{{%car_type}}',  'id_car_type', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_modification_id_car_serie', '{{%car_modification}}', 'id_car_serie', '{{%car_serie}}',  'id_car_serie', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_modification_id_car_model', '{{%car_modification}}', 'id_car_model', '{{%car_model}}',  'id_car_model', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_characteristic_value_id_car_modification', '{{%car_characteristic_value}}', 'id_car_modification', '{{%car_modification}}',  'id_car_modification', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_option_id_car_type', '{{%car_option}}', 'id_car_type', '{{%car_type}}',  'id_car_type', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_option_value_id_car_type', '{{%car_option_value}}', 'id_car_type', '{{%car_type}}',  'id_car_type', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_option_value_id_car_option', '{{%car_option_value}}', 'id_car_option', '{{%car_option}}',  'id_car_option', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_option_value_id_car_equipment', '{{%car_option_value}}', 'id_car_equipment', '{{%car_equipment}}',  'id_car_equipment', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_equipment_id_car_type', '{{%car_equipment}}', 'id_car_type', '{{%car_type}}',  'id_car_type', 'CASCADE', 'CASCADE');
        $this->addForeignKey('frx_car_equipment_id_car_modification', '{{%car_equipment}}', 'id_car_modification', '{{%car_modification}}',  'id_car_modification', 'CASCADE', 'CASCADE');

        // Если у нас есть в базе RBAC
        if ($this->existRBAC()) {

            $this->batchInsert('{{%auth_item}}', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
                ['auto', 2, "Управление Авто модулем", NULL, time(), time()],
            ]);

            $this->batchInsert('{{%auth_item_child}}', ['parent', 'child'], [
                ['administrator', 'auto'],
            ]);
        }

    }

    public function safeDown()
    {
        $this->dropColumn('{{%car_mark}}','slug');
        $this->dropColumn('{{%car_mark}}','active');
        $this->dropColumn('{{%car_model}}','slug');
        $this->dropColumn('{{%car_model}}','active');

        $this->dropForeignKey('frx_car_serie_id_car_model', '{{%car_serie}}');
        $this->dropForeignKey('frx_car_serie_id_car_type', '{{%car_serie}}');
        $this->dropForeignKey('frx_car_model_id_car_type', '{{%car_model}}');
        $this->dropForeignKey('frx_car_model_id_car_mark', '{{%car_model}}');
        $this->dropForeignKey('frx_car_characteristic_id_car_type', '{{%car_characteristic}}');
        $this->dropForeignKey('frx_car_characteristic_value_id_car_modification', '{{%car_characteristic_value}}');
        $this->dropForeignKey('frx_car_characteristic_value_id_car_characteristic', '{{%car_characteristic_value}}');
        $this->dropForeignKey('frx_car_characteristic_value_id_car_type', '{{%car_characteristic_value}}');
        $this->dropForeignKey('frx_car_generation_id_car_model', '{{%car_generation}}');
        $this->dropForeignKey('frx_car_generation_id_car_type', '{{%car_generation}}');
        $this->dropForeignKey('frx_car_modification_id_car_model', '{{%car_modification}}');
        $this->dropForeignKey('frx_car_modification_id_car_serie', '{{%car_modification}}');
        $this->dropForeignKey('frx_car_modification_id_car_type', '{{%car_modification}}');
        $this->dropForeignKey('frx_car_option_id_car_type', '{{%car_option}}');
        $this->dropForeignKey('frx_car_option_value_id_car_equipment', '{{%car_option_value}}');
        $this->dropForeignKey('frx_car_option_value_id_car_option', '{{%car_option_value}}');
        $this->dropForeignKey('frx_car_option_value_id_car_type', '{{%car_option_value}}');
        $this->dropForeignKey('frx_car_equipment_id_car_modification', '{{%car_equipment}}');
        $this->dropForeignKey('frx_car_equipment_id_car_type', '{{%car_equipment}}');

        // Если у нас есть в базе RBAC
        if ($this->existRBAC()) {
            \modules\users\models\AuthItem::deleteAll(['name'=>'auto']);
            \modules\users\models\AuthItemChild::deleteAll(['parent'=>'auto']);
            \modules\users\models\AuthItemChild::deleteAll(['child'=>'auto']);
        }
    }

    private function existAutoBase()
    {
        return (
            (\Yii::$app->db->getTableSchema('{{%car_mark}}', true) !== null) &&
            (\Yii::$app->db->getTableSchema('{{%car_model}}', true) !== null) &&
            (\Yii::$app->db->getTableSchema('{{%car_serie}}', true) !== null) &&
            (\Yii::$app->db->getTableSchema('{{%car_characteristic}}', true) !== null) &&
            (\Yii::$app->db->getTableSchema('{{%car_characteristic_value}}', true) !== null) &&
            (\Yii::$app->db->getTableSchema('{{%car_generation}}', true) !== null) &&
            (\Yii::$app->db->getTableSchema('{{%car_modification}}', true) !== null) &&
            (\Yii::$app->db->getTableSchema('{{%car_option}}', true) !== null) &&
            (\Yii::$app->db->getTableSchema('{{%car_option_value}}', true) !== null) &&
            (\Yii::$app->db->getTableSchema('{{%car_equipment}}', true) !== null)
        );
    }

    private function existRBAC()
    {
        return (
            (\Yii::$app->db->getTableSchema('{{%auth_item}}', true) !== null) &&
            (\Yii::$app->db->getTableSchema('{{%auth_item_child}}', true) !== null)
        );
    }
}
