<?php

namespace modules\auto;

use yii\base\BootstrapInterface;
use Yii;

/**
 * Class Bootstrap
 * @package modules\main
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $rules = [];

        if (Yii::$app->id == 'app-frontend') {
            $rules['/new'] = '/auto/new/index';
            $rules['/new/<mark>'] = '/auto/new/mark';
            $rules['/new/<mark>/<serie>'] = '/auto/new/serie';

            $rules['/catalog'] = '/auto/catalog/index';
        }

        $app->urlManager->addRules($rules, false);
    }
}
