<?php

namespace modules\auto\controllers\frontend;

use modules\auto\models\CarMark;
use modules\auto\models\CarSerie;
use frontend\components\FrontendController;
use yii\web\NotFoundHttpException;

/**
 * Class NewController
 * @package modules\auto\controllers\frontend
 */
class NewController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionIndex()
    {
        $marks = CarMark::find()
            ->distinct()
            ->joinWith('carModels')
            ->leftJoin('car_fotosmall', 'car_model.id_car_model = car_fotosmall.id_car_model')
            ->active()
            ->andWhere(['IS NOT', 'car_fotosmall.id', null])
            ->limit(18)
            ->all();

        $seo = $this->getSeo('New cars');

        return $this->render('index', [
            'marks' => $marks,
            'seo' => $seo,
        ]);
    }

    public function actionMark($mark)
    {
        $mark = $this->findModelMark($mark);
        $series = $mark->getCarSerie()->isSlug()->all();
        $seo = $this->getSeo('Модельный ряд {{mark}}', [
            'mark' => $mark->name,
            'mark_rus' => $mark->name_rus,
        ]);

        return $this->render('mark', [
            'mark' => $mark,
            'series' => $series,
            'seo' => $seo,
        ]);
    }

    public function actionSerie($mark, $serie)
    {
        $mark = $this->findModelMark($mark);
        $serie = $this->findModelSerie($serie);
        $model = $serie->carModel;
        $series = $mark->getCarSerie()
            ->andWhere(['<>', 'car_serie.id_car_serie', $serie->id_car_serie])
            ->andWhere(['>=', 'car_serie.cost', $serie->cost])
            ->andWhere(['<=', 'car_serie.cost', $serie->cost])
            ->isSlug()
            ->all();

        $modifications = $serie->getCarModifications()->orderBy('name')->all();

        $seo = $this->getSeo('{{serie_cat}}', [
            'mark' => $mark->name,
            'mark_rus' => $mark->name_rus,
            'model' => $model->name,
            'model_rus' => $model->name_rus,
            'serie' => $serie->name,
            'serie_cat' => $serie->name_cat,
        ]);

        return $this->render('serie', [
            'mark' => $mark,
            'serie' => $serie,
            'model' => $model,
            'series' => $series,
            'modifications' => $modifications,
            'seo' => $seo,
        ]);
    }

    /**
     * Finds the CarMark model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return CarMark the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelMark($slug)
    {
        if ($model = CarMark::find()->slug($slug)->one()) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the CarSerie model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return CarSerie the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelSerie($slug)
    {
        if ($model = CarSerie::find()->slug($slug)->one()) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}