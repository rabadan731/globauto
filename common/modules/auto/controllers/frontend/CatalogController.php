<?php

namespace modules\auto\controllers\frontend;

use modules\auto\models\CarMark;
use modules\auto\models\CarSerie;
use frontend\components\FrontendController;
use yii\web\NotFoundHttpException;

/**
 * Class CatalogController
 * @package modules\auto\controllers\frontend
 */
class CatalogController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionIndex()
    {
        $seo = $this->getSeo('Каталог автомобилей', [
        ]);

        return $this->render('index', [
            'seo' => $seo
        ]);
    }
}