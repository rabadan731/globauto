<?php

namespace modules\auto\controllers\backend;

use Yii;
use modules\auto\models\CarOption;
use modules\auto\models\search\CarOptionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CarOptionController implements the CRUD actions for CarOption model.
 */
class CarOptionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CarOption models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarOptionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CarOption model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CarOption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CarOption();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_car_option]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CarOption model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_car_option]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }



    public function actionAjaxUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {

            $post = Yii::$app->request->post("CarOption", null);

            if (is_null($post)) {
                return $this->renderAjax('ajax-update', [
                    'model' => $model,
                ]);
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            $status = ($model->load(Yii::$app->request->post()) && $model->save());

            $res = array(
                'success' => $status,
                'statusText' => $status?Yii::t('app', 'Saved'):Yii::t('app', 'Errors'),
                'errors' => $model->errors
            );
            return $res;
        }

        throw new NotFoundHttpException('The requested page does not exist. Please Ajax.');
    }




    /**
     * Deletes an existing CarOption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CarOption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CarOption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarOption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
