<?php

namespace modules\auto\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use modules\auto\models\query\CarModelQuery;

/**
 * This is the model class for table "car_model".
 *
 * @property integer $id_car_model
 * @property integer $id_car_mark
 * @property integer $active
 * @property string $name
 * @property string $slug
 * @property string $date_create
 * @property string $date_update
 * @property integer $id_car_type
 * @property string $name_rus
 *
 * @property CarGeneration[] $carGenerations
 * @property CarMark $carMark
 * @property CarType $carType
 * @property CarModification[] $carModifications
 * @property CarSerie[] $carSeries
 * @property CarFotosmall[] $photoSmall
 * @property CarFotobig[] $photoBig
 * @property CarVideo[] $videos
 */
class CarModel extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'ensureUnique' => true,
                'immutable' => true
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_car_mark', 'name', 'id_car_type'], 'required'],
            [['id_car_mark', 'date_create', 'date_update', 'id_car_type', 'active'], 'integer'],
            [['name', 'name_rus'], 'string', 'max' => 255],
            [
                ['id_car_mark'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarMark::className(),
                'targetAttribute' => ['id_car_mark' => 'id_car_mark']
            ],
            [
                ['id_car_type'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarType::className(),
                'targetAttribute' => ['id_car_type' => 'id_car_type']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_model' => Yii::t('cars', 'ID'),
            'id_car_mark' => Yii::t('cars', 'Car Mark'),
            'carMark.name' => Yii::t('cars', 'Car Mark'),
            'name' => Yii::t('cars', 'Car Models'),
            'date_create' => Yii::t('cars', 'Date Create'),
            'date_update' => Yii::t('cars', 'Date Update'),
            'id_car_type' => Yii::t('cars', 'Car Type'),
            'name_rus' => Yii::t('cars', 'Car Models Rus'),
            'active' => Yii::t('cars', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarGenerations()
    {
        return $this->hasMany(CarGeneration::className(), ['id_car_model' => 'id_car_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMark()
    {
        return $this->hasOne(CarMark::className(), ['id_car_mark' => 'id_car_mark']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarType::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModifications()
    {
        return $this->hasMany(CarModification::className(), ['id_car_model' => 'id_car_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarSeries()
    {
        return $this->hasMany(CarSerie::className(), ['id_car_model' => 'id_car_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotoBig()
    {
        return $this->hasMany(CarFotobig::className(), ['id_car_model' => 'id_car_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotoSmall()
    {
        return $this->hasMany(CarFotosmall::className(), ['id_car_model' => 'id_car_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideos()
    {
        return $this->hasMany(CarVideo::className(), ['id_car_model' => 'id_car_model']);
    }

    /**
     * @inheritdoc
     * @return CarModelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarModelQuery(get_called_class());
    }


    /**
     * @param string $p1
     * @param string $p2
     * @param bool $mark_id
     * @param bool $in
     * @return array
     */
    public static function listDropdown($p1 = 'id_car_model', $p2 = 'name', $mark_id = false, $in = false, $group = false)
    {
        $find = self::find()
            ->select([$p1, $p2])
            ->orderBy($p2);

        if ($mark_id !== false) {
            $find->markId($mark_id);
        }

        if ($in !== false) {
            $find->andWhere(['in', 'id_car_model', $in]);
        }

        return ArrayHelper::map($find->all(), $p1,$p2);
    }

    /**
     * @param string $p1
     * @param string $p2
     * @param bool $mark_id
     * @param bool $in
     * @return array
     */
    public static function listGroupDropdown($p1 = 'id_car_model', $p2 = 'name', $mark_id = false, $in = false)
    {
        $find = self::find()
            ->select([$p1, $p2, 'id_car_mark'])
            ->orderBy($p2);

        if ($mark_id !== false) {
            $find->markId($mark_id);
        }

        if ($in !== false) {
            $find->andWhere(['in', 'id_car_model', $in]);
        }

        $result = [];
        /** @var self $item */
        foreach ($find->all() as $item) {
            $result[CarMark::findOne($item->id_car_mark)->name][$item->$p1] = $item->$p2;
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        $photoSmall = $this->getPhotoSmall()->one();
        return $photoSmall->image;
    }
}
