<?php

namespace common\modules\auto\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\auto\models\AutoCatalog]].
 *
 * @see \common\modules\auto\models\AutoCatalog
 */
class AutoCatalogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\auto\models\AutoCatalog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\auto\models\AutoCatalog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
