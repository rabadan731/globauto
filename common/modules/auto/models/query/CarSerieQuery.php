<?php

namespace modules\auto\models\query;

/**
 * This is the ActiveQuery class for [[\modules\auto\models\CarSerie]].
 *
 * @see \modules\auto\models\CarSerie
 */
class CarSerieQuery extends \yii\db\ActiveQuery
{
    public function isSlug()
    {
        $this->andWhere(['IS NOT', 'car_serie.slug', NULL]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarSerie[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarSerie|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $find
     * @return $this
     */
    public function markId($find)
    {
        $this->joinWith(['carMark']);

        $this->andWhere(['car_mark.id_car_mark'=>(int)$find]);

        return $this;
    }


    /**
     * @param $slug
     * @return $this
     */
    public function slug($slug)
    {
        $this->andWhere(['slug'=>$slug]);
        return $this;
    }
}
