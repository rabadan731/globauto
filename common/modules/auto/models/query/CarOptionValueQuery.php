<?php

namespace modules\auto\models\query;

/**
 * This is the ActiveQuery class for [[\modules\auto\models\CarOptionValue]].
 *
 * @see \modules\auto\models\CarOptionValue
 */
class CarOptionValueQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarOptionValue[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarOptionValue|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function equipmentId($find)
    {
        $this->joinWith(['carEquipment']);

        $this->andWhere(['car_equipment.id_car_equipment'=>(int)$find]);

        return $this;
    }
}
