<?php

namespace modules\auto\models\query;

use modules\selling\models\CarsAdverts;
use modules\geo\models\GeoRegion;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[\modules\auto\models\CarMark]].
 *
 * @see \modules\auto\models\CarMark
 */
class CarMarkQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere(['car_mark.active' => 1]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarMark[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarMark|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function orderTitle()
    {
        $this->orderBy('name');
        return $this;
    }

    public function inAdverts($local = true)
    {
        $adverts_mark = CarsAdverts::find()->active()->select('mark_id')->distinct()->asArray();

        if ($local) {
            $city = \Yii::$app->geo->city_id;
            if (is_null($city)) {
                $region_id = \Yii::$app->geo->region_id;
                if (!is_null($region_id)) {
                    $region = GeoRegion::findOne($region_id);
                    if (!is_null($region)) {
                        $cityies = $region->getCities()->select('id')->asArray()->all();
                        $adverts_mark->cityId($cityies);
                    } else {
                        $adverts_mark->cityId(0);
                    }
                } else {
                    $adverts_mark->cityId(0);
                }
            } else {
                $adverts_mark->cityId($city);
            }
        }

        $cars_mark_ind_adcerts_id = ArrayHelper::getColumn($adverts_mark->all(), 'mark_id');
        $this->andWhere(['id_car_mark' => $cars_mark_ind_adcerts_id]);
        return $this;
    }


    public function slug($slug)
    {
        $this->andWhere(['slug' => $slug]);
        return $this;
    }
}
