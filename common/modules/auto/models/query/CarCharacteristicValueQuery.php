<?php

namespace modules\auto\models\query;

/**
 * This is the ActiveQuery class for [[\modules\auto\models\CarCharacteristicValue]].
 *
 * @see \modules\auto\models\CarCharacteristicValue
 */
class CarCharacteristicValueQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarCharacteristicValue[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarCharacteristicValue|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
