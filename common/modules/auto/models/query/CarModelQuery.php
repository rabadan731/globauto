<?php

namespace modules\auto\models\query;
use modules\auto\models\CarMark;
use modules\selling\models\CarsAdverts;
use yii\helpers\ArrayHelper;
use modules\geo\models\GeoRegion;

/**
 * This is the ActiveQuery class for [[\modules\auto\models\CarModel]].
 *
 * @see \modules\auto\models\CarModel
 */
class CarModelQuery extends \yii\db\ActiveQuery
{
    

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $find
     * @return $this
     */
    public function markId($find)
    {
        $this->andWhere(['id_car_mark'=>(int)$find]);

        return $this;
    }

    /**
     * @param $find
     * @return $this
     */
    public function markSlug($find)
    {
        $mark = CarMark::find()->select('id')->where(['slug'=>$find])->one();
        if (is_null($mark)) {
            $this->andWhere(['id_car_mark'=>null]);
        } else {
            $this->andWhere(['id_car_mark'=>$mark->id_car_mark]);
        }

        return $this;
    }


    /**
     * @param $mark_id
     * @param bool $local
     * @return $this
     */
    public function inAdverts($mark_id,$local=true)
    {
        $carAdverts = CarsAdverts::find()
            ->active()
            ->select('model_id')
            ->where(['mark_id'=>$mark_id])
            ->distinct()
            ->asArray();


        if ($local) {
            $city = \Yii::$app->geo->city_id;
            if (is_null($city)) {
                $region_id = \Yii::$app->geo->region_id;
                if (!is_null($region_id)) {
                    $region = GeoRegion::findOne($region_id);
                    if (!is_null($region)) {
                        $cities = $region->getCities()->select('id')->asArray()->all();
                        $carAdverts->cityId($cities);
                    } else {
                        $carAdverts->cityId(0);
                    }
                } else {
                    $carAdverts->cityId(0);
                }
            } else {
                $carAdverts->cityId($city);
            }
        }


        $list_id = ArrayHelper::getColumn($carAdverts->all(),'model_id');

        $this->andWhere(['id_car_model'=>$list_id]);

        return $this;
    }

    /**
     * @return $this
     */
    public function orderTitle()
    {
        $this->orderBy('name');
        return $this;
    }

    /**
     * @param $slug
     * @return $this
     */
    public function slug($slug)
    {
        $this->andWhere(['slug'=>$slug]);
        return $this;
    }
}
