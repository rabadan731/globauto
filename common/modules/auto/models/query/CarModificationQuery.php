<?php

namespace modules\auto\models\query;

/**
 * This is the ActiveQuery class for [[\modules\auto\models\CarModification]].
 *
 * @see \modules\auto\models\CarModification
 */
class CarModificationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarModification[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarModification|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function markId($find)
    {
        $this->joinWith(['carMark']);

        $this->andWhere(['car_mark.id_car_mark'=>(int)$find]);

        return $this;
    }
}
