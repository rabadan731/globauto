<?php

namespace modules\auto\models\query;

/**
 * This is the ActiveQuery class for [[\modules\auto\models\CarFotosmall]].
 *
 * @see \modules\auto\models\CarFotosmall
 */
class CarFotosmallQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarFotosmall[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \modules\auto\models\CarFotosmall|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
