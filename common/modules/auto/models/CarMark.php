<?php

namespace modules\auto\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use modules\auto\models\query\CarMarkQuery;

/**
 * This is the model class for table "car_mark".
 *
 * @property integer $id_car_mark
 * @property integer $active
 * @property string $name
 * @property string $slug
 * @property string $date_create
 * @property string $date_update
 * @property integer $id_car_type
 * @property string $name_rus
 * @property string $icon
 *
 * @property CarType $carType
 * @property CarModel[] $carModels
 */
class CarMark extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class'=>SluggableBehavior::className(),
                'attribute'=>'name',
                'ensureUnique'=>true,
                'immutable' => true
            ],
            [
                'class'=>TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update'
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_mark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'id_car_type'], 'required'],
            [['date_create', 'date_update', 'id_car_type', 'active'], 'integer'],
            [['name', 'name_rus'], 'string', 'max' => 255],
            [['id_car_type'], 'exist', 'skipOnError' => true, 'targetClass' => CarType::className(), 'targetAttribute' => ['id_car_type' => 'id_car_type']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_mark' => Yii::t('cars', 'ID'),
            'name' => Yii::t('cars', 'Brand'),
            'active' => Yii::t('cars', 'Active'),
            'date_create' => Yii::t('cars', 'Date Create'),
            'date_update' => Yii::t('cars', 'Date Update'),
            'id_car_type' => Yii::t('cars', 'Car Type'),
            'name_rus' => Yii::t('cars', 'Brand Rus'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarType::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModels()
    {
        return $this->hasMany(CarModel::className(), ['id_car_mark' => 'id_car_mark']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarSerie()
    {
        return $this->hasMany(CarSerie::className(), ['id_car_model' => 'id_car_model'])
            ->viaTable('car_model', ['id_car_mark' => 'id_car_mark']);
    }

    /**
     * @inheritdoc
     * @return CarMarkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarMarkQuery(get_called_class());
    }

    /**
     * @param string $p1
     * @param string $p2
     * @param bool $in
     * @return array
     */
    public static function listDropdown($p1='id_car_mark',$p2='name',$in=false) {
        $find = self::find()
            ->select([$p1,$p2])
            ->orderBy($p2);

        if ($in) {
            $find->andWhere(['in','id_car_mark',$in]);
        }

        return ArrayHelper::map($find->asArray()->all(), $p1,$p2);
    }

    /**
     * @param string $default
     * @return string
     */
    public function getIcon($default='/storage/noimage.jpg')
    {
        $icon = "/storage/mark_icons/{$this->slug}.png";

        if (file_exists(Yii::getAlias("@root{$icon}"))) {
            return $icon;
        }

        return $default;
    }
}
