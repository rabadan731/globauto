<?php

namespace modules\auto\models;

use Yii;
use modules\auto\models\query\CarCharacteristicValueQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "car_characteristic_value".
 *
 * @property integer $id_car_characteristic_value
 * @property string $value
 * @property string $unit
 * @property integer $id_car_characteristic
 * @property integer $id_car_modification
 * @property integer $id_car_model
 * @property string $date_create
 * @property string $date_update
 * @property integer $id_car_type
 *
 * @property CarCharacteristic $carCharacteristic
 * @property CarModification $carModification
 * @property CarType $carType
 */
class CarCharacteristicValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_characteristic_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value', 'id_car_characteristic', 'id_car_modification', 'id_car_type'], 'required'],
            [['id_car_characteristic', 'id_car_modification', 'date_create', 'date_update', 'id_car_type'], 'integer'],
            [['value', 'unit'], 'string', 'max' => 255],
            [
                ['id_car_characteristic', 'id_car_modification'],
                'unique',
                'targetAttribute' => ['id_car_characteristic', 'id_car_modification'],
                'message' => 'The combination of Id Car Characteristic and Id Car Modification has already been taken.'
            ],
            [
                ['id_car_characteristic', 'id_car_modification', 'id_car_type'],
                'unique',
                'targetAttribute' => ['id_car_characteristic', 'id_car_modification', 'id_car_type'],
                'message' => 'The combination of Id Car Characteristic, Id Car Modification and Id Car Type has already been taken.'],
            [
                ['id_car_characteristic'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarCharacteristic::className(),
                'targetAttribute' => ['id_car_characteristic' => 'id_car_characteristic']
            ],
            [
                ['id_car_modification'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarModification::className(),
                'targetAttribute' => ['id_car_modification' => 'id_car_modification']
            ],
            [
                ['id_car_type'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarType::className(),
                'targetAttribute' => ['id_car_type' => 'id_car_type']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_characteristic_value' => Yii::t('cars', 'ID'),
            'value' => Yii::t('cars', 'Value'),
            'unit' => Yii::t('cars', 'Unit'),
            'id_car_characteristic' => Yii::t('cars', 'Car Characteristics'),
            'id_car_modification' => Yii::t('cars', 'Car Modifications'),
            'date_create' => Yii::t('cars', 'Date Create'),
            'date_update' => Yii::t('cars', 'Date Update'),
            'id_car_type' => Yii::t('cars', 'Car Type'),
            'id_car_mark'           => Yii::t('cars', 'Car Mark'),
            'carMark.name'          => Yii::t('cars', 'Car Mark'),
            'id_car_model'          => Yii::t('cars', 'Car Models'),
            'carModel.name'         => Yii::t('cars', 'Car Models'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarCharacteristic()
    {
        return $this->hasOne(CarCharacteristic::className(), ['id_car_characteristic' => 'id_car_characteristic']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModification()
    {
        return $this->hasOne(CarModification::className(), ['id_car_modification' => 'id_car_modification']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModel()
    {
        return $this->hasMany(CarModel::className(), ['id_car_model' => 'id_car_model'])
            ->viaTable('car_modification', ['id_car_modification' => 'id_car_modification']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMark()
    {
        return $this->hasMany(CarMark::className(), ['id_car_mark' => 'id_car_mark'])->via('carModel');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarType::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @inheritdoc
     * @return CarCharacteristicValueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarCharacteristicValueQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId_car_mark()
    {
        $carModel = $this->carModel;
        if (is_null($carModel)) {
            return null;
        } else {
            return $carModel->id_car_mark;
        }
    }

    /**
     * @return int|null
     */
    public function getId_car_model()
    {
        $carModification = $this->carModification;
        if (is_null($carModification)) {
            return null;
        } else {
            return $carModification->id_car_model;
        }
    }

    public static function findValuesInParams($param_id)
    {
        $find = self::find()
            ->select('value')
            ->distinct()
            ->andWhere(['id_car_characteristic' => $param_id])
            ->orderBy('value')
            ->all();

        return ArrayHelper::getColumn($find, 'value');
    }
}
