<?php

namespace modules\auto\models;

use Yii;
use modules\auto\models\query\CarVideoQuery;

/**
 * This is the model class for table "car_video".
 *
 * @property integer $id
 * @property integer $id_car_model
 * @property string $video
 * @property string $image
 * @property string $youtubeFrame
 */
class CarVideo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_car_model', 'video'], 'required'],
            [['id', 'id_car_model'], 'integer'],
            [['video'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cars', 'ID'),
            'id_car_model' => Yii::t('cars', 'Id Car Model'),
            'video' => Yii::t('cars', 'Video'),
        ];
    }

    /**
     * @inheritdoc
     * @return CarVideoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarVideoQuery(get_called_class());
    }


    public function getYoutubeFrame($w='100%', $h=515)
    {
        return "<iframe width=\"{$w}\" height=\"{$h}\" src=\"https://www.youtube.com/embed/{$this->video}\" frameborder=\"0\" allowfullscreen></iframe>";
    }

    public function getImage()
    {
        return "https://img.youtube.com/vi/{$this->video}/hqdefault.jpg";
    }

}
