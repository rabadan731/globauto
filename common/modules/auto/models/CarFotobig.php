<?php

namespace modules\auto\models;

use Yii;
use modules\auto\models\query\CarFotobigQuery;

/**
 * This is the model class for table "car_fotobig".
 *
 * @property integer $id
 * @property integer $id_car_model
 * @property string $foto
 * @property string $image
 */
class CarFotobig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_fotobig';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_car_model', 'foto'], 'required'],
            [['id', 'id_car_model'], 'integer'],
            [['foto'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cars', 'ID'),
            'id_car_model' => Yii::t('cars', 'Id Car Model'),
            'foto' => Yii::t('cars', 'Foto'),
        ];
    }

    /**
     * @inheritdoc
     * @return CarFotobigQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarFotobigQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return "/storage/carobka/big/{$this->foto}";
    }

    /**
     * @return bool|string
     */
    public function getImagePath()
    {
        return Yii::getAlias("@root{$this->image}");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModel()
    {
        return $this->hasOne(CarModel::className(), ['id_car_model' => 'id_car_model']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMark()
    {
        return $this->hasMany(CarMark::className(), ['id_car_mark' => 'id_car_mark'])
            ->viaTable('car_model', ['id_car_model' => 'id_car_model']);
    }
}
