<?php

namespace modules\auto\models;

use Yii;
use yii\helpers\ArrayHelper;
use modules\auto\models\query\CarEquipmentQuery;

/**
 * This is the model class for table "car_equipment".
 *
 * @property integer $id_car_equipment
 * @property integer $id_car_model
 * @property string $name
 * @property string $date_create
 * @property string $date_update
 * @property integer $id_car_modification
 * @property integer $price_min
 * @property integer $id_car_type
 * @property integer $year
 *
 * @property CarModification $carModification
 * @property CarType $carType
 * @property CarOptionValue[] $carOptionValues
 */
class CarEquipment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_equipment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_create', 'date_update', 'id_car_modification', 'id_car_type'], 'required'],
            [['date_create', 'date_update', 'id_car_modification', 'price_min', 'id_car_type', 'year'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['id_car_modification'], 'exist', 'skipOnError' => true, 'targetClass' => CarModification::className(), 'targetAttribute' => ['id_car_modification' => 'id_car_modification']],
            [['id_car_type'], 'exist', 'skipOnError' => true, 'targetClass' => CarType::className(), 'targetAttribute' => ['id_car_type' => 'id_car_type']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_equipment' => Yii::t('cars', 'ID'),
            'name' => Yii::t('cars', 'Name'),
            'date_create' => Yii::t('cars', 'Date Create'),
            'date_update' => Yii::t('cars', 'Date Update'),
            'id_car_modification' => Yii::t('cars', 'Car Modifications'),
            'carModification.name' => Yii::t('cars', 'Car Modifications'),
            'price_min' => Yii::t('cars', 'Price Min'),
            'id_car_type' => Yii::t('cars', 'Car Type'),
            'year' => Yii::t('cars', 'Year'),
            'id_car_mark'           => Yii::t('cars', 'Car Mark'),
            'carMark.name'          => Yii::t('cars', 'Car Mark'),
            'id_car_model'          => Yii::t('cars', 'Car Models'),
            'carModel.name'         => Yii::t('cars', 'Car Models'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModification()
    {
        return $this->hasOne(CarModification::className(), ['id_car_modification' => 'id_car_modification']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModel()
    {
        return $this->hasMany(CarModel::className(), ['id_car_model' => 'id_car_model'])
            ->via('carModification');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMark()
    {
        return $this->hasMany(CarMark::className(), ['id_car_mark' => 'id_car_mark'])
            ->via('carModel');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarType::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarOptionValues()
    {
        return $this->hasMany(CarOptionValue::className(), ['id_car_equipment' => 'id_car_equipment']);
    }

    /**
     * @inheritdoc
     * @return CarEquipmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarEquipmentQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId_car_mark()
    {
        $carModel = $this->carModel;
        if (is_null($carModel)) {
            return null;
        } else {
            return $carModel->id_car_mark;
        }
    }

    /**
     * @return int|null
     */
    public function getId_car_model()
    {
        $carModification = $this->carModification;
        if (is_null($carModification)) {
            return null;
        } else {
            return $carModification->id_car_model;
        }
    }

    /**
     * @param string $p1
     * @param string $p2
     * @return array
     */
    public static function listDropdown($p1='id_car_equipment',$p2='name', $params = []) {
        $find = self::find()
            ->select([$p1,$p2])
            ->orderBy($p2);

        if (count($params)) {
            $find->andWhere($params);
        }


        $result = [];
        /** @var self $item */
        foreach ($find->all() as $item) {
            $result[$item->$p1] = $item->$p2;
        }

        return $result;
    }

}
