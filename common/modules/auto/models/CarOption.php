<?php

namespace modules\auto\models;

use Yii;
use yii\helpers\ArrayHelper;
use modules\auto\models\query\CarOptionQuery;

/**
 * This is the model class for table "car_option".
 *
 * @property integer $id_car_option
 * @property string $name
 * @property integer $id_parent
 * @property string $date_create
 * @property string $date_update
 * @property integer $id_car_type
 *
 * @property CarType $carType
 * @property CarOptionValue[] $carOptionValues
 */
class CarOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_create', 'date_update', 'id_car_type'], 'required'],
            [['id_parent', 'date_create', 'date_update', 'id_car_type'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [
                ['id_car_type'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarType::className(),
                'targetAttribute' => ['id_car_type' => 'id_car_type']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_option' => Yii::t('cars', 'ID'),
            'name' => Yii::t('cars', 'Name'),
            'id_parent' => Yii::t('cars', 'Parent'),
            'date_create' => Yii::t('cars', 'Date Create'),
            'date_update' => Yii::t('cars', 'Date Update'),
            'id_car_type' => Yii::t('cars', 'Car Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarType::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarOptionValues()
    {
        return $this->hasMany(CarOptionValue::className(), ['id_car_option' => 'id_car_option']);
    }

    /**
     * @inheritdoc
     * @return CarOptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarOptionQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function listDropdownParent()
    {
        $find = self::find()
            ->andWhere(['is', 'id_parent', null])
            ->select(['id_car_option', 'name'])
            ->orderBy('name');


        $items = ArrayHelper::map($find->asArray()->all(), 'id_car_option', 'name');

        return $items;
    }

    /**
     * @param bool $parent
     * @param bool $in
     * @return array
     */
    public static function listDropdown($parent = false, $in = false)
    {
        $find = self::find()
            ->select(['id_car_option', 'name'])
            ->orderBy('name');

        if ($parent) {
            $find->andWhere(['id_parent' => $parent]);
        }

        if ($in) {
            $find->andWhere(['IN', 'id_car_option', $in]);
        }

        $items = ArrayHelper::map($find->asArray()->all(), 'id_car_option', 'name');

        return $items;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id_car_option' => 'id_parent']);
    }
}
