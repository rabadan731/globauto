<?php

namespace modules\auto\models;

use Yii;

/**
 * This is the model class for table "car_type".
 *
 * @property integer $id_car_type
 * @property string $name
 *
 * @property CarCharacteristic[] $carCharacteristics
 * @property CarCharacteristicValue[] $carCharacteristicValues
 * @property CarEquipment[] $carEquipments
 * @property CarGeneration[] $carGenerations
 * @property CarMark[] $carMarks
 * @property CarModel[] $carModels
 * @property CarModification[] $carModifications
 * @property CarOption[] $carOptions
 * @property CarOptionValue[] $carOptionValues
 * @property CarSerie[] $carSeries
 */
class CarType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_type' => Yii::t('cars', 'Id Car Type'),
            'name' => Yii::t('cars', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarCharacteristics()
    {
        return $this->hasMany(CarCharacteristic::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarCharacteristicValues()
    {
        return $this->hasMany(CarCharacteristicValue::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarEquipments()
    {
        return $this->hasMany(CarEquipment::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarGenerations()
    {
        return $this->hasMany(CarGeneration::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMarks()
    {
        return $this->hasMany(CarMark::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModels()
    {
        return $this->hasMany(CarModel::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModifications()
    {
        return $this->hasMany(CarModification::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarOptions()
    {
        return $this->hasMany(CarOption::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarOptionValues()
    {
        return $this->hasMany(CarOptionValue::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarSeries()
    {
        return $this->hasMany(CarSerie::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @inheritdoc
     * @return \modules\auto\models\query\CarTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \modules\auto\models\query\CarTypeQuery(get_called_class());
    }
}
