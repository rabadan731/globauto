<?php

namespace modules\auto\models;

use Yii;
use modules\auto\models\query\CarFotosmallQuery;

/**
 * This is the model class for table "car_fotosmall".
 *
 * @property integer $id
 * @property integer $id_car_model
 * @property string $foto
 * @property string $image
 */
class CarFotosmall extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_fotosmall';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_car_model', 'foto'], 'required'],
            [['id_car_model'], 'integer'],
            [['foto'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cars', 'ID'),
            'id_car_model' => Yii::t('cars', 'Id Car Model'),
            'foto' => Yii::t('cars', 'Foto'),
        ];
    }

    /**
     * @inheritdoc
     * @return CarFotosmallQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarFotosmallQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return "/storage/carobka/small/{$this->foto}";
    }

    /**
     * @return bool|string
     */
    public function getImagePath()
    {
        return Yii::getAlias("@root{$this->image}");
    }
}
