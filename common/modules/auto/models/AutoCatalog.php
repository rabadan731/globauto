<?php

namespace modules\auto\models;

use Yii;
use common\modules\auto\models\query\AutoCatalogQuery;

/**
 * This is the model class for table "auto_catalog".
 *
 * @property integer $auto_catalog_id
 * @property string $slug
 * @property integer $country_id
 * @property integer $type_body_id
 * @property integer $engine_id
 * @property integer $gear_id
 * @property integer $transmission_id
 * @property string $color
 * @property string $price_start
 * @property string $price_end
 * @property string $run_start
 * @property string $run_end
 */
class AutoCatalog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auto_catalog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug'], 'required'],
            [[
                'country_id',
                'type_body_id',
                'engine_id',
                'gear_id',
                'transmission_id',
                'price_start',
                'price_end',
                'run_start',
                'run_end'
            ], 'integer'],
            [['slug'], 'string', 'max' => 512],
            [['color'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'auto_catalog_id' => Yii::t('cars', 'Auto Catalog ID'),
            'slug' => Yii::t('cars', 'Slug'),
            'country_id' => Yii::t('cars', 'Country ID'),
            'type_body_id' => Yii::t('cars', 'Type Body ID'),
            'engine_id' => Yii::t('cars', 'Engine ID'),
            'gear_id' => Yii::t('cars', 'Gear ID'),
            'transmission_id' => Yii::t('cars', 'Transmission ID'),
            'color' => Yii::t('cars', 'Color'),
            'price_start' => Yii::t('cars', 'Price Start'),
            'price_end' => Yii::t('cars', 'Price End'),
            'run_start' => Yii::t('cars', 'Run Start'),
            'run_end' => Yii::t('cars', 'Run End'),
        ];
    }

    /**
     * @inheritdoc
     * @return AutoCatalogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AutoCatalogQuery(get_called_class());
    }
}
