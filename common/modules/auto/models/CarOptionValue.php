<?php

namespace modules\auto\models;

use Yii;

/**
 * This is the model class for table "car_option_value".
 *
 * @property integer $id_car_option_value
 * @property integer $is_base
 * @property integer $id_car_option
 * @property integer $id_car_equipment
 * @property string $date_create
 * @property string $date_update
 * @property integer $id_car_type
 *
 * @property CarEquipment $carEquipment
 * @property CarOption $carOption
 * @property CarType $carType
 */
class CarOptionValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_option_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_base', 'id_car_option', 'id_car_equipment', 'date_create', 'date_update', 'id_car_type'], 'integer'],
            [['id_car_option', 'id_car_equipment', 'date_create', 'date_update', 'id_car_type'], 'required'],
            [
                ['id_car_option', 'id_car_equipment', 'id_car_type'],
                'unique',
                'targetAttribute' => ['id_car_option', 'id_car_equipment', 'id_car_type'],
                'message' => 'The combination of Id Car Option, Id Car Equipment and Id Car Type has already been taken.'
            ],
            [
                ['id_car_equipment'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarEquipment::className(),
                'targetAttribute' => ['id_car_equipment' => 'id_car_equipment']
            ],
            [
                ['id_car_option'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarOption::className(),
                'targetAttribute' => ['id_car_option' => 'id_car_option']
            ],
            [
                ['id_car_type'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarType::className(),
                'targetAttribute' => ['id_car_type' => 'id_car_type']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_option_value' => Yii::t('cars', 'ID'),
            'id_car_option_parent' => Yii::t('cars', 'Parent'),
            'is_base' => Yii::t('cars', 'Is Base'),
            'id_car_option' => Yii::t('cars', 'Car Options'),
            'carOption.name' => Yii::t('cars', 'Car Options'),
            'id_car_equipment' => Yii::t('cars', 'Car Equipments'),
            'carEquipment.name' => Yii::t('cars', 'Car Equipments'),
            'date_create' => Yii::t('cars', 'Date Create'),
            'date_update' => Yii::t('cars', 'Date Update'),
            'id_car_type' => Yii::t('cars', 'Car Type'),
            'id_car_mark' => Yii::t('cars', 'Car Mark'),
            'carMark.name' => Yii::t('cars', 'Car Mark'),
            'id_car_model' => Yii::t('cars', 'Car Models'),
            'carModel.name' => Yii::t('cars', 'Car Models'),
            'id_car_modification' => Yii::t('cars', 'Car Modifications'),
            'carModification.name' => Yii::t('cars', 'Car Modifications'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarEquipment()
    {
        return $this->hasOne(CarEquipment::className(), ['id_car_equipment' => 'id_car_equipment']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModification()
    {
        return $this->hasOne(CarModification::className(), ['id_car_modification' => 'id_car_modification'])
            ->via('carEquipment');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModel()
    {
        return $this->hasMany(CarModel::className(), ['id_car_model' => 'id_car_model'])
            ->via('carModification');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMark()
    {
        return $this->hasMany(CarMark::className(), ['id_car_mark' => 'id_car_mark'])
            ->via('carModel');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarOption()
    {
        return $this->hasOne(CarOption::className(), ['id_car_option' => 'id_car_option']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarType::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @inheritdoc
     * @return \modules\auto\models\query\CarOptionValueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \modules\auto\models\query\CarOptionValueQuery(get_called_class());
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId_car_modification()
    {
        $carModification = $this->carModification;
        if (is_null($carModification)) {
            return null;
        } else {
            return $carModification->id_car_modification;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId_car_mark()
    {
        $carModel = $this->carModel;
        if (is_null($carModel)) {
            return null;
        } else {
            return $carModel->id_car_mark;
        }
    }

    /**
     * @return int|null
     */
    public function getId_car_model()
    {
        $carModification = $this->carModification;
        if (is_null($carModification)) {
            return null;
        } else {
            return $carModification->id_car_model;
        }
    }
}
