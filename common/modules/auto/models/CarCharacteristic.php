<?php

namespace modules\auto\models;

use Yii;
use yii\helpers\ArrayHelper;
use modules\auto\models\query\CarCharacteristicQuery;

/**
 * This is the model class for table "car_characteristic".
 *
 * @property integer $id_car_characteristic
 * @property string $name
 * @property integer $id_parent
 * @property string $date_create
 * @property string $date_update
 * @property integer $id_car_type
 *
 * @property CarCharacteristic $parent
 * @property CarType $carType
 * @property CarCharacteristicValue[] $carCharacteristicValues
 * @property CarModification[] $carModifications
 */
class CarCharacteristic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_characteristic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_parent', 'date_create', 'date_update', 'id_car_type'], 'integer'],
            [['id_car_type'], 'required'],
            [['name'], 'string', 'max' => 255],
            [
                ['id_car_type'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarType::className(),
                'targetAttribute' => ['id_car_type' => 'id_car_type']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_characteristic' => Yii::t('cars', 'ID'),
            'name' => Yii::t('cars', 'Name'),
            'id_parent' => Yii::t('cars', 'Parent'),
            'date_create' => Yii::t('cars', 'Date Create'),
            'date_update' => Yii::t('cars', 'Date Update'),
            'id_car_type' => Yii::t('cars', 'Car Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarType::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(CarCharacteristic::className(), ['id_car_characteristic' => 'id_parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarCharacteristicValues()
    {
        return $this->hasMany(CarCharacteristicValue::className(), [
            'id_car_characteristic' => 'id_car_characteristic'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModifications()
    {
        return $this->hasMany(CarModification::className(), ['id_car_modification' => 'id_car_modification'])
            ->viaTable('car_characteristic_value', ['id_car_characteristic' => 'id_car_characteristic']);
    }

    /**
     * @inheritdoc
     * @return CarCharacteristicQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarCharacteristicQuery(get_called_class());
    }

    public static function listDropdownParent() {
        $find = self::find()
            ->andWhere(['is','id_parent',null])
            ->select(['id_car_characteristic','name'])
            ->orderBy('name');


        $items = ArrayHelper::map($find->asArray()->all(), 'id_car_characteristic','name');
        
        return $items;
    }

    public static function listDropdown() {
        $find = self::find() 
            ->select(['id_car_characteristic','name'])
            ->orderBy('name');


        $items = ArrayHelper::map($find->asArray()->all(), 'id_car_characteristic','name');

        return $items;
    }
}
