<?php

namespace modules\auto\models;

use Yii;
use yii\helpers\ArrayHelper;
use modules\auto\models\query\CarModificationQuery;

/**
 * This is the model class for table "car_modification".
 *
 * @property integer $id_car_modification
 * @property integer $id_car_serie
 * @property integer $id_car_model
 * @property string $name
 * @property string $date_create
 * @property string $date_update
 * @property integer $id_car_type
 *
 * @property CarCharacteristicValue[] $carCharacteristicValues
 * @property CarCharacteristic[] $idCarCharacteristics
 * @property CarEquipment[] $carEquipments
 * @property CarModel $carModel
 * @property CarSerie $carSerie
 * @property CarType $carType
 */
class CarModification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_modification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_car_serie', 'id_car_model', 'name', 'id_car_type'], 'required'],
            [['id_car_serie', 'id_car_model', 'date_create', 'date_update', 'id_car_type'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [
                ['id_car_model'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarModel::className(),
                'targetAttribute' => ['id_car_model' => 'id_car_model']
            ],
            [
                ['id_car_serie'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarSerie::className(),
                'targetAttribute' => ['id_car_serie' => 'id_car_serie']
            ],
            [
                ['id_car_type'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarType::className(),
                'targetAttribute' => ['id_car_type' => 'id_car_type']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_modification'   => Yii::t('cars', 'ID'),
            'id_car_generation'     => Yii::t('cars', 'Car Generations'),
            'id_car_serie'          => Yii::t('cars', 'Car Series'),
            'carSerie.name'         => Yii::t('cars', 'Car Series'),
            'id_car_mark'           => Yii::t('cars', 'Car Mark'),
            'carMark.name'          => Yii::t('cars', 'Car Mark'),
            'id_car_model'          => Yii::t('cars', 'Car Models'),
            'carModel.name'         => Yii::t('cars', 'Car Models'),
            'name'                  => Yii::t('cars', 'Name'),
            'date_create'           => Yii::t('cars', 'Date Create'),
            'date_update'           => Yii::t('cars', 'Date Update'),
            'id_car_type'           => Yii::t('cars', 'Car Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarCharacteristicValues()
    {
        return $this->hasMany(CarCharacteristicValue::className(), ['id_car_modification' => 'id_car_modification']);
    }

    /**
     * @param $idCarCharacteristic
     * @return string
     */
    public function getCharacteristic($idCarCharacteristic)
    {
        $result = $this->getCarCharacteristicValues()
            ->andWhere(['id_car_characteristic' => $idCarCharacteristic])
            ->one();

        /** @var $result CarCharacteristicValue */

        if (!is_null($result)) {
            return $result->value;
        }

        return '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarCharacteristics()
    {
        return $this->hasMany(CarCharacteristic::className(), ['id_car_characteristic' => 'id_car_characteristic'])
            ->viaTable('car_characteristic_value', ['id_car_modification' => 'id_car_modification']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarEquipments()
    {
        return $this->hasMany(CarEquipment::className(), ['id_car_modification' => 'id_car_modification']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModel()
    {
        return $this->hasOne(CarModel::className(), ['id_car_model' => 'id_car_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarSerie()
    {
        return $this->hasOne(CarSerie::className(), ['id_car_serie' => 'id_car_serie']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarType::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @inheritdoc
     * @return CarModificationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarModificationQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMark()
    {
        return $this->hasMany(CarMark::className(), ['id_car_mark' => 'id_car_mark'])
            ->viaTable('car_model', ['id_car_model' => 'id_car_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarGeneration()
    {
        return $this->hasMany(CarGeneration::className(), ['id_car_generation' => 'id_car_generation'])
            ->viaTable('car_serie', ['id_car_serie' => 'id_car_serie']);
    }


    /**
     * @return int|null
     */
    public function getId_car_mark()
    {
        $carModel = $this->carModel;
        if (is_null($carModel)) {
            return null;
        } else {
            return $carModel->id_car_mark;
        }
    }

    /**
     * @param string $p1
     * @param string $p2
     * @param bool $model_id
     * @return array
     */
    public static function listDropdown($p1 = 'id_car_modification', $p2 = 'name', $model_id = false)
    {
        $find = self::find()
            ->select(['car_modification.id_car_modification', 'car_modification.name', 'car_modification.id_car_model'])
            ->orderBy($p2);

        $find->joinWith("carEquipments");
        $find->joinWith("carModel");
        $find->andWhere(['is not', 'car_equipment.id_car_equipment', null]);

        if ($model_id !== false) {
            $find->andWhere(['car_modification.id_car_model' => $model_id]);
        }



        $result = [];
        /** @var self $item */
        foreach ($find->all() as $item) {
            $result[$item->carModel->name][$item->$p1] = $item->$p2;
        }

        return $result;
    }
}
