<?php

namespace modules\auto\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "car_generation".
 *
 * @property integer $id_car_generation
 * @property string $name
 * @property integer $id_car_mark
 * @property integer $id_car_model
 * @property string $year_begin
 * @property string $year_end
 * @property string $date_create
 * @property string $date_update
 * @property integer $id_car_type
 *
 * @property CarModel $carModel
 * @property CarType $carType
 */
class CarGeneration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_generation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'id_car_model', 'date_create'], 'required'],
            [['id_car_model', 'date_create', 'date_update', 'id_car_type'], 'integer'],
            [['name', 'year_begin', 'year_end'], 'string', 'max' => 255],
            [
                ['id_car_model'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarModel::className(),
                'targetAttribute' => ['id_car_model' => 'id_car_model']
            ],
            [
                ['id_car_type'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarType::className(),
                'targetAttribute' => ['id_car_type' => 'id_car_type']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_generation' => Yii::t('cars', 'Id'),
            'name' => Yii::t('cars', 'Name'),
            'year_begin' => Yii::t('cars', 'Year Begin'),
            'year_end' => Yii::t('cars', 'Year End'),
            'date_create' => Yii::t('cars', 'Date Create'),
            'date_update' => Yii::t('cars', 'Date Update'),
            'id_car_type' => Yii::t('cars', 'Id Car Type'),
            'id_car_mark' => Yii::t('cars', 'Car Mark'),
            'carModel.name' => Yii::t('cars', 'Car Models'),
            'id_car_model' => Yii::t('cars', 'Car Models'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModel()
    {
        return $this->hasOne(CarModel::className(), ['id_car_model' => 'id_car_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMark()
    {
        return $this->hasMany(CarMark::className(), ['id_car_mark' => 'id_car_mark'])
            ->viaTable('car_model', ['id_car_model' => 'id_car_model']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId_car_mark()
    {
        $carModel = $this->carModel;
        if (is_null($carModel)) {
            return null;
        } else {
            return $carModel->id_car_mark;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarType::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @inheritdoc
     * @return \modules\auto\models\query\CarGenerationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \modules\auto\models\query\CarGenerationQuery(get_called_class());
    }

    public static function listDropdown($p1='id_car_generation',$p2='name',$model_id=false)
    {
        $find = self::find()
            ->select([$p1,$p2])
            ->orderBy($p2);

        if ($model_id) {
            $find->andWhere(['id_car_model'=>$model_id]);
        }

        return ArrayHelper::map(
            $find->asArray()->all(),
            $p1,$p2);
    }
}













