<?php

namespace modules\auto\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use modules\auto\models\query\CarSerieQuery;

/**
 * This is the model class for table "car_serie".
 *
 * @property integer $id_car_serie
 * @property integer $id_car_model
 * @property integer $id_car_mark
 * @property string $name
 * @property string $date_create
 * @property string $date_update
 * @property integer $id_car_generation
 * @property integer $id_car_type
 * @property string $name_cat
 * @property string $text
 * @property string $icon
 * @property string $cost
 * @property string $slug
 *
 * @property CarModification[] $carModifications
 * @property CarModel $carModel
 * @property CarType $carType
 */
class CarSerie extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class'=>SluggableBehavior::className(),
                'attribute'=>'name_cat',
                'ensureUnique'=>true,
                'immutable' => true
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_serie';
    }

    /**
     * @inheritdoc
     *
     */
    public function rules()
    {
        return [
            [['id_car_model', 'name', 'id_car_type'], 'required'],
            [[
                'id_car_model',
                'date_create',
                'date_update',
                'id_car_generation',
                'id_car_type'
            ], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['name_cat', 'icon'], 'string', 'max' => 128],
            [['text', 'cost'], 'string'],
            [
                ['id_car_model'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarModel::className(),
                'targetAttribute' => ['id_car_model' => 'id_car_model']
            ],
            [
                ['id_car_type'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarType::className(),
                'targetAttribute' => ['id_car_type' => 'id_car_type']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_car_serie'          => Yii::t('cars', 'ID'),
            'name'                  => Yii::t('cars', 'Name'),
            'date_create'           => Yii::t('cars', 'Date Create'),
            'date_update'           => Yii::t('cars', 'Date Update'),
            'id_car_generation'     => Yii::t('cars', 'Car Generations'),
            'carGeneration.name'    => Yii::t('cars', 'Car Generations'),
            'id_car_type'           => Yii::t('cars', 'Car Type'),
            'id_car_mark'           => Yii::t('cars', 'Car Mark'),
            'carMark.name'          => Yii::t('cars', 'Car Mark'),
            'id_car_model'          => Yii::t('cars', 'Car Models'),
            'carModel.name'         => Yii::t('cars', 'Car Models'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarGeneration()
    {
        return $this->hasOne(CarGeneration::className(), ['id_car_generation' => 'id_car_generation']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModifications()
    {
        return $this->hasMany(CarModification::className(), ['id_car_serie' => 'id_car_serie']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModel()
    {
        return $this->hasOne(CarModel::className(), ['id_car_model' => 'id_car_model']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarType::className(), ['id_car_type' => 'id_car_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMark()
    {
        return $this->hasMany(CarMark::className(), ['id_car_mark' => 'id_car_mark'])
            ->viaTable('car_model', ['id_car_model' => 'id_car_model']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarVideos()
    {
        return $this->hasMany(CarVideo::className(), ['id_car_model' => 'id_car_model']);
    }

    /**
     * @inheritdoc
     * @return CarSerieQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarSerieQuery(get_called_class());
    }

    /**
     * @return int|null
     */
    public function getId_car_mark()
    {
        $carModel = $this->carModel;
        if (is_null($carModel)) {
            return null;
        } else {
            return $carModel->id_car_mark;
        }
    }

    /**
     * @param string $p1
     * @param string $p2
     * @param bool $model_id
     * @param bool $id_car_generation
     * @return array
     */
    public static function listDropdown($p1='id_car_serie', $p2='name', $model_id=false, $id_car_generation=false)
    {
        $find = self::find()->select([$p1,$p2, 'id_car_generation'])->orderBy($p2);

        if ($model_id) {
            $find->andWhere(['id_car_model'=>$model_id]);
        }

        if ($id_car_generation) {
            $find->andWhere(['id_car_generation'=>$id_car_generation]);
        }

        $result = [];
        /** @var self $item */
        foreach ($find->all() as $item) {
            $result[CarGeneration::findOne($item->id_car_generation)->name][$item->$p1] = $item->$p2;
        }

        return $result;
    }


    /**
     * @return string
     */
    public function getImage()
    {
        return "/storage/carobka/icon/{$this->icon}";
    }

    /**
     * @return string
     */
    public function getNiceCost()
    {
        if ($this->cost > 0) {
            return number_format($this->cost, 0, '.', ' ') . " руб.";
        }
        return '';
    }
}
