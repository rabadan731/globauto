<?php

namespace modules\auto\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\auto\models\CarModification;

/**
 * CarModificationSearch represents the model behind the search form about `modules\auto\models\CarModification`.
 */
class CarModificationSearch extends CarModification
{
    public $id_car_mark;
    public $id_car_generation;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_car_modification', 'id_car_serie', 'id_car_model', 'date_create', 'date_update', 'id_car_type'], 'integer'],
            [['name', 'id_car_mark', 'id_car_generation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarModification::find();

        // add conditions that should always apply here
        $query->joinWith(['carMark']);
        $query->joinWith(['carGeneration']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_car_modification' => $this->id_car_modification,
            'car_modification.id_car_serie' => $this->id_car_serie,
            'car_modification.id_car_model' => $this->id_car_model,
            'car_mark.id_car_mark' => $this->id_car_mark,
            'car_generation.id_car_generation' => $this->id_car_generation,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
            'id_car_type' => $this->id_car_type,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
