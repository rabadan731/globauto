<?php

namespace modules\auto\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\auto\models\CarOptionValue;

/**
 * CarOptionValueSearch represents the model behind the search form about `modules\auto\models\CarOptionValue`.
 */
class CarOptionValueSearch extends CarOptionValue
{
    public $id_car_option_parent;
    public $id_car_mark;
    public $id_car_model;
    public $id_car_modification;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'id_car_mark',
                'id_car_model',
                'id_car_modification',
                'id_car_option_value',
                'is_base',
                'id_car_option',
                'id_car_option_parent',
                'id_car_equipment',
                'date_create',
                'date_update',
                'id_car_type'
            ], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarOptionValue::find();

        // add conditions that should always apply here
        $query->joinWith(['carMark']);
        $query->joinWith(['carOption']);
        $query->joinWith(['carEquipment']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'car_option_value.id_car_option_value' => $this->id_car_option_value,
            'car_option_value.is_base' => $this->is_base,
            'car_option_value.id_car_option' => $this->id_car_option,
            'car_option_value.id_car_equipment' => $this->id_car_equipment,
            'car_option_value.date_create' => $this->date_create,
            'car_option_value.date_update' => $this->date_update,
            'car_option_value.id_car_type' => $this->id_car_type,
            'car_option.id_parent' => $this->id_car_option_parent,
            'car_mark.id_car_mark' => $this->id_car_mark,
            'car_model.id_car_model' => $this->id_car_model,
        ]);

        return $dataProvider;
    }
}
