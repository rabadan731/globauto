<?php

namespace modules\auto\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\auto\models\CarCharacteristicValue;

/**
 * CarCharacteristicValueSearch represents the model behind the search form about `modules\auto\models\CarCharacteristicValue`.
 */
class CarCharacteristicValueSearch extends CarCharacteristicValue
{
    public $id_car_mark;
    public $id_car_model;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'id_car_characteristic_value',
                'id_car_characteristic',
                'id_car_modification',
                'date_create',
                'date_update',
                'id_car_mark',
                'id_car_model',
                'id_car_type'
            ], 'integer'],
            [['value', 'unit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarCharacteristicValue::find();

        // add conditions that should always apply here
        $query->joinWith("carModel");
        $query->joinWith("carMark");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'car_characteristic_value.id_car_characteristic_value' => $this->id_car_characteristic_value,
            'car_characteristic_value.id_car_characteristic' => $this->id_car_characteristic,
            'car_characteristic_value.id_car_modification' => $this->id_car_modification,
            'car_characteristic_value.date_create' => $this->date_create,
            'car_characteristic_value.date_update' => $this->date_update,
            'car_characteristic_value.id_car_type' => $this->id_car_type,
            'car_mark.id_car_mark' => $this->id_car_mark,
            'car_model.id_car_model' => $this->id_car_model,
        ]);

        $query->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'unit', $this->unit]);

        return $dataProvider;
    }
}
