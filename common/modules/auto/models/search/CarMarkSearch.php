<?php

namespace modules\auto\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\auto\models\CarMark;

/**
 * CarMarkSearch represents the model behind the search form about `modules\auto\models\CarMark`.
 */
class CarMarkSearch extends CarMark
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_car_mark', 'date_create', 'date_update', 'id_car_type', 'active'], 'integer'],
            [['name', 'name_rus', 'slug'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarMark::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_car_mark' => $this->id_car_mark,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
            'id_car_type' => $this->id_car_type,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_rus', $this->name_rus])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
