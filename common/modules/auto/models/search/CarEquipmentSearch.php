<?php

namespace modules\auto\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\auto\models\CarEquipment;

/**
 * CarEquipmentSearch represents the model behind the search form about `modules\auto\models\CarEquipment`.
 */
class CarEquipmentSearch extends CarEquipment
{
    public $id_car_mark;
    public $id_car_model;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_car_equipment', 'date_create', 'date_update', 'id_car_modification', 'price_min', 'id_car_type', 'year'], 'integer'],
            [['name', 'id_car_mark', 'id_car_model'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarEquipment::find();

        // add conditions that should always apply here
        $query->joinWith("carModel");
        $query->joinWith("carMark");


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_car_equipment' => $this->id_car_equipment,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
            'car_equipment.id_car_modification' => $this->id_car_modification,
            'price_min' => $this->price_min,
            'id_car_type' => $this->id_car_type,
            'year' => $this->year,
            'car_model.id_car_model' => $this->id_car_model,
            'car_mark.id_car_mark' => $this->id_car_mark,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
