<?php

namespace modules\auto\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\auto\models\AutoCatalog;

/**
 * AutoCatalogSearch represents the model behind the search form about `modules\auto\models\AutoCatalog`.
 */
class AutoCatalogSearch extends AutoCatalog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auto_catalog_id', 'country_id', 'type_body_id', 'engine_id', 'gear_id', 'transmission_id', 'price_start', 'price_end', 'run_start', 'run_end'], 'integer'],
            [['slug', 'color'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AutoCatalog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'auto_catalog_id' => $this->auto_catalog_id,
            'country_id' => $this->country_id,
            'type_body_id' => $this->type_body_id,
            'engine_id' => $this->engine_id,
            'gear_id' => $this->gear_id,
            'transmission_id' => $this->transmission_id,
            'price_start' => $this->price_start,
            'price_end' => $this->price_end,
            'run_start' => $this->run_start,
            'run_end' => $this->run_end,
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'color', $this->color]);

        return $dataProvider;
    }
}
