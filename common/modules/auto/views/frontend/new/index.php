<?php

use themes\globauto\assets\GlobAutoAsset;
use yii\helpers\Url;
use modules\auto\models\CarMark;

/* @var $this yii\web\View */
/* @var $seo \modules\seo\models\Seo */
/* @var $marks CarMark[] */

$this->params['mainClass'] = "main cars row main_bg";
$asset = GlobAutoAsset::register($this);

$this->params['breadcrumbs'][] = $seo->breadcrumb;

?>
<div class="container">

    <div class="row">
        <h1 class="pagetitle"><?=$seo->h1;?></h1>
        <div>
            <?=$seo->body;?>
        </div>
    </div>

    <div class="row">
        <div class="content col-xs-12" itemprop="mainContentOfPage">
            <section class="brands_catalog phone_button hidden-xs">
                <div class="clearfix">
                    <h2 class="title"><?=$seo->h2;?></h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="brands_catalog_list clearfix">
                    <?php foreach ($marks as $mark) { ?>
                        <li class="brands_catalog_list_item">
                            <a href="<?= Url::to(['/auto/new/mark', 'mark'=> $mark->slug]);?>" class="brands_catalog_list_item_link">
                                <img src="<?= $mark->icon; ?>"
                                     alt="<?= $mark->name; ?>"
                                     class="brands_catalog_list_item_logotip">
                                <span class="brands_catalog_list_item_name"><?=$mark->name;?></span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </section>

            <ul class="important_list  clearfix">
                <li class="important_list_item col-sm-6 col-md-4">
                    <a href="#" class="important_list_item_link">
                        <img src="<?= $asset->baseUrl; ?>/img/review-icon.png" alt="Отзывы на авто" class="important_list_item_icon">
                        <span class="important_list_item_name">Отзывы на авто</span>
                        <span class="important_list_item_meaning">2513 отзывов</span>
                    </a>
                </li>
                <li class="important_list_item col-sm-6 col-md-4">
                    <a href="#" class="important_list_item_link">
                        <img src="<?= $asset->baseUrl; ?>/img/map-icon.png" alt="" class="important_list_item_icon">
                        <span class="important_list_item_name">Автосалоны Уфы</span>
                        <span class="important_list_item_meaning">17 автосалонов</span>
                    </a>
                </li>
                <li class="important_list_item col-sm-6 col-md-4">
                    <a href="#" class="important_list_item_link">
                        <img src="<?= $asset->baseUrl; ?>/img/percentage-icon.png" alt="" class="important_list_item_icon">
                        <span class="important_list_item_name">Акции и скидки</span>
                        <span class="important_list_item_meaning">5 предложений</span>
                    </a>
                </li>
                <li class="important_list_item col-sm-6 col-md-4">
                    <a href="#" class="important_list_item_link">
                        <img src="<?= $asset->baseUrl; ?>/img/new-car-icon.png" alt="" class="important_list_item_icon">
                        <span class="important_list_item_name">Новые автомобили</span>
                        <span class="important_list_item_meaning">572 автомобиля</span>
                    </a>
                </li>
                <li class="important_list_item col-sm-6 col-md-4">
                    <a href="#" class="important_list_item_link">
                        <img src="<?= $asset->baseUrl; ?>/img/used-cars-icon.png" alt="" class="important_list_item_icon">
                        <span class="important_list_item_name">Авто с пробегом </span>
                        <span class="important_list_item_meaning">12725 автомобилей</span>
                    </a>
                </li>
            </ul>

            <section class="cars_showroom_list new_catalog phone_button  hidden-xs">
                <div class="clearfix">
                    <h2 class="title">АВТОСАЛОНы в Уфе</h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="interesting_list row">
                    <li class="interesting_list_item col-sm-4">
                        <img src="<?= $asset->baseUrl; ?>/img/photo-1.png" alt="Автолайф-Премиум" class="interesting_list_item_photo">
                        <a href="#" class="interesting_list_item_title">Автолайф-Премиум</a>
                    </li>
                    <li class="interesting_list_item col-sm-4">
                        <img src="<?= $asset->baseUrl; ?>/img/photo-1.png" alt="Автолайф-Премиум" class="interesting_list_item_photo">
                        <a href="#" class="interesting_list_item_title">Автолайф-Премиум</a>
                    </li>
                    <li class="interesting_list_item col-sm-4">
                        <img src="<?= $asset->baseUrl; ?>/img/photo-1.png" alt="Автолайф-Премиум" class="interesting_list_item_photo">
                        <a href="#" class="interesting_list_item_title">Автолайф-Премиум</a>
                    </li>
                    <li class="interesting_list_item col-sm-4">
                        <img src="<?= $asset->baseUrl; ?>/img/photo-1.png" alt="Автолайф-Премиум" class="interesting_list_item_photo">
                        <a href="#" class="interesting_list_item_title">Автолайф-Премиум</a>
                    </li>
                    <li class="interesting_list_item col-sm-4">
                        <img src="<?= $asset->baseUrl; ?>/img/photo-1.png" alt="Автолайф-Премиум" class="interesting_list_item_photo">
                        <a href="#" class="interesting_list_item_title">Автолайф-Премиум</a>
                    </li>
                    <li class="interesting_list_item col-sm-4">
                        <img src="<?= $asset->baseUrl; ?>/img/photo-1.png" alt="Автолайф-Премиум" class="interesting_list_item_photo">
                        <a href="#" class="interesting_list_item_title">Автолайф-Премиум</a>
                    </li>
                </ul>
            </section>

            <section class="new_catalog phone_button hidden-xs">
                <div class="clearfix">
                    <h2 class="title">НОВЫЕ АВТОМОБИЛИ В АВТОСАЛОНАХ Уфы</h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="new_catalog_list clearfix">
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                </ul>
            </section>

            <section class="discounts hidden-xs">
                <div class="clearfix">
                    <h2 class="title">акции и скидки на новые автомобили в Уфе</h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="important_list clearfix">
                    <li class="important_list_item col-sm-4">
                        <a href="#" class="important_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="Отзывы на авто" class="important_list_item_icon">
                            <span class="important_list_item_name">Запчасти со скидкой на Mitsubishi</span>
                            <span class="important_list_item_meaning">До 30 июня</span>
                        </a>
                    </li>
                    <li class="important_list_item col-sm-4">
                        <a href="#" class="important_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="" class="important_list_item_icon">
                            <span class="important_list_item_name">Запчасти со скидкой
на Mitsubishi</span>
                            <span class="important_list_item_meaning">До 30 июня</span>
                        </a>
                    </li>
                    <li class="important_list_item col-sm-4">
                        <a href="#" class="important_list_item_link">
                            <img src="<?= $asset->baseUrl; ?>/img/peugeot.png" alt="" class="important_list_item_icon">
                            <span class="important_list_item_name">Запчасти со скидкой
на Mitsubishi</span>
                            <span class="important_list_item_meaning">До 30 июня</span>
                        </a>
                    </li>
                </ul>
            </section>

            <section class="used_catalog hidden-xs">
                <div class="clearfix">
                    <h2 class="title">Автомобили с пробегом в Уфе</h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="used_catalog_list row">
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                </ul>
            </section>

            <section class="cars_reviews brand_test row hidden-xs">
                <div class="clearfix">
                    <h2 class="title">ПОСЛЕДНИЕ ОТЗЫВЫ НА АВТО</h2>
                </div>
                <ul class="brand_test_list">
                    <li class="brand_test_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="brand_test_list_item_photo">
                        <a href="#" class="brand_test_list_item_title">Отзыв о Audi A6 «Ауди a6 4f»</a>
                        <span class="brand_test_list_item_description">Хороший надежный авто ! Красивый внешне и удобен в салоне ! Качество отделки салона на высоте ! На трассе просто класс......едет как утюг....Конечно дороговат в обслуживании , но что сейчас дешево ! Ходовая неплохая...</span>
                    </li>
                    <li class="brand_test_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="brand_test_list_item_photo">
                        <a href="#" class="brand_test_list_item_title">Отзыв о Audi A6 «Ауди a6 4f»</a>
                        <span class="brand_test_list_item_description">Хороший надежный авто ! Красивый внешне и удобен в салоне ! Качество отделки салона на высоте ! На трассе просто класс......едет как утюг....Конечно дороговат в обслуживании , но что сейчас дешево ! Ходовая неплохая...</span>
                    </li>
                    <li class="brand_test_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/photo.png" alt="Volkswagen Passat Alltrack универсал"
                             class="brand_test_list_item_photo">
                        <a href="#" class="brand_test_list_item_title">Отзыв о Audi A6 «Ауди a6 4f»</a>
                        <span class="brand_test_list_item_description">Хороший надежный авто ! Красивый внешне и удобен в салоне ! Качество отделки салона на высоте ! На трассе просто класс......едет как утюг....Конечно дороговат в обслуживании , но что сейчас дешево ! Ходовая неплохая...</span>
                    </li>
                </ul>
            </section>

        </div>
        <div class="sidebar visible-lg">
            <div class="sidebar_banner"><img src="<?= $asset->baseUrl; ?>/img/banner-backing.png" alt=""></div>
            <div class="sidebar_banner"><img src="<?= $asset->baseUrl; ?>/img/banner-backing.png" alt=""></div>
        </div>
    </div>

</div>