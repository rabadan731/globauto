<?php

use themes\globauto\assets\GlobAutoAsset;
use yii\helpers\Url;
use modules\auto\models\CarMark;
use modules\seo\models\Seo;

/* @var $this yii\web\View */
/* @var $mark CarMark */
/* @var $series \modules\auto\models\CarSerie[] */
/* @var $seo Seo */

$this->params['mainClass'] = "main cars row main_bg";
$asset = GlobAutoAsset::register($this);

$this->params['breadcrumbs'][] = [
    "label" => Seo::br('auto/new','index', $seo->replaceData, 'Новые авто'),
    "url" => Url::to(['/auto/new/index'])
];
$this->params['breadcrumbs'][] = $seo->breadcrumb;


?>

<div class="container">
    <div class="row">
        <div class="content col-xs-12"  itemprop="mainContentOfPage">
            <article class="row">
                <div class="clearfix">
                    <h1 class="pagetitle"><?=$seo->h1;?> </h1>
                </div>
                <div class="brand_content clearfix">
                    <div class="brand_content_logo">
                        <img src="<?=$mark->icon;?>" alt="">
                    </div>
                    <p>Ford Motor Company — американский автопроизводитель, четвертый в мире по объемам продаж за всю историю. Штаб-квартира базируется в Дирборне, пригороде Детройта, штат Мичиган. Компания была основана Генри Фордом 16 июня 1903 года. Ford Motor Company в течение своей долгой истории остается одним из самых крупных и прибыльных автопроизводителей, а также одним из немногих, которые пережили Великую депрессию.</p>
                </div>
                <div class="brand_content_full clearfix">
                    <p>Ford Motor Company — американский автопроизводитель, четвертый в мире по объемам продаж за всю историю. Штаб-квартира базируется в Дирборне, пригороде Детройта, штат Мичиган. Компания была основана Генри Фордом 16 июня 1903 года. Ford Motor Company в течение своей долгой истории остается одним из самых крупных и прибыльных автопроизводителей, а также одним из немногих, которые пережили Великую депрессию.	Ford Motor Company — американский автопроизводитель, четвертый в мире по объемам продаж за всю историю. Штаб-квартира базируется в Дирборне, пригороде Детройта, штат Мичиган. Компания была основана Генри Фордом 16 июня 1903 года. Ford Motor Company в течение своей долгой истории остается одним из самых крупных и прибыльных автопроизводителей, а также одним из немногих, которые пережили Великую депрессию.</p>
                </div>
                <a href="#" class="brand_content_more">Читать полностью</a>
            </article>

            <section class="new_catalog">
                <div class="clearfix">
                    <h2 class="title"><?=$seo->h2;?></h2>

                </div>
                <ul class="new_catalog_list row">
                    <?php foreach ($series as $model) { ?>
                        <li class="new_catalog_list_item">
                            <img src="<?= $model->image; ?>" alt="<?=$model->name_cat;?>" class="new_catalog_list_item_photo">
                            <a href="<?= Url::to(['/auto/new/serie', 'mark'=> $mark->slug, 'serie'=> $model->slug]);?>"
                               class="new_catalog_list_item_name"><?=$model->name_cat;?></a>
                            <span class="new_catalog_list_item_cost">от <?=$model->niceCost;?></span>
                        </li>
                    <?php }?>
                </ul>
            </section>

            <section class="used_catalog phone_button row">
                <div class="clearfix">
                    <h2 class="title">FORD с пробегом в Уфе</h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="used_catalog_list clearfix">
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                </ul>
            </section>

            <section class="brand_test  row">
                <div class="clearfix">
                    <h2 class="title">ТЕСТ-ДРАЙВЫ FORD</h2>
                </div>
                <ul class="brand_test_list">
                    <li class="brand_test_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/photo.png" alt="Volkswagen Passat Alltrack универсал" class="brand_test_list_item_photo">
                        <a href="#" class="brand_test_list_item_title">Двадцатилетний юбилей. Тест-драйв Ford Mondeo «Anniversary 20»</a>
                        <span class="brand_test_list_item_date">2 декабря 2013</span>
                        <span class="brand_test_list_item_description">В нынешнем году исполняется 20 лет со дня первого выпуска в свет первого Ford Mondeo. Событие это не могло пройти незамеченным, и поэтому на заводе «Форд» во Всеволжске запустилось производство ограниченной серии «Anniversary 20», созданной в честь юбилея.</span>
                    </li>
                    <li class="brand_test_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/photo.png" alt="Volkswagen Passat Alltrack универсал" class="brand_test_list_item_photo">
                        <a href="#" class="brand_test_list_item_title">Двадцатилетний юбилей. Тест-драйв Ford Mondeo «Anniversary 20»</a>
                        <span class="brand_test_list_item_date">2 декабря 2013</span>
                        <span class="brand_test_list_item_description">В нынешнем году исполняется 20 лет со дня первого выпуска в свет первого Ford Mondeo. Событие это не могло пройти незамеченным, и поэтому на заводе «Форд» во Всеволжске запустилось производство ограниченной серии «Anniversary 20», созданной в честь юбилея.</span>
                    </li>
                    <li class="brand_test_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/photo.png" alt="Volkswagen Passat Alltrack универсал" class="brand_test_list_item_photo">
                        <a href="#" class="brand_test_list_item_title">Двадцатилетний юбилей. Тест-драйв Ford Mondeo «Anniversary 20»</a>
                        <span class="brand_test_list_item_date">2 декабря 2013</span>
                        <span class="brand_test_list_item_description">В нынешнем году исполняется 20 лет со дня первого выпуска в свет первого Ford Mondeo. Событие это не могло пройти незамеченным, и поэтому на заводе «Форд» во Всеволжске запустилось производство ограниченной серии «Anniversary 20», созданной в честь юбилея.</span>
                    </li>
                </ul>
            </section>

            <section class="brand_news phone_button  row">
                <div class="clearfix">
                    <h2 class="title">ПУБЛИКАЦИИ FORD</h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="brand_news_list">
                    <li class="brand_news_list_item col-sm-6 col-md-4">
                        <a class="facade_news_list_item" href="#">
                            <img src="<?= $asset->baseUrl; ?>/img/news.jpg" alt="" class="facade_news_list_item_photo">
                            <span class="facade_news_list_item_description">
												<span class="facade_news_list_item_name">Rolls-Royce Phantom «в зените» или Прощальная версия легендарного автомобиля</span>
												<span class="facade_news_list_item_info">
													<span class="facade_news_list_item_info_date">24 мая 2016 </span>
													<span class="facade_news_list_item_info_comments">21</span>
												</span>
											</span>
                        </a>
                    </li>
                    <li class="brand_news_list_item col-sm-6 col-md-4">
                        <a class="facade_news_list_item" href="#">
                            <img src="<?= $asset->baseUrl; ?>/img/news.jpg" alt="" class="facade_news_list_item_photo">
                            <span class="facade_news_list_item_description">
												<span class="facade_news_list_item_name">Rolls-Royce Phantom «в зените» или Прощальная версия легендарного автомобиля</span>
												<span class="facade_news_list_item_info">
													<span class="facade_news_list_item_info_date">24 мая 2016 </span>
													<span class="facade_news_list_item_info_comments">21</span>
												</span>
											</span>
                        </a>
                    </li>
                    <li class="brand_news_list_item col-sm-6 col-md-4">
                        <a class="facade_news_list_item" href="#">
                            <img src="<?= $asset->baseUrl; ?>/img/news.jpg" alt="" class="facade_news_list_item_photo">
                            <span class="facade_news_list_item_description">
												<span class="facade_news_list_item_name">Rolls-Royce Phantom «в зените» или Прощальная версия легендарного автомобиля</span>
												<span class="facade_news_list_item_info">
													<span class="facade_news_list_item_info_date">24 мая 2016 </span>
													<span class="facade_news_list_item_info_comments">21</span>
												</span>
											</span>
                        </a>
                    </li>
                    <li class="brand_news_list_item col-sm-6 col-md-4">
                        <a class="facade_news_list_item" href="#">
                            <img src="<?= $asset->baseUrl; ?>/img/news.jpg" alt="" class="facade_news_list_item_photo">
                            <span class="facade_news_list_item_description">
												<span class="facade_news_list_item_name">Rolls-Royce Phantom «в зените» или Прощальная версия легендарного автомобиля</span>
												<span class="facade_news_list_item_info">
													<span class="facade_news_list_item_info_date">24 мая 2016 </span>
													<span class="facade_news_list_item_info_comments">21</span>
												</span>
											</span>
                        </a>
                    </li>
                    <li class="brand_news_list_item col-sm-6 col-md-4">
                        <a class="facade_news_list_item" href="#">
                            <img src="<?= $asset->baseUrl; ?>/img/news.jpg" alt="" class="facade_news_list_item_photo">
                            <span class="facade_news_list_item_description">
												<span class="facade_news_list_item_name">Rolls-Royce Phantom «в зените» или Прощальная версия легендарного автомобиля</span>
												<span class="facade_news_list_item_info">
													<span class="facade_news_list_item_info_date">24 мая 2016 </span>
													<span class="facade_news_list_item_info_comments">21</span>
												</span>
											</span>
                        </a>
                    </li>
                    <li class="brand_news_list_item col-sm-6 col-md-4">
                        <a class="facade_news_list_item" href="#">
                            <img src="<?= $asset->baseUrl; ?>/img/news.jpg" alt="" class="facade_news_list_item_photo">
                            <span class="facade_news_list_item_description">
												<span class="facade_news_list_item_name">Rolls-Royce Phantom «в зените» или Прощальная версия легендарного автомобиля</span>
												<span class="facade_news_list_item_info">
													<span class="facade_news_list_item_info_date">24 мая 2016 </span>
													<span class="facade_news_list_item_info_comments">21</span>
												</span>
											</span>
                        </a>
                    </li>
                </ul>
            </section>

            <ul class="brand_more_list clearfix">
                <li class="brand_more_list_item col-sm-4">
                    <a href="#" class="brand_more_list_item_link brand_more_list_item_link_shares">Скидки на Ford в Уфе</a>
                </li>
                <li class="brand_more_list_item col-sm-8">
                    <a href="#" class="brand_more_list_item_link brand_more_list_item_link_news">Новости о Ford</a>
                </li>
                <li class="brand_more_list_item col-sm-4">
                    <a href="#" class="brand_more_list_item_link brand_more_list_item_link_used">Ford с пробегом в Уфе</a>
                </li>
                <li class="brand_more_list_item col-sm-8">
                    <a href="#" class="brand_more_list_item_link brand_more_list_item_link_reviews">Отзывы о Ford</a>
                </li>
            </ul>

        </div>

        <div class="sidebar visible-lg">
            <div class="sidebar_banner"><img src="<?= $asset->baseUrl; ?>/img/banner-backing.png" alt=""></div>
            <div class="sidebar_banner"><img src="<?= $asset->baseUrl; ?>/img/banner-backing.png" alt=""></div>
        </div>
    </div>

</div>