<?php

use themes\globauto\assets\GlobAutoAsset;
use yii\helpers\Url;
use modules\auto\models\CarMark;
use modules\seo\models\Seo;

/* @var $this yii\web\View */
/* @var $mark CarMark */
/* @var $serie \modules\auto\models\CarSerie */
/* @var $series \modules\auto\models\CarSerie[] */
/* @var $model \modules\auto\models\CarModel */
/* @var $models \modules\auto\models\CarModel[] */
/* @var $modifications \modules\auto\models\CarModification[] */
/* @var $seo Seo */

$this->params['mainClass'] = "main cars row main_bg";
$asset = GlobAutoAsset::register($this);

$this->params['breadcrumbs'][] = [
    "label" => Seo::br('auto/new','index', $seo->replaceData, 'Новые авто'),
    "url" => Url::to(['/auto/new/index'])
];
$this->params['breadcrumbs'][] = [
    "label" => Seo::br('auto/new','mark', $seo->replaceData, '{{mark}}'),
    "url" => Url::to(['/auto/new/mark', 'mark' => $mark->slug])
];

$this->params['breadcrumbs'][] = $seo->breadcrumb;

?>
<div class="container">
    <div class="row">
        <div class="row">
            <h1 class="pagetitle"><?= $seo->h1; ?></h1>
        </div>
        <div class="model_photo_slider clearfix">
            <div id="sync1" class="owl-carousel ">
                <?php foreach ($model->photoBig as $photo) { ?>
                    <div class="item">
                        <img src="<?= $photo->image;?>" alt="">
                    </div>
                <?php } ?>
                <?php foreach ($serie->carVideos as $video) { ?>
                    <div class="item">
                        <?= $video->youtubeFrame;?>
                    </div>
                <?php } ?>
            </div>
            <div id="sync2" class="owl-carousel hidden-xs">
                <?php foreach ($model->photoSmall as $photo) { ?>
                    <div class="item">
                        <img src="<?= $photo->image;?>" alt="">
                    </div>
                <?php } ?>
                <?php foreach ($serie->carVideos as $video) { ?>
                    <div class="item">
                        <img src="<?= $video->image;?>" alt="">
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="content col-xs-12"  itemprop="mainContentOfPage">

            <article class="clearfix">
                <div class="row">
                    <h2 class="title"><?= $seo->h2; ?></h2>
                </div>
                <div class="brand_content clearfix serie_text">
                    <p><?= $serie->text; ?></p>
                </div>
            </article>

            <section class="model_price">
                <div class="row">
                    <h2 class="title">ЦЕНА НА <?= $mark->name; ?> <?= $model->name; ?> <?= $serie->name; ?></h2>
                </div>
                <ul class="model_price_list clearfix">
                    <li class="model_price_list_item">
                        <div class="model_price_list_item_components">Комплектация</div>
                        <div class="model_price_list_item_drive">Привод</div>
                        <div class="model_price_list_item_engine">Двигатель, л</div>
                        <div class="model_price_list_item_power">Мощность, л.с.</div>
                        <div class="model_price_list_item_transmission">КПП</div>
                        <div class="model_price_list_item_cost">Цена, руб.</div>
                    </li>
                    <?php foreach ($modifications as $modification) { ?>
                        <li class="model_price_list_item">
                            <div class="model_price_list_item_components">
                                <?= $modification->name; ?>
                            </div>
                            <div class="model_price_list_item_drive">
                                <?= $modification->getCharacteristic(27); ?>
                            </div>
                            <div class="model_price_list_item_engine">
                                <?= round($modification->getCharacteristic(13)/1000,2); ?>,
                                <?= $modification->getCharacteristic(12); ?>
                            </div>
                            <div class="model_price_list_item_power">
                                <?= $modification->getCharacteristic(14); ?>
                            </div>
                            <div class="model_price_list_item_transmission">
                                <?= $modification->getCharacteristic(24); ?>
                            </div>
                            <div class="model_price_list_item_cost">0</div>
                        </li>
                    <?php } ?>
                </ul>
            </section>

            <?php if (count($series)) { ?>
                <section class="new_catalog">
                    <div class="row">
                        <h2 class="title">ДРУГИЕ КУЗОВА <?= $mark->name; ?> <?= $model->name; ?></h2>
                    </div>
                    <ul class="new_catalog_list row">
                        <?php foreach ($series as $item) { ?>
                            <li class="new_catalog_list_item">
                                <img src="<?= $item->image; ?>" alt="<?= $item->name_cat; ?>" class="new_catalog_list_item_photo">
                                <a href="<?= Url::to(['/auto/new/serie', 'mark'=> $mark->slug, 'serie'=> $item->slug]);?>"
                                   class="new_catalog_list_item_name"><?= $item->name_cat; ?></a>
                                <span class="new_catalog_list_item_cost"><?= $item->niceCost; ?></span>
                            </li>
                        <?php } ?>
                    </ul>
                </section>
            <?php } ?>

            <section class="used_catalog phone_button row">
                <div class="clearfix">
                    <h2 class="title">FORD с пробегом в Уфе</h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="used_catalog_list clearfix">
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="used_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                        <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                        <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                </ul>
            </section>

            <section class="new_catalog">
                <div class="row">
                    <h2 class="title">ПОХОЖИЕ АВТОМОБИЛИ</h2>
                </div>
                <ul class="new_catalog_list row">
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                    <li class="new_catalog_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="new_catalog_list_item_photo">
                        <a href="#" class="new_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                        <span class="new_catalog_list_item_cost">от 2 359 000 руб.</span>
                    </li>
                </ul>
            </section>

            <section class="brand_test  row">
                <div class="clearfix">
                    <h2 class="title">ТЕСТ-ДРАЙВЫ FORD</h2>
                </div>
                <ul class="brand_test_list col-xs-12">
                    <li class="brand_test_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/photo.png" alt="Volkswagen Passat Alltrack универсал" class="brand_test_list_item_photo">
                        <a href="#" class="brand_test_list_item_title">Двадцатилетний юбилей. Тест-драйв Ford Mondeo «Anniversary 20»</a>
                        <span class="brand_test_list_item_date">2 декабря 2013</span>
                        <span class="brand_test_list_item_description">В нынешнем году исполняется 20 лет со дня первого выпуска в свет первого Ford Mondeo. Событие это не могло пройти незамеченным, и поэтому на заводе «Форд» во Всеволжске запустилось производство ограниченной серии «Anniversary 20», созданной в честь юбилея.</span>
                    </li>
                    <li class="brand_test_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/photo.png" alt="Volkswagen Passat Alltrack универсал" class="brand_test_list_item_photo">
                        <a href="#" class="brand_test_list_item_title">Двадцатилетний юбилей. Тест-драйв Ford Mondeo «Anniversary 20»</a>
                        <span class="brand_test_list_item_date">2 декабря 2013</span>
                        <span class="brand_test_list_item_description">В нынешнем году исполняется 20 лет со дня первого выпуска в свет первого Ford Mondeo. Событие это не могло пройти незамеченным, и поэтому на заводе «Форд» во Всеволжске запустилось производство ограниченной серии «Anniversary 20», созданной в честь юбилея.</span>
                    </li>
                    <li class="brand_test_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/photo.png" alt="Volkswagen Passat Alltrack универсал" class="brand_test_list_item_photo">
                        <a href="#" class="brand_test_list_item_title">Двадцатилетний юбилей. Тест-драйв Ford Mondeo «Anniversary 20»</a>
                        <span class="brand_test_list_item_date">2 декабря 2013</span>
                        <span class="brand_test_list_item_description">В нынешнем году исполняется 20 лет со дня первого выпуска в свет первого Ford Mondeo. Событие это не могло пройти незамеченным, и поэтому на заводе «Форд» во Всеволжске запустилось производство ограниченной серии «Anniversary 20», созданной в честь юбилея.</span>
                    </li>
                </ul>
            </section>

            <section class="brand_news phone_button  row">
                <div class="clearfix">
                    <h2 class="title">ПУБЛИКАЦИИ FORD</h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="brand_news_list">
                    <li class="brand_news_list_item col-sm-6 col-md-4">
                        <a class="facade_news_list_item" href="#">
                            <img src="<?= $asset->baseUrl; ?>/img/news.jpg" alt="" class="facade_news_list_item_photo">
                            <span class="facade_news_list_item_description">
												<span class="facade_news_list_item_name">Rolls-Royce Phantom «в зените» или Прощальная версия легендарного автомобиля</span>
												<span class="facade_news_list_item_info">
													<span class="facade_news_list_item_info_date">24 мая 2016 </span>
													<span class="facade_news_list_item_info_comments">21</span>
												</span>
											</span>
                        </a>
                    </li>
                    <li class="brand_news_list_item col-sm-6 col-md-4">
                        <a class="facade_news_list_item" href="#">
                            <img src="<?= $asset->baseUrl; ?>/img/news.jpg" alt="" class="facade_news_list_item_photo">
                            <span class="facade_news_list_item_description">
												<span class="facade_news_list_item_name">Rolls-Royce Phantom «в зените» или Прощальная версия легендарного автомобиля</span>
												<span class="facade_news_list_item_info">
													<span class="facade_news_list_item_info_date">24 мая 2016 </span>
													<span class="facade_news_list_item_info_comments">21</span>
												</span>
											</span>
                        </a>
                    </li>
                    <li class="brand_news_list_item col-sm-6 col-md-4">
                        <a class="facade_news_list_item" href="#">
                            <img src="<?= $asset->baseUrl; ?>/img/news.jpg" alt="" class="facade_news_list_item_photo">
                            <span class="facade_news_list_item_description">
												<span class="facade_news_list_item_name">Rolls-Royce Phantom «в зените» или Прощальная версия легендарного автомобиля</span>
												<span class="facade_news_list_item_info">
													<span class="facade_news_list_item_info_date">24 мая 2016 </span>
													<span class="facade_news_list_item_info_comments">21</span>
												</span>
											</span>
                        </a>
                    </li>
                    <li class="brand_news_list_item col-sm-6 col-md-4">
                        <a class="facade_news_list_item" href="#">
                            <img src="<?= $asset->baseUrl; ?>/img/news.jpg" alt="" class="facade_news_list_item_photo">
                            <span class="facade_news_list_item_description">
												<span class="facade_news_list_item_name">Rolls-Royce Phantom «в зените» или Прощальная версия легендарного автомобиля</span>
												<span class="facade_news_list_item_info">
													<span class="facade_news_list_item_info_date">24 мая 2016 </span>
													<span class="facade_news_list_item_info_comments">21</span>
												</span>
											</span>
                        </a>
                    </li>
                    <li class="brand_news_list_item col-sm-6 col-md-4">
                        <a class="facade_news_list_item" href="#">
                            <img src="<?= $asset->baseUrl; ?>/img/news.jpg" alt="" class="facade_news_list_item_photo">
                            <span class="facade_news_list_item_description">
												<span class="facade_news_list_item_name">Rolls-Royce Phantom «в зените» или Прощальная версия легендарного автомобиля</span>
												<span class="facade_news_list_item_info">
													<span class="facade_news_list_item_info_date">24 мая 2016 </span>
													<span class="facade_news_list_item_info_comments">21</span>
												</span>
											</span>
                        </a>
                    </li>
                    <li class="brand_news_list_item col-sm-6 col-md-4">
                        <a class="facade_news_list_item" href="#">
                            <img src="<?= $asset->baseUrl; ?>/img/news.jpg" alt="" class="facade_news_list_item_photo">
                            <span class="facade_news_list_item_description">
												<span class="facade_news_list_item_name">Rolls-Royce Phantom «в зените» или Прощальная версия легендарного автомобиля</span>
												<span class="facade_news_list_item_info">
													<span class="facade_news_list_item_info_date">24 мая 2016 </span>
													<span class="facade_news_list_item_info_comments">21</span>
												</span>
											</span>
                        </a>
                    </li>
                </ul>
            </section>

            <section class="cars_reviews brand_test row  phone_button">
                <div class="clearfix">
                    <h2 class="title">ПОСЛЕДНИЕ ОТЗЫВЫ НА FORD FIESTA</h2>
                    <a href="#" class="more">Смотреть все</a>
                </div>
                <ul class="brand_test_list col-xs-12">
                    <li class="brand_test_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/photo.png" alt="Volkswagen Passat Alltrack универсал" class="brand_test_list_item_photo">
                        <a href="#" class="brand_test_list_item_title">Отзыв о Audi A6 «Ауди a6 4f»</a>
                        <span class="brand_test_list_item_description">Хороший надежный авто ! Красивый внешне и удобен в салоне ! Качество отделки салона на высоте ! На трассе просто класс......едет как утюг....Конечно дороговат в обслуживании , но что сейчас дешево ! Ходовая неплохая...</span>
                    </li>
                    <li class="brand_test_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/photo.png" alt="Volkswagen Passat Alltrack универсал" class="brand_test_list_item_photo">
                        <a href="#" class="brand_test_list_item_title">Отзыв о Audi A6 «Ауди a6 4f»</a>
                        <span class="brand_test_list_item_description">Хороший надежный авто ! Красивый внешне и удобен в салоне ! Качество отделки салона на высоте ! На трассе просто класс......едет как утюг....Конечно дороговат в обслуживании , но что сейчас дешево ! Ходовая неплохая...</span>
                    </li>
                    <li class="brand_test_list_item">
                        <img src="<?= $asset->baseUrl; ?>/img/photo.png" alt="Volkswagen Passat Alltrack универсал" class="brand_test_list_item_photo">
                        <a href="#" class="brand_test_list_item_title">Отзыв о Audi A6 «Ауди a6 4f»</a>
                        <span class="brand_test_list_item_description">Хороший надежный авто ! Красивый внешне и удобен в салоне ! Качество отделки салона на высоте ! На трассе просто класс......едет как утюг....Конечно дороговат в обслуживании , но что сейчас дешево ! Ходовая неплохая...</span>
                    </li>
                </ul>
            </section>

        </div>

        <div class="sidebar visible-lg">
            <div class="sidebar_banner"><img src="<?= $asset->baseUrl; ?>/img/banner-backing.png" alt=""></div>
            <div class="sidebar_banner"><img src="<?= $asset->baseUrl; ?>/img/banner-backing.png" alt=""></div>
        </div>
    </div>

</div>