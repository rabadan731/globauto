<?php

/**
 * Created by PhpStorm.
 * User: rabadan731
 * Date: 26.10.2017
 * Time: 22:37
 */

?>

<div class="filter tabs clearfix" id="filter">
    <div class="title">
        <ul class="tabs-nav">
            <li class="active">ПО ПАРАМЕТРАМ</li>
            <li>ПО МАРКЕ</li>
        </ul>
    </div>
    <div class="tabs-box">

        <div class="active">
            <form class="filter_form">
                <div class="form_line">
                    <select class="filter_city">
                        <option value="Уфа(123)">Уфа(123)</option>
                        <option value="Питер(456)">Питер(456)</option>
                        <option value="Москва(789)">Москва(789)</option>
                    </select>
                    <a href="#" class="filter_add">Добавить регион</a>
                </div>
                <div class="form_line">
                    <div class="col-xs-6">
                        <select class="filter_make">
                            <option value="1 марка" class="filter_city_item">1 марка</option>
                            <option value="2 марка" class="filter_city_item">2 марка</option>
                            <option value="3 марка" class="filter_city_item">3 марка</option>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <select class="filter_model">
                            <option value="1 модель" class="filter_city_item">1 модель</option>
                            <option value="2 модель" class="filter_city_item">2 модель</option>
                            <option value="3 модель" class="filter_city_item">3 модель</option>
                        </select>
                    </div>
                    <a href="#" class="filter_add">Добавить марку</a>
                </div>
                <div class="form_line filter_cost">
                    <div class="col-xs-6">
                        <input type="text" placeholder="Цена от">
                    </div>
                    <div class="col-xs-6">
                        <input type="text" placeholder="Цена до">
                    </div>

                </div>
                <div class="form_line">
                    <div class="col-xs-6">
                        <select class="filter_year_from">
                            <option value="1900" class="filter_city_item">1900</option>
                            <option value="2000" class="filter_city_item">2000</option>
                            <option value="2100" class="filter_city_item">2100</option>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <select class="filter_year_to">
                            <option value="1900" class="filter_city_item">1900</option>
                            <option value="2000" class="filter_city_item">2000</option>
                            <option value="2100" class="filter_city_item">2100</option>
                        </select>
                    </div>
                </div>
                <div class="form_line filter_body">
                    <div class="col-xs-6">
                        <input type="checkbox" id="1"  name="1" />
                        <label for="1">Все кузова</label>
                    </div>
                    <div class="col-xs-6">
                        <input type="checkbox" id="2" name="2">
                        <label for="2">Хэтчбек</label>
                    </div>
                    <div class="col-xs-6">
                        <input type="checkbox" id="3" name="3">
                        <label for="3">Седан</label>
                    </div>
                    <div class="col-xs-6">
                        <input type="checkbox" id="4" name="4">
                        <label for="4">Кроссовер</label>
                    </div>
                    <div class="col-xs-6">
                        <input type="checkbox" id="5" name="5">
                        <label for="5">Универсал</label>
                    </div>
                    <div class="col-xs-6">
                        <input type="checkbox" id="6" name="6">
                        <label for="6">Минивен</label>
                    </div>
                    <a href="#" class="filter_add">Другие типы кузовов</a>
                </div>
                <div class="form_line">
                    <select class="filter_mileage">
                        <option value="" selected>Пробег от</option>
                        <option value="12000">12000</option>
                        <option value="13000">13000</option>
                        <option value="50000">50000</option>
                    </select>
                </div>
                <div class="form_line filter_transmission">
                    <div class="col-xs-6">
                        <input type="checkbox" id="7"  name="7" />
                        <label for="7">АКПП</label>
                    </div>
                    <div class="col-xs-6">
                        <input type="checkbox" id="8"  name="8" />
                        <label for="8">МКПП</label>
                    </div>
                </div>
                <div class="form_line filter_color">
                    <div class="col-xs-12">
                        <input type="checkbox" id="9"  name="9" checked/>
                        <label for="9">Любой цвет</label>
                    </div>
                    <div class="filter_color_inner">
                        <div class="filter_color_inner_box">
                            <input type="checkbox" id="c1"  name="c1"/>
                            <label for="c1"></label>
                        </div>
                        <div class="filter_color_inner_box">
                            <input type="checkbox" id="c2"  name="c2"/>
                            <label for="c2"></label>
                        </div>
                        <div class="filter_color_inner_box">
                            <input type="checkbox" id="c3"  name="c3"/>
                            <label for="c3"></label>
                        </div>
                        <div class="filter_color_inner_box">
                            <input type="checkbox" id="c4"  name="c4"/>
                            <label for="c4"></label>
                        </div>
                        <div class="filter_color_inner_box">
                            <input type="checkbox" id="c5"  name="c5"/>
                            <label for="c5"></label>
                        </div>
                        <div class="filter_color_inner_box">
                            <input type="checkbox" id="c6"  name="c6"/>
                            <label for="c6"></label>
                        </div>
                        <div class="filter_color_inner_box">
                            <input type="checkbox" id="c7"  name="c7"/>
                            <label for="c7"></label>
                        </div>
                        <div class="filter_color_inner_box">
                            <input type="checkbox" id="c8"  name="c8"/>
                            <label for="c8"></label>
                        </div>
                    </div>
                </div>
                <div class="form_line">
                    <input type="submit" class="filter_submit button lilac" value="Показать">
                </div>
            </form>
        </div>
        <div>
            <ul class="brands_catalog_list clearfix">
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
                <li class="brands_catalog_list_item">
                    <a href="#" class="brands_catalog_list_item_link">
                        <img src="img/peugeot.png" alt="Land Rover" class="brands_catalog_list_item_logotip">
                        <span class="brands_catalog_list_item_name">Land Rover</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
