<?php

/* @var $seo \modules\seo\models\Seo */

$this->params['mainClass'] = "main used car model brand main_bg row";

?>

<div class="container">
    <div class="row">
        <ul class="breadcrumbs hidden-xs row"  itemprop="breadcrumb">
            <li><a href="#">Главная</a></li>
            <li><span>Объявления</span></li>
        </ul>
        <div class="row">
            <h1 class="pagetitle"><?= $seo->h1; ?></h1>
            <a href="#filter" class="filter_button hidden-lg button orange popup_box2">отфильтровать</a>
        </div>

        <div class="content col-xs-12"  itemprop="mainContentOfPage">

            <ul class="brand_more_list hidden-xs clearfix">
                <li class="brand_more_list_item col-sm-4">
                    <a href="#" class="brand_more_list_item_link brand_more_list_item_link_shares">Скидки на Audi в Уфе</a>
                </li>
                <li class="brand_more_list_item col-sm-4">
                    <a href="#" class="brand_more_list_item_link brand_more_list_item_link_news">Новости о Audi</a>
                </li>
                <li class="brand_more_list_item col-sm-4">
                    <a href="#" class="brand_more_list_item_link brand_more_list_item_link_used">Audi с пробегом в Уфе</a>
                </li>
                <li class="brand_more_list_item col-sm-4">
                    <a href="#" class="brand_more_list_item_link brand_more_list_item_link_catalog">Каталог Audi в Уфе</a>
                </li>
                <li class="brand_more_list_item col-sm-4">
                    <a href="#" class="brand_more_list_item_link brand_more_list_item_link_reviews">Отзывы о Audi</a>
                </li>
            </ul>

            <div class="car_model hidden-xs">
                <a href="#" class="car_model_button">a4</a>
                <a href="#" class="car_model_button">a5</a>
                <a href="#" class="car_model_button">a6</a>
                <a href="#" class="car_model_button">a7</a>
                <a href="#" class="car_model_button">q3</a>
                <a href="#" class="car_model_button">q5</a>
                <a href="#" class="car_model_button">q7</a>
                <a href="#" class="car_model_button">s8</a>
            </div>

            <div class="sorting">
                <span class="sorting_title">Сортировка:</span>
                <a href="#" class="sorting_button">Дата</a>
                <a href="#" class="sorting_button">Стоимость</a>
                <a href="#" class="sorting_button">Пробег</a>
                <a href="#" class="sorting_button">Год выпуска</a>
                <a href="#" class="catalog_view_line hidden-xs"></a>
                <a href="#" class="catalog_view_block hidden-xs"></a>

            </div>

            <ul class="used_catalog_list row">
                <li class="used_catalog_list_item">
                    <img src="img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                    <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                    <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                    <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                </li>
                <li class="used_catalog_list_item">
                    <img src="img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                    <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                    <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                    <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                </li>
                <li class="used_catalog_list_item">
                    <img src="img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                    <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                    <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                    <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                </li>
                <li class="used_catalog_list_item">
                    <img src="img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                    <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                    <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                    <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                </li>
                <li class="used_catalog_list_item">
                    <img src="img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                    <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                    <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                    <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                </li>
                <li class="used_catalog_list_item">
                    <img src="img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                    <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                    <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                    <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                </li>
                <li class="used_catalog_list_item">
                    <img src="img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                    <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                    <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                    <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                </li>
                <li class="used_catalog_list_item">
                    <img src="img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                    <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                    <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                    <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                </li>
                <li class="used_catalog_list_item">
                    <img src="img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                    <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                    <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                    <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                </li>
                <li class="used_catalog_list_item">
                    <img src="img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                    <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                    <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                    <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                </li>
                <li class="used_catalog_list_item">
                    <img src="img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                    <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                    <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                    <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                </li>
                <li class="used_catalog_list_item">
                    <img src="img/10th-photo.png" alt="Volkswagen Passat Alltrack универсал" class="used_catalog_list_item_photo">
                    <a href="#" class="used_catalog_list_item_name">Volkswagen Passat Alltrack универсал</a>
                    <span class="used_catalog_list_item_description">2011, 64 150 км</span>
                    <span class="used_catalog_list_item_cost">от 2 359 000 руб.</span>
                </li>
            </ul>


        </div>



        <div class="sidebar visible-lg">
            <?= $this->render('_filter'); ?>
        </div>
    </div>

</div>
