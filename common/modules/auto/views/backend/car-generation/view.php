<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarGeneration */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Generations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-generation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('cars', 'Update'), ['update', 'id' => $model->id_car_generation], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('cars', 'Delete'), ['delete', 'id' => $model->id_car_generation], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('cars', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_car_generation',
            'name',
            'id_car_model',
            'year_begin',
            'year_end',
            'date_create',
            'date_update',
            'id_car_type',
        ],
    ]) ?>

</div>
