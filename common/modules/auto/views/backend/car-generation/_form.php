<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\auto\models\CarModel;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarGeneration */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-generation-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_car_model')
        ->dropDownList(CarModel::listGroupDropdown(), ['prompt' => '-Выберите модель-']) ?>

    <?= $form->field($model, 'year_begin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'year_end')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
