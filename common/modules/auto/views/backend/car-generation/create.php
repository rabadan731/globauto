<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarGeneration */
$this->title = Yii::t('cars', 'Add: {model}', [
    'model' => Yii::t('cars', 'Car Generations'),
]);
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Generations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-generation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
