<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarGeneration */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => "{$model->name}[{$model->id_car_generation}]",
]);

$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Generations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_car_generation]];
$this->params['breadcrumbs'][] = Yii::t('cars', 'Update');
?>
<div class="car-generation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
