<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\auto\models\CarOption;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarOption */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-option-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_parent')->dropDownList(
        CarOption::listDropdownParent(),
        ['prompt' => '-выберите родительскую опцию-']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('cars', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
