<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\assets\AjaxFormAsset;
use modules\auto\models\CarOption;

/* @var $this yii\web\View */
/* @var $searchModel modules\auto\models\search\CarOptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cars', 'Car Options');
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = $this->title;

AjaxFormAsset::register($this);

?>
<div class="car-option-index">
    <div class="row">

        <div class="col-md-6">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id_car_option',
            [
                'attribute'=>'id_parent',
                'value'=>'parent.name',
                'filter' => CarOption::listDropdownParent()
            ],
            'name',
            //'id_parent',
            //'date_create',
            //'date_update',
            // 'id_car_type',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{ajax-update} {delete}',
                'options' => ['style' => 'width:70px']
            ],
        ],
    ]); ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
