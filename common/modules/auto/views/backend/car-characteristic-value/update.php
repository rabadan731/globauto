<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarCharacteristicValue */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => "{$model->value}[{$model->id_car_characteristic_value}]",
]);
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Characteristic Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_car_characteristic_value, 'url' => ['view', 'id' => $model->id_car_characteristic_value]];
$this->params['breadcrumbs'][] = Yii::t('cars', 'Update');
?>
<div class="car-characteristic-value-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
