<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarCharacteristicValue */

$this->title = Yii::t('cars', 'Add: {model}', [
    'model' => Yii::t('cars', 'Car Characteristic Values'),
]);
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Characteristic Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-characteristic-value-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
