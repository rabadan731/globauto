<?php

use yii\helpers\Html;
use common\assets\AjaxFormAsset;
use yii\grid\GridView;
use modules\auto\models\CarMark;
use modules\auto\models\CarModel;
use modules\auto\models\CarModification;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel modules\auto\models\search\CarCharacteristicValueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cars', 'Car Characteristic Values');
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = $this->title;

AjaxFormAsset::register($this);

$model_id = false;
$mark_id = false;

$models = [];

$query = $dataProvider->query;
$findMarks = CarMark::find()->select('car_mark.id_car_mark')->distinct();
$findModel = CarModification::find()->select('car_modification.id_car_model')->distinct();


$findMarks->joinWith('carModels');
$findMarks->andWhere([
    'in',
    'car_model.id_car_model',
    ArrayHelper::getColumn($findModel->asArray()->all(), 'id_car_model')
]);

$marks = ArrayHelper::getColumn($findMarks->asArray()->all(), 'id_car_mark');

if (isset($query->where['car_mark.id_car_mark'])) {
    $mark_id = $query->where['car_mark.id_car_mark'];
    $findModel->markId($mark_id);
}

if (isset($query->where['car_model.id_car_model'])) {
    $model_id = $query->where['car_model.id_car_model'];
}

$models = ArrayHelper::getColumn($findModel->asArray()->all(),'id_car_model');

if (isset($query->where['car_model.id_car_model'])) {
    $model_id = $query->where['car_model.id_car_model'];
}

if ($model_id) {
    $findModification = CarModification::listDropdown('id_car_modification','name',$model_id);
}


?>
<div class="car-characteristic-value-index">
    <div class="row">

        <div class="col-md-6">
            <p>
                <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id_car_characteristic_value',
                    [
                        'attribute' => 'id_car_mark',
                        'value' => 'carModification.carModel.carMark.name',
                        'filter' => CarMark::listDropdown('id_car_mark', 'name', $marks)
                    ],
                    [
                        'attribute'=>'id_car_model',
                        'value'=>'carModification.carModel.name',
                        'filter' => CarModel::listDropdown('id_car_model','name',$mark_id,$models)
                    ],
                    [
                        'attribute' => 'id_car_modification',
                        'value' => 'carModification.name',
                        'filter' => $model_id?$findModification:'--Выбирете модель--',
                    ],
                    [
                        'attribute' => 'id_car_characteristic',
                        'value' => 'carCharacteristic.name',
                    ],

                    'value',
                    'unit',
                    // 'date_create',
                    // 'date_update',
                    // 'id_car_type',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{ajax-update} {delete}',
                        'options' => ['style' => 'width:70px']
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
