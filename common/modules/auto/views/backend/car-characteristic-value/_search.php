<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\search\CarCharacteristicValueSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-characteristic-value-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_car_characteristic_value') ?>

    <?= $form->field($model, 'value') ?>

    <?= $form->field($model, 'unit') ?>

    <?= $form->field($model, 'id_car_characteristic') ?>

    <?= $form->field($model, 'id_car_modification') ?>

    <?php // echo $form->field($model, 'date_create') ?>

    <?php // echo $form->field($model, 'date_update') ?>

    <?php // echo $form->field($model, 'id_car_type') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('cars', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('cars', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
