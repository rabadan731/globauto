<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarCharacteristicValue */

$this->title = $model->id_car_characteristic_value;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Characteristic Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-characteristic-value-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('cars', 'Update'), ['update', 'id' => $model->id_car_characteristic_value], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('cars', 'Delete'), ['delete', 'id' => $model->id_car_characteristic_value], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('cars', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_car_characteristic_value',
            'value',
            'unit',
            'id_car_characteristic',
            'id_car_modification',
            'date_create',
            'date_update',
            'id_car_type',
        ],
    ]) ?>

</div>
