<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\auto\models\CarModification;
use modules\auto\models\CarCharacteristic;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarCharacteristicValue */
/* @var $form yii\widgets\ActiveForm */

$findModification = CarModification::listDropdown('id_car_modification', 'name', $model->id_car_model);

?>

<div class="car-characteristic-value-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <?= $form->field($model, 'id_car_modification')->dropDownList(
        $findModification,
        ['prompt' => '-Выберите-']
    ) ?>

    <?= $form->field($model, 'id_car_characteristic')->dropDownList(
        CarCharacteristic::listDropdown(),
        ['prompt' => '-Выберите-']
    ) ?>

    <div class="row">
        <div class="col-md-9">
            <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
