<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarCharacteristicValue */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => "{$model->value}[{$model->id_car_characteristic_value}]",
]);
?>
<div class="car-characteristic-value-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
