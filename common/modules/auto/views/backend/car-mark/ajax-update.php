<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarMark */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => "{$model->name}[{$model->id_car_mark}]",
]);
?>
<div class="car-mark-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
