<?php

use yii\helpers\Html;
use common\assets\AjaxFormAsset;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel modules\auto\models\search\CarTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cars', 'Car Types');
$this->params['breadcrumbs'][] = $this->title;

AjaxFormAsset::register($this);

?>
<div class="car-type-index">
    <div class="row">
    <h1><?= Html::encode($this->title) ?></h1>
        <div class="col-md-6">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('cars', 'Create Car Type'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id_car_type',
            'name',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{ajax-update} {delete}',
                'options' => ['style' => 'width:70px']
            ],
        ],
    ]); ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
