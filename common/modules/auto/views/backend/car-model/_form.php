<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-model-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-1">Основные данные</a>
            </li>
            <?php if (!$model->isNewRecord) { ?>
                <li class="">
                    <a data-toggle="tab" href="#tab-2">Фотографии</a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#tab-3">Видео</a>
                </li>
            <?php } ?>
        </ul>
        <div class="tab-content ">
            <div id="tab-1" class="tab-pane active">
                <div class="panel-body">
                    <?= $form->field($model, 'id_car_mark')->textInput() ?>

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'name_rus')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'active')->checkbox() ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' =>'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
            <?php if (!$model->isNewRecord) { ?>
                <div id="tab-2" class="tab-pane">
                    <div class="panel-body">
                        <?php foreach ($model->photoSmall as $photo) { ?>
                            <?= Html::img($photo->image, ['class' => 'img-thumbnail']); ?>
                        <?php } ?>
                    </div>
                </div>
                <div id="tab-3" class="tab-pane">
                    <div class="panel-body">
                        <?php foreach ($model->videos as $video) { ?>
                            <?= $video->getYoutubeFrame(320, 240); ?>
                            <br />
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
