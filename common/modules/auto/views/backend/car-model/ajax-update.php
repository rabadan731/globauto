<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarModel */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => "{$model->name}[{$model->id_car_model}]",
]);
?>
<div class="car-model-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
