<?php

use yii\helpers\Html;
use common\assets\AjaxFormAsset;
use yii\grid\GridView;
use modules\auto\models\CarMark;


/* @var $this yii\web\View */
/* @var $searchModel modules\auto\models\search\CarModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cars', 'Car Models');
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = $this->title;

AjaxFormAsset::register($this);

?>
<div class="car-model-index">
    <div class="row">
        <div class="col-md-6">
            <p>
                <?= Html::a(
                    Yii::t('app', 'Add'),
                    ['create'],
                    ['class' => 'btn btn-success']
                ) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id_car_model',
                        'options' => ['style' => 'width:70px']
                    ],
                    [
                        'attribute' => 'id_car_mark',
                        'value' => 'carMark.name',
                        'filter' => CarMark::listDropdown()
                    ],
                    'name',
                    'name_rus',
                    'slug',
                    'active:boolean',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{ajax-update} {delete}',
                        'options' => ['style' => 'width:70px']
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
