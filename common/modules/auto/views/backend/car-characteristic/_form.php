<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\auto\models\CarCharacteristic;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarCharacteristic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-characteristic-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_parent')->dropDownList(
        CarCharacteristic::listDropdownParent(),
        ['prompt'=>'-Корневая категория-']
    ); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('cars', 'Create') : Yii::t('cars', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
