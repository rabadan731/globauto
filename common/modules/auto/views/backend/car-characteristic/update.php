<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarCharacteristic */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => "{$model->name}[{$model->id_car_characteristic}]",
]);
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Characteristics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_car_characteristic]];
$this->params['breadcrumbs'][] = Yii::t('cars', 'Update');
?>
<div class="car-characteristic-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
