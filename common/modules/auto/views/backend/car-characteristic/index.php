<?php

use yii\helpers\Html;
use common\assets\AjaxFormAsset;
use yii\grid\GridView;
use modules\auto\models\CarCharacteristic;

/* @var $this yii\web\View */
/* @var $searchModel modules\auto\models\search\CarCharacteristicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cars', 'Car Characteristics');
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = $this->title;

AjaxFormAsset::register($this);

?>
<div class="car-characteristic-index">
    <div class="row">

        <div class="col-md-6">
            <p>
                <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id_car_characteristic',
                        'options' => ['style' => 'width:70px']
                    ],
                    'name',
                    [
                        'attribute' => 'id_parent',
                        'value' => 'parent.name',
                        'filter' => CarCharacteristic::listDropdownParent()
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{ajax-update} {delete}',
                        'options' => ['style' => 'width:70px']
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
