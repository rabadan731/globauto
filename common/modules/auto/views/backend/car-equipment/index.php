<?php

use yii\helpers\Html;
use common\assets\AjaxFormAsset;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use modules\auto\models\CarMark;
use modules\auto\models\CarModel;
use modules\auto\models\CarGeneration;
use modules\auto\models\CarModification;

/* @var $this yii\web\View */
/* @var $searchModel modules\auto\models\search\CarEquipmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cars', 'Car Equipments');
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = $this->title;

AjaxFormAsset::register($this);

$findModification = null;
$model_id = false;
$mark_id = false;

$models = [];

$query = $dataProvider->query;
$findMarks = CarMark::find()->select('car_mark.id_car_mark')->distinct();
$findModel = CarModification::find()->select('car_modification.id_car_model')->distinct();

$findMarks->joinWith('carModels');
$findMarks->andWhere(['in', 'car_model.id_car_model', ArrayHelper::getColumn($findModel->asArray()->all(), 'id_car_model')]);

$marks = ArrayHelper::getColumn($findMarks->asArray()->all(), 'id_car_mark');

if (isset($query->where['car_mark.id_car_mark'])) {
    $mark_id = $query->where['car_mark.id_car_mark'];
    $findModel->markId($mark_id);
}

if (isset($query->where['car_model.id_car_model'])) {
    $model_id = $query->where['car_model.id_car_model'];
}

$models = ArrayHelper::getColumn($findModel->asArray()->all(), 'id_car_model');

if ($model_id) {
    $findModification = CarModification::listDropdown('id_car_modification', 'name', $model_id);
}


?>
<div class="car-equipment-index">
    <div class="row">
        <div class="col-md-6">
            <p>
                <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id_car_equipment',
                        'options' => ['style' => 'width:70px']
                    ],
                    [
                        'attribute' => 'id_car_mark',
                        'value' => 'carModification.carModel.carMark.name',
                        'filter' => CarMark::listDropdown('id_car_mark', 'name')
                    ],
                    [
                        'attribute' => 'id_car_model',
                        'value' => 'carModification.carModel.name',
                        'filter' => CarModel::listDropdown('id_car_model', 'name', $mark_id, $models)
                    ],
                    [
                        'attribute' => 'id_car_modification',
                        'value' => 'carModification.name',
                        'filter' => $model_id ? $findModification : '--Выбирете модель--',
                    ],
                    'name',
                    'price_min',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{ajax-update} {delete}',
                        'options' => ['style' => 'width:70px']
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
