<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\auto\models\CarModification;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarEquipment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-equipment-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_car_modification')->dropDownList(CarModification::listDropdown(
        'id_car_modification',
        'name',
        $model->id_car_model
    )) ?>

    <?= $form->field($model, 'price_min')->textInput() ?>

    <?= $form->field($model, 'year')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
