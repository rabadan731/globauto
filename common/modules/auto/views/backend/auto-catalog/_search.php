<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\search\AutoCatalogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auto-catalog-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'auto_catalog_id') ?>

    <?= $form->field($model, 'slug') ?>

    <?= $form->field($model, 'country_id') ?>

    <?= $form->field($model, 'type_body_id') ?>

    <?= $form->field($model, 'engine_id') ?>

    <?php // echo $form->field($model, 'gear_id') ?>

    <?php // echo $form->field($model, 'transmission_id') ?>

    <?php // echo $form->field($model, 'color') ?>

    <?php // echo $form->field($model, 'price_start') ?>

    <?php // echo $form->field($model, 'price_end') ?>

    <?php // echo $form->field($model, 'run_start') ?>

    <?php // echo $form->field($model, 'run_end') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('cars', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('cars', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
