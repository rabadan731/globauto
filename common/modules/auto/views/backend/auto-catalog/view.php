<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\AutoCatalog */

$this->title = $model->auto_catalog_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Auto Catalogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auto-catalog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('cars', 'Update'), ['update', 'id' => $model->auto_catalog_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('cars', 'Delete'), ['delete', 'id' => $model->auto_catalog_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('cars', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'auto_catalog_id',
            'slug',
            'country_id',
            'type_body_id',
            'engine_id',
            'gear_id',
            'transmission_id',
            'color',
            'price_start',
            'price_end',
            'run_start',
            'run_end',
        ],
    ]) ?>

</div>
