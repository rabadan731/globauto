<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\auto\models\AutoCatalog */

$this->title = Yii::t('cars', 'Create Auto Catalog');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Auto Catalogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auto-catalog-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
