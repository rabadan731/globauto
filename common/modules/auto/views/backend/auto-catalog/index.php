<?php

use yii\helpers\Html;
use common\assets\AjaxFormAsset;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use modules\auto\models\AutoCatalog;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel modules\auto\models\search\AutoCatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cars', 'Auto Catalogs');
$this->params['breadcrumbs'][] = $this->title;

AjaxFormAsset::register($this);

?>
<div class="auto-catalog-index">
    <div class="row">
        <div class="col-md-6">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php $form = ActiveForm::begin(['action' => Url::to(['create'])]); ?>

        <div class="row">
            <div class="col-md-9">
                <?= $form->field(new AutoCatalog(), 'slug')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <label>&nbsp;</label>
                <div class="form-group">
                    <?= Html::submitButton(
                        Yii::t('app', 'Add'),
                        ['class' => 'btn btn-primary']
                    ) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'auto_catalog_id',
            'slug',
            'country_id',
            'type_body_id',
            'engine_id',
            // 'gear_id',
            // 'transmission_id',
            // 'color',
            // 'price_start',
            // 'price_end',
            // 'run_start',
            // 'run_end',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{ajax-update} {delete}',
                'options' => ['style' => 'width:70px']
            ],
        ],
    ]); ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
