<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\AutoCatalog */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => 'Auto Catalog',
]) . $model->auto_catalog_id;
?>
<div class="auto-catalog-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
