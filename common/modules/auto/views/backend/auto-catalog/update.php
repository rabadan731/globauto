<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\AutoCatalog */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => 'Auto Catalog',
]) . $model->auto_catalog_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Auto Catalogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->auto_catalog_id, 'url' => ['view', 'id' => $model->auto_catalog_id]];
$this->params['breadcrumbs'][] = Yii::t('cars', 'Update');
?>
<div class="auto-catalog-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
