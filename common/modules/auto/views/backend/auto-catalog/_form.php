<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\auto\models\CarCharacteristicValue;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\AutoCatalog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auto-catalog-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country_id')->textInput() ?>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'type_body_id')->dropDownList(
                CarCharacteristicValue::findValuesInParams(2), ['prompt' => '-- select --']
            ) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'engine_id')->dropDownList(
                CarCharacteristicValue::findValuesInParams(12), ['prompt' => '-- select --']
            ) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'gear_id')->dropDownList(
                CarCharacteristicValue::findValuesInParams(27), ['prompt' => '-- select --']
            ) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'transmission_id')->dropDownList(
                CarCharacteristicValue::findValuesInParams(24), ['prompt' => '-- select --']
            ) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'price_start')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'price_end')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'run_start')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'run_end')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
