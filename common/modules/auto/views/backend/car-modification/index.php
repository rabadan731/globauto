<?php

use yii\helpers\Html;
use common\assets\AjaxFormAsset;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use modules\auto\models\CarMark;
use modules\auto\models\CarModel;
use modules\auto\models\CarSerie;
use modules\auto\models\CarModification;
use modules\auto\models\CarGeneration;

/* @var $this yii\web\View */
/* @var $searchModel modules\auto\models\search\CarModificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cars', 'Car Modifications');
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = $this->title;

AjaxFormAsset::register($this);

$model_id = false;
$mark_id = false;
$id_car_generation = false;

$models = [];

$query = $dataProvider->query;
$findMarks = CarMark::find()->select('car_mark.id_car_mark')->distinct();
$findModel = CarModification::find()->select('car_modification.id_car_model')->distinct();

$findMarks->joinWith('carModels');
$findMarks->andWhere(['in', 'car_model.id_car_model', ArrayHelper::getColumn($findModel->asArray()->all(), 'id_car_model')]);

$marks = ArrayHelper::getColumn($findMarks->asArray()->all(), 'id_car_mark');

if (isset($query->where['car_mark.id_car_mark'])) {
    $mark_id = $query->where['car_mark.id_car_mark'];
    $findModel->markId($mark_id);
}

if (isset($query->where['car_modification.id_car_model'])) {
    $model_id = $query->where['car_modification.id_car_model'];
}
if (isset($query->where['car_generation.id_car_generation'])) {
    $id_car_generation = $query->where['car_generation.id_car_generation'];
}

$models = ArrayHelper::getColumn($findModel->asArray()->all(), 'id_car_model');


?>
<div class="car-modification-index">
    <div class="row">

        <div class="col-md-6">
            <p>
                <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id_car_mark',
                        'value' => 'carModel.carMark.name',
                        'filter' => CarMark::listDropdown('id_car_mark', 'name', $marks)
                    ],
                    [
                        'attribute' => 'id_car_model',
                        'value' => 'carModel.name',
                        'filter' => CarModel::listDropdown('id_car_model', 'name', $mark_id, $models)
                    ],
                    [
                        'attribute' => 'id_car_generation',
                        'value' => 'carSerie.carGeneration.name',
                        'filter' => $model_id ? CarGeneration::listDropdown(
                            'id_car_generation',
                            'name',
                            $model_id
                        ) : "--выбирете модель--"
                    ],
                    [
                        'attribute' => 'id_car_serie',
                        'value' => 'carSerie.name',
                        'filter' => $model_id ? CarSerie::listDropdown(
                            'id_car_serie',
                            'name',
                            $model_id,
                            $id_car_generation
                        ) : "--выбирете модель--"
                    ],
                    'name',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{ajax-update} {delete}',
                        'options' => ['style' => 'width:70px']
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
