<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\auto\models\CarModel;
use modules\auto\models\CarSerie;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarModification */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-modification-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <?= $form->field($model, 'id_car_model')
        ->dropDownList(CarModel::listGroupDropdown(), ['prompt' => '-Выберите модель-']) ?>

    <?= $form->field($model, 'id_car_serie')->dropDownList(CarSerie::listDropdown(
        'id_car_serie',
        'name',
        $model->id_car_model
    ), ['prompt' => '-Выберите серию-']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
