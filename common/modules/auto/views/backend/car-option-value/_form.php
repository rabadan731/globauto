<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\auto\models\CarOption;
use modules\auto\models\CarEquipment;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarOptionValue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-option-value-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <?= $form->field($model, 'id_car_option')->dropDownList(CarOption::listDropdown($model->carOption->id_parent)) ?>

    <?= $form->field($model, 'id_car_equipment')->dropDownList(CarEquipment::listDropdown(
        'id_car_equipment', 'name', [
            'id_car_modification' => $model->carEquipment->id_car_modification,
        ]
    )) ?>

    <?= $form->field($model, 'is_base')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('cars', 'Create') : Yii::t('cars', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
