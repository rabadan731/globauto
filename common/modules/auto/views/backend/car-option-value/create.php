<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarOptionValue */

$this->title = Yii::t('cars', 'Add: {model}', [
    'model' => Yii::t('cars', 'Car Option Values'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Option Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-option-value-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
