<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarOptionValue */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => "{$model->id_car_option_value}",
]);
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Option Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_car_option_value, 'url' => ['view', 'id' => $model->id_car_option_value]];
$this->params['breadcrumbs'][] = Yii::t('cars', 'Update');
?>
<div class="car-option-value-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
