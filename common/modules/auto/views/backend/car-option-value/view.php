<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarOptionValue */

$this->title = $model->id_car_option_value;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Option Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-option-value-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('cars', 'Update'), ['update', 'id' => $model->id_car_option_value], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('cars', 'Delete'), ['delete', 'id' => $model->id_car_option_value], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('cars', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_car_option_value',
            'is_base',
            'id_car_option',
            'id_car_equipment',
            'date_create',
            'date_update',
            'id_car_type',
        ],
    ]) ?>

</div>
