<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarOptionValue */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => "{$model->id_car_option_value}",
]);
?>
<div class="car-option-value-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
