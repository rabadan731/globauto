<?php

use yii\helpers\Html;
use common\assets\AjaxFormAsset;
use yii\grid\GridView;
use modules\auto\models\CarOption;
use yii\helpers\ArrayHelper;
use modules\auto\models\CarMark;
use modules\auto\models\CarModel;
use modules\auto\models\CarOptionValue;
use modules\auto\models\CarModification;
/* @var $this yii\web\View */
/* @var $searchModel modules\auto\models\search\CarOptionValueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cars', 'Car Option Values');
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = $this->title;

AjaxFormAsset::register($this);
$query = $dataProvider->query;


$findModification = null;
$model_id = false;
$mark_id = false;

$models = [];

$query = $dataProvider->query;
$findMarks = CarMark::find()->select('car_mark.id_car_mark')->distinct();
$findModel = CarModification::find()->select('car_modification.id_car_model')->distinct();

$findMarks->joinWith('carModels');
$findMarks->andWhere(['in','car_model.id_car_model',ArrayHelper::getColumn($findModel->asArray()->all(),'id_car_model')]);

$marks = ArrayHelper::getColumn($findMarks->asArray()->all(),'id_car_mark');

if (isset($query->where['car_mark.id_car_mark'])) {
    $mark_id = $query->where['car_mark.id_car_mark'];
    $findModel->markId($mark_id);
}

if (isset($query->where['car_model.id_car_model'])) {
    $model_id = $query->where['car_model.id_car_model'];
}

$models = ArrayHelper::getColumn($findModel->asArray()->all(),'id_car_model');

if ($model_id) {
    $findModification = CarModification::listDropdown('id_car_modification','name',$model_id);
}

$id_car_option = false;
if (isset($query->where['car_option.id_parent'])) {
    $id_car_option = $query->where['car_option.id_parent'];
}

if ($id_car_option) {

    $findCarOptionValue = clone $query;
    $where = $findCarOptionValue->where;
    unset($where['car_option_value.id_car_option']);
    $findCarOptionValue->where($where);

    $findCarOptionValue->select('car_option_value.id_car_option');

    $optionValueIds = ArrayHelper::getColumn($findCarOptionValue->asArray()->all(),'id_car_option');

    $carOptionList = CarOption::listDropdown($id_car_option, $optionValueIds);
}

?>
<div class="car-option-value-index">
    <div class="row">

        <div class="col-md-8">
            <p>
                <?= Html::a(Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute'=>'id_car_mark',
                        'value'=>'carModification.carModel.carMark.name',
                        'filter' => CarMark::listDropdown('id_car_mark','name')
                    ],
                    [
                        'attribute'=>'id_car_model',
                        'value'=>'carModification.carModel.name',
                        'filter' => CarModel::listDropdown('id_car_model','name',$mark_id,$models)
                    ],
                    [
                        'attribute'=>'id_car_modification',
                        'value'=>'carModification.name',
                        'filter' => $model_id?$findModification:'--Выбирете модель--',
                    ],

                    [
                        'attribute' => 'id_car_equipment',
                        'value' => 'carEquipment.name',
                        'filter' => $id_car_modification?$carEquipmentList:'-select modification-'
                    ],
                    [
                        'attribute' => 'id_car_option_parent',
                        'value' => 'carOption.parent.name',
                        'filter' => CarOption::listDropdownParent(),
                    ],
                    [
                        'attribute' => 'id_car_option',
                        'value' => 'carOption.name',
                        'filter' => $id_car_option ? $carOptionList : '-Выберите родит.кат.-',
                    ],
                    [
                        'attribute' => 'is_base',
                    ],

                    //'date_create',
                    // 'date_update',
                    // 'id_car_type',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{ajax-update} {delete}',
                        'options' => ['style' => 'width:70px']
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-4">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
