<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\search\CarSerieSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-serie-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_car_serie') ?>

    <?= $form->field($model, 'id_car_model') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'date_create') ?>

    <?= $form->field($model, 'date_update') ?>

    <?php // echo $form->field($model, 'id_car_generation') ?>

    <?php // echo $form->field($model, 'id_car_type') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('cars', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('cars', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
