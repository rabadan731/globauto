<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarSerie */

$this->title = Yii::t('cars', 'Add: {model}', [
    'model' => Yii::t('cars', 'Car Series'),
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Series'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-serie-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
