<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarSerie */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => "{$model->name}[{$model->id_car_serie}]",
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Series'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_car_serie]];
$this->params['breadcrumbs'][] = Yii::t('cars', 'Update');
?>
<div class="car-serie-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
