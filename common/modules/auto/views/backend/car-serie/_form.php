<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\auto\models\CarModel;
use modules\auto\models\CarGeneration;

/* @var $this yii\web\View */
/* @var $model modules\auto\models\CarSerie */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-serie-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <?= $form->field($model, 'id_car_model')
        ->dropDownList(CarModel::listGroupDropdown(), ['prompt' => '-Выберите модель-']) ?>

    <?= $form->field($model, 'id_car_generation')->dropDownList(
        CarGeneration::listDropdown('id_car_generation','name', $model->id_car_model), ['prompt' => '-Выберите модель-']
    ) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
