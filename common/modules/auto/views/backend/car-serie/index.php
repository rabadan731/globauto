<?php

use yii\helpers\Html;
use common\assets\AjaxFormAsset;
use yii\grid\GridView;
use modules\auto\models\CarMark;
use modules\auto\models\CarModel;
use modules\auto\models\CarGeneration;
use modules\auto\models\CarSerie;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $countNoSlug integer */
/* @var $searchModel modules\auto\models\search\CarSerieSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$model_id = false;
$mark_id = false;

$models = [];

$query = $dataProvider->query;
$findMarks = CarMark::find()->select('car_mark.id_car_mark')->distinct();
$findModel = CarSerie::find()->select('car_serie.id_car_model')->distinct();

$findMarks->joinWith('carModels');
$findMarks->andWhere(['in', 'car_model.id_car_model', ArrayHelper::getColumn($findModel->asArray()->all(), 'id_car_model')]);

$marks = ArrayHelper::getColumn($findMarks->asArray()->all(), 'id_car_mark');

if (isset($query->where['car_mark.id_car_mark'])) {
    $mark_id = $query->where['car_mark.id_car_mark'];
    $findModel->markId($mark_id);
}

if (isset($query->where['car_serie.id_car_model'])) {
    $model_id = $query->where['car_serie.id_car_model'];
}

$models = ArrayHelper::getColumn($findModel->asArray()->all(), 'id_car_model');


$this->title = Yii::t('cars', 'Car Series');
$this->params['breadcrumbs'][] = Yii::t('cars', 'Cars');
$this->params['breadcrumbs'][] = $this->title;


AjaxFormAsset::register($this);

?>
<div class="car-serie-index">
    <div class="row">
        <div class="col-md-6">
            <p>
                <?= Html::a(Yii::t('App', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
                <?php if ($countNoSlug) { ?>
                    <?= Html::a("Создать новые {$countNoSlug} - slug", ['create-slug'], ['class' => 'btn btn-info']) ?>
                <?php } ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id_car_mark',
                        'value' => 'carModel.carMark.name',
                        'filter' => CarMark::listDropdown('id_car_mark', 'name', $marks)
                    ],
                    [
                        'attribute' => 'id_car_model',
                        'value' => 'carModel.name',
                        'filter' => CarModel::listDropdown('id_car_model', 'name', $mark_id, $models)
                    ],
                    [
                        'attribute' => 'id_car_generation',
                        'value' => 'carGeneration.name',
                        'filter' => $model_id ? CarGeneration::listDropdown('id_car_generation', 'name', $model_id) : '--выбирете модель--'
                    ],
                    'name',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{ajax-update} {delete}',
                        'options' => ['style' => 'width:70px']
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>
