<?php
namespace modules\users\models\forms;

use Yii;
use yii\base\Model;
use yii\rbac\Item;

class AssignmentForm extends Model
{
    /** @var   */
    public $model;

    /** @var   */
    public $assigned;

    /** @var   */
    public $unassigned;

    /** @var   */
    public $target;

    /** @var   */
    public $action;

    public function rules()
    {
        return [
            [['assigned', 'unassigned', 'action'], 'safe'],
            [['action'], 'required'],
            [['unassigned'], 'noEmpty', 'skipOnEmpty' => false, 'params' => ['action' => 'assign']],
            [['assigned'], 'noEmpty', 'skipOnEmpty' => false, 'params' => ['action' => 'revoke']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'assigned' => Yii::t('users', 'RBAC_ASSIGNED'),
            'unassigned' => Yii::t('users', 'RBAC_UNASSIGNED'),
        ];
    }

    /**
     * @param $attribute
     * @param $parameters
     */
    public function noEmpty($attribute, $parameters)
    {
        if ($this->action == $parameters['action']) {
            $items = $this->{$attribute};
            if (!is_array($items) || !count($items)) {
                $this->addError($attribute, Yii::t('users', 'CHOOSE_ITEMS'));
            }
        }
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        if ($this->target) {
            if ($this->action == 'assign') {
                return $this->saveChildren($this->unassigned, 'addChild');
            } else {
                return $this->saveChildren($this->assigned, 'removeChild');
            }
        } else {
            if ($this->action == 'assign') {
                return $this->saveAssignments($this->unassigned, 'assign');
            } else {
                return $this->saveAssignments($this->assigned, 'revoke');
            }
        }
    }

    /**
     * @param $items
     * @param $method
     * @return bool
     */
    protected function saveChildren($items, $method)
    {
        $allSuccess = true;
        $auth = Yii::$app->authManager;

        foreach ($items as $item) {
            $allSuccess = $allSuccess && $auth->$method($this->target, $this->getItem($item));
        }

        return $allSuccess;
    }

    /**
     * @param $items
     * @param $method
     * @return bool
     */
    protected function saveAssignments($items, $method)
    {
        $allSuccess = true;
        $auth = Yii::$app->authManager;

        foreach ($items as $item) {
            $allSuccess = $allSuccess && $auth->$method($this->getItem($item), $this->model->id);
        }

        return $allSuccess;
    }

    /**
     * @param $serialize
     * @return null|\yii\rbac\Permission|\yii\rbac\Role
     */
    protected function getItem($serialize)
    {
        $item = unserialize($serialize);
        $auth = Yii::$app->authManager;
        return ($item[1] == Item::TYPE_ROLE) ? $auth->getRole($item[0]) : $auth->getPermission($item[0]);
    }
}
