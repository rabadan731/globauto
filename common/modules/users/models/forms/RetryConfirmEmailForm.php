<?php
namespace modules\users\models\forms;

use yii\base\Model;
use modules\users\models\User;
use Yii;

/**
 * Class RetryConfirmEmailForm
 * @package modules\users\models\forms
 */
class RetryConfirmEmailForm extends Model
{
    public $email;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\modules\users\models\UserEmailConfirmToken',
                'targetAttribute' => 'new_email',
                'filter' => ['old_email' => ''],
                'message' => Yii::t('users', 'USER_WITH_SUCH_EMAIL_DO_NOT_EXISTS')
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('users', 'EMAIL'),
        ];
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return User::findOne(['email' => $this->email]);
    }
}