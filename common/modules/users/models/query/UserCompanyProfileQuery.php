<?php

namespace modules\users\models\query;

/**
 * This is the ActiveQuery class for [[\modules\users\models\UserCompanyProfile]].
 *
 * @see \modules\users\models\UserCompanyProfile
 */
class UserCompanyProfileQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \modules\users\models\UserCompanyProfile[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \modules\users\models\UserCompanyProfile|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
