<?php

namespace modules\users\models;

use Yii;
use modules\users\models\query\UserCompanyProfileQuery;

/**
 * This is the model class for table "user_company_profile".
 *
 * @property integer $user_id
 * @property string $title
 * @property string $text
 * @property integer $cityID
 * @property string $address
 * @property string $service
 * @property string $category
 * @property string $open_hours
 * @property string $website
 * @property string $email
 * @property string $phone
 * @property integer $updated_at
 */
class UserCompanyProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_company_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'cityID', 'updated_at'], 'integer'],
            [['title', 'text', 'address'], 'string'],
            [['service', 'category', 'open_hours', 'website', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('user', 'User ID'),
            'title' => Yii::t('user', 'Title company'),
            'text' => Yii::t('user', 'Text'),
            'cityID' => Yii::t('user', 'City'),
            'address' => Yii::t('user', 'Address'),
            'service' => Yii::t('user', 'Service'),
            'category' => Yii::t('user', 'Category'),
            'open_hours' => Yii::t('user', 'Open Hours'),
            'website' => Yii::t('user', 'Website'),
            'email' => Yii::t('user', 'Email'),
            'phone' => Yii::t('user', 'Phone'),
            'updated_at' => Yii::t('user', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserCompanyProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserCompanyProfileQuery(get_called_class());
    }
}
