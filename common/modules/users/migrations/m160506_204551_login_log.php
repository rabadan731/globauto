<?php

use yii\db\Migration;

/**
 * Handles the creation for table `users_login`.
 */
class m160506_204551_login_log extends Migration
{
    /**
     * @var string
     */
    public $table = '{{%users_login}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'ip' => $this->string(25),
            'ua' => $this->string(200),
            'created_at' => $this->integer(),
        ]);

        $this->createIndex('user', $this->table, ['user_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
