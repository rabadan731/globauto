<?php

use yii\db\Migration;
use yii\rbac\Item;
use modules\users\Module;
use modules\users\models\User;

class m130524_201442_user_init extends Migration
{
    public function up()
    {
        Module::registerTranslations();

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //таблица user
        $this->createTable('{{%user}}', [
            'id'            => $this->primaryKey(),
            'username'      => $this->string(255)->notNull(),
            'auth_key'      => $this->string(32)->notNull(),
            'password_hash' => $this->string(255)->notNull(),
            'email'         => $this->string(255),
            'photo'         => $this->string(255),
            'phone'         => $this->string(255),
            'locale'        => $this->string(32)->notNull()->defaultValue(Yii::$app->language),
            'balance'       => $this->decimal(12,2)->notNull()->defaultValue(0.00),
            'sex'           => $this->smallInteger()->notNull()->defaultValue(User::SEX_MALE),
            'status'        => $this->smallInteger()->notNull()->defaultValue(10),
            'cityID'        => $this->integer(),
            'type'          => $this->integer()->defaultValue(0),
            'created_at'    => $this->integer(),
            'updated_at'    => $this->integer(),
        ], $tableOptions);

        //таблица auth_rule
        $this->createTable('{{%user_company_profile}}', [
            'user_id'    => $this->primaryKey(),
            'title'      => $this->text(),
            'text'       => $this->text(),
            'cityID'     => $this->integer(),
            'address'    => $this->text(),
            'service'    => $this->string(255),
            'category'   => $this->string(255),
            'open_hours' => $this->string(255),
            'website'    => $this->string(255),
            'email'      => $this->string(255),
            'phone'      => $this->string(255),
            'updated_at' => $this->integer(),
        ], $tableOptions);



        //таблица user_password_reset_token
        $this->createTable('{{%user_password_reset_token}}', [
            'id'            => $this->primaryKey(),
            'user_id'       => $this->integer()->notNull(),
            'token'         => $this->string(255),
        ], $tableOptions);

        //
        $this->addForeignKey(
            'user_password_reset_token_user_id_fk',
            '{{%user_password_reset_token}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        //таблица user_change_email_token
        $this->createTable('{{%user_email_confirm_token}}', [
            'id'                => $this->primaryKey(),
            'user_id'           => $this->integer()->notNull(),
            'old_email'         => $this->string(255),
            'old_email_token'   => $this->string(255),
            'old_email_confirm' => $this->smallInteger()->defaultValue(0),
            'new_email'         => $this->string(255)->notNull(),
            'new_email_token'   => $this->string(255)->notNull(),
            'new_email_confirm' => $this->smallInteger()->defaultValue(0)
        ], $tableOptions);

        $this->addForeignKey(
            'user_email_confirm_token_user_id_fk',
            '{{%user_email_confirm_token}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        //таблица user_oauth_key
        $this->createTable('{{%user_oauth_key}}', [
            'id'                => $this->primaryKey(),
            'user_id'           => $this->integer(),
            'provider_id'       => $this->integer(),
            'provider_user_id'  => $this->string(255)
        ], $tableOptions);

        $this->addForeignKey(
            'user_oauth_key_user_id_fk',
            '{{%user_oauth_key}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        //таблица auth_rule
        $this->createTable('{{%auth_rule}}', [
            'name'       => $this->string(64)->notNull(),
            'data'       => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),

        ], $tableOptions);

        $this->addPrimaryKey('auth_rule_pk', '{{%auth_rule}}', 'name');

        //таблица auth_item
        $this->createTable('{{%auth_item}}', [
            'name'          => $this->string(64)->notNull(),
            'type'          => $this->integer()->notNull(),
            'description'   => $this->text(),
            'rule_name'     => $this->string(64),
            'data'          => $this->text(),
            'created_at'    => $this->integer(),
            'updated_at'    => $this->integer()
        ], $tableOptions);
        $this->createIndex('auth_item_type_index', '{{%auth_item}}', 'type');
        $this->addPrimaryKey('auth_item_name_pk', '{{%auth_item}}', 'name');
        $this->addForeignKey(
            'auth_item_rule_name_fk',
            '{{%auth_item}}',
            'rule_name',
            '{{%auth_rule}}',
            'name',
            'SET NULL',
            'CASCADE'
        );

        //таблица auth_item_child
        $this->createTable('{{%auth_item_child}}', [
            'parent' => $this->string(64)->notNull(),
            'child'  => $this->string(64)->notNull(),
        ], $tableOptions);
        $this->addPrimaryKey('auth_item_child_pk', '{{%auth_item_child}}', array('parent', 'child'));
        $this->addForeignKey(
            'auth_item_child_parent_fk',
            '{{%auth_item_child}}',
            'parent',
            '{{%auth_item}}',
            'name',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'auth_item_child_child_fk',
            '{{%auth_item_child}}',
            'child',
            '{{%auth_item}}',
            'name',
            'CASCADE',
            'CASCADE'
        );

        //таблица auth_assignment
        $this->createTable('{{%auth_assignment}}', [
            'item_name'  => $this->string(64)->notNull(),
            'user_id'    => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);
        $this->addPrimaryKey('auth_assignment_pk', '{{%auth_assignment}}', array('item_name', 'user_id'));
        $this->addForeignKey(
            'auth_assignment_item_name_fk',
            '{{%auth_assignment}}',
            'item_name',
            '{{%auth_item}}',
            'name',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'auth_assignment_user_id_fk',
            '{{%auth_assignment}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $time = time();

        //аккаунт для администратора и права
        $this->batchInsert('{{%user}}', [
            'username',
            'auth_key',
            'password_hash',
            'email',
            'status',
            'cityID',
            'phone',
            'created_at',
            'updated_at'
        ], [
                [
                    Yii::t('users', 'MIGRATION_ADMINISTRATOR'),
                    Yii::$app->security->generateRandomString(),
                    Yii::$app->security->generatePasswordHash('rabadan731'),
                    'rabadan731@yandex.ru',
                    User::STATUS_ACTIVE,
                    171146,
                    '+7 988 212 62 62',
                    $time,
                    $time
                ],
                [
                    Yii::t('users', 'MIGRATION_MODERATOR'),
                    Yii::$app->security->generateRandomString(),
                    Yii::$app->security->generatePasswordHash('moderator@example.com'),
                    'moderator@example.com',
                    User::STATUS_ACTIVE,
                    171146,
                    '+7 988 212 62 62',
                    $time,
                    $time
                ],
                [
                    Yii::t('users', 'USER'),
                    Yii::$app->security->generateRandomString(),
                    Yii::$app->security->generatePasswordHash('user@example.com'),
                    'user@example.com',
                    User::STATUS_ACTIVE,
                    171146,
                    '+7 988 212 62 62',
                    $time,
                    $time
                ]
        ]);
        $this->insert('{{%auth_rule}}', [
            'name' => 'noElderRank',
            'data' => 'O:34:"modules\users\rbac\NoElderRankRule":3:{s:4:"name";s:11:"noElderRank";s:9:"createdAt";i:1503505338;s:9:"updatedAt";i:1503505338;}',
            'created_at' => $time,
            'updated_at' => $time,
        ]);
        $this->batchInsert('{{%auth_item}}', ['name', 'type', 'description', 'rule_name', 'created_at', 'updated_at'], [
            ['administrator', Item::TYPE_ROLE, Yii::t('users', 'MIGRATION_ADMINISTRATOR'), NULL, $time, $time],
            ['moderator', Item::TYPE_ROLE, Yii::t('users', 'MIGRATION_MODERATOR'), NULL, $time, $time],
            ['user', Item::TYPE_ROLE, Yii::t('users', 'USER'), NULL, $time, $time],

            ['backend', Item::TYPE_PERMISSION, Yii::t('users', 'BACKEND_ACCESS'), NULL, $time, $time],
            ['rbacManage', Item::TYPE_PERMISSION, Yii::t('users', 'MIGRATION_RBAC_MANAGE'), NULL, $time, $time],
            ['userManage', Item::TYPE_PERMISSION, Yii::t('users', 'MIGRATION_USER_MANAGE'), NULL, $time, $time],
            ['userPermissions', Item::TYPE_PERMISSION, Yii::t('users', 'MIGRATION_USER_PERMISSIONS'), NULL, $time, $time],
            ['userUpdateNoElderRank', Item::TYPE_PERMISSION, Yii::t('users', 'MIGRATION_USER_UPDATE_NO_ELDER_RANK'), 'noElderRank', $time, $time],
        ]);

        $this->batchInsert('{{%auth_item_child}}', ['parent', 'child'], [
            ['administrator', 'rbacManage'],
            ['administrator', 'backend'],
            ['administrator', 'userPermissions'],
            ['administrator', 'userManage'],
            ['administrator', 'moderator'],
            ['administrator', 'user'],
            ['moderator', 'userManage'],
            ['moderator', 'userUpdateNoElderRank'],
        ]);
        $this->batchInsert('{{%auth_assignment}}', ['item_name', 'user_id', 'created_at', 'updated_at'], [
            ['administrator', 1, $time, $time],
            ['moderator', 2, $time, $time],
            ['user', 3, $time, $time],
        ]);



    }

    public function down()
    {
        $this->dropTable('{{%auth_assignment}}');
        $this->dropTable('{{%auth_item_child}}');
        $this->dropTable('{{%auth_item}}');
        $this->dropTable('{{%auth_rule}}');
        $this->dropTable('{{%user_oauth_key}}');
        $this->dropTable('{{%user_email_confirm_token}}');
        $this->dropTable('{{%user_password_reset_token}}');
        $this->dropTable('{{%user_company_profile}}');
        $this->dropTable('{{%user}}');
    }
}
