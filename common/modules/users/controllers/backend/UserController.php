<?php

namespace modules\users\controllers\backend;

use modules\users\models\AuthItem;
use modules\users\models\forms\LoginForm;
use modules\users\models\forms\SignupForm;
use Yii;
use modules\users\models\User;
use yii\data\ActiveDataProvider;
use yii\rbac\Item;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use modules\users\models\forms\AssignmentForm;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * AdminController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    private $_model = false;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['userManage'],
                    ],
                ],
            ],
        ];
    }
    
    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            if ($user = $model->signup()) {
                $transaction->commit();
                return $this->redirect(['view', 'id' => $user->id]);
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('users', 'CAN_NOT_CREATE_NEW_USER'));
                $transaction->rollBack();
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionAjaxUpdate($id)
    {
        if ($id == 0) {
            $model = new User();
        } else {
            $model = $this->findModel($id);
        }

        if (Yii::$app->request->isAjax) {

            $post = Yii::$app->request->post("User", null);

            if (is_null($post)) {
                return $this->renderAjax('ajax-update', [
                    'model' => $model,
                ]);
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($id == 0) {
                $model->generateAuthKey();
            }

            $password = Yii::$app->request->post('password', null);

            if (!empty($password)) {
                $model->setPassword($password);
            }

            $status = ($model->load(Yii::$app->request->post()) && $model->save());

            if (Yii::$app->user->can("userPermissions")) {
                Yii::$app->authManager->revokeAll($model->id);
                foreach (Yii::$app->request->post('child_role', []) as $roleName => $val) {
                    Yii::$app->authManager->assign($this->findAuthEntity($roleName, AuthItem::TYPE_ROLE), $model->id);
                }
                foreach (Yii::$app->request->post('child_rule', []) as $roleName => $val) {
                    Yii::$app->authManager->assign($this->findAuthEntity($roleName, AuthItem::TYPE_RULES), $model->id);
                }
            }

            $res = array(
                'success' => $status,
                'statusText' => $status?Yii::t('app', 'Saved'):Yii::t('app', 'Errors'),
                'errors' => $model->errors
            );
            return $res;
        }

        throw new NotFoundHttpException('The requested page does not exist. Please Ajax.');
    }

    /**
     * @param $id
     * @param $type
     * @return null|\yii\rbac\Permission|\yii\rbac\Role|\yii\rbac\Rule
     * @throws NotFoundHttpException
     */
    protected function findAuthEntity($id, $type)
    {
        $auth = Yii::$app->authManager;
        if ($type == null) {
            $entity = $auth->getRule($id);
        } else {
            $entity = ($type == Item::TYPE_ROLE) ? $auth->getRole($id) : $auth->getPermission($id);
        }

        if ($entity) {
            return $entity;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response

    public function actionPermissions($id)
    {
        $modelForm = new AssignmentForm;
        $modelForm->model = $this->findModel($id);

        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->save()) {
            Yii::$app->session->setFlash('success', Yii::t('users', 'SUCCESS_UPDATE_PERMISSIONS'));
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('permissions', [
            'modelForm' => $modelForm
        ]);
    }
     */
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if ($this->_model === false) {
            $this->_model = User::findOne($id);
        }

        if ($this->_model !== null) {
            return $this->_model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
