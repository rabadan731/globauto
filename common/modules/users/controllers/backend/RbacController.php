<?php

namespace modules\users\controllers\backend;

use Yii;
use yii\rbac\Item;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use modules\users\models\AuthItem;
use modules\users\models\AuthRule;
use modules\users\models\search\AuthItemSearch;
use modules\users\models\search\AuthRuleSearch;
use modules\users\models\forms\AssignmentForm;
use yii\web\Response;

/**
 * Class RbacController
 */
class RbacController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['rbacManage'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $rolesSearchModel = new AuthItemSearch();
        $rolesSearchModel->formName = 'RolesSearch';
        $rolesDataProvider = $rolesSearchModel->search(Yii::$app->request->queryParams);

        $rulesSearchModel = new AuthItemSearch();
        $rulesSearchModel->formName = 'PermissionsSearch';
        $rulesDataProvider = $rulesSearchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'rolesDataProvider' => $rolesDataProvider,
            'rolesSearchModel' => $rolesSearchModel,
            'rulesSearchModel' => $rulesSearchModel,
            'rulesDataProvider' => $rulesDataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionIndexRuleFile()
    {
        $rulesSearchModel = new AuthRuleSearch();

        return $this->render('index-rule-file', [
            'rulesSearchModel' => $rulesSearchModel
        ]);
    }

    /**
     * @param null|integer $type
     * @return string|\yii\web\Response
     */
    public function actionCreate($type = null)
    {
        $className = 'modules\\users\\models\\' . $this->getModelName($type);
        /** @var AuthItem $model */
        $model = new $className;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $auth = Yii::$app->authManager;

            if ($type != null) {
                if ($type == Item::TYPE_ROLE) {
                    $entity = $auth->createRole($model->name);
                } else {
                    $entity = $auth->createPermission($model->name);
                }
                $entity->description = $model->description;
                $entity->data = $model->data;
                $entity->ruleName = ($model->rule_name != '') ? $model->rule_name : null;
            } else {
                $entity = new $model->name;
            }

            if ($auth->add($entity)) {
                Yii::$app->session->setFlash('success', Yii::t('users', 'SUCCESS_ADD', [
                    'type' => $this->getModelTypeTitle($type),
                    'name' => $entity->name
                ]));
            } else {
                Yii::$app->session->setFlash('success', Yii::t('users', 'ERROR_ADD', [
                    'type' => $this->getModelTypeTitle($type)
                ]));
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'type' => $type
            ]);
        }
    }

    /**
     * @param $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionAjaxUpdate($id)
    {
        if ($id == "0") {
            $model = new AuthItem();
        } else {
            $model = $this->findModel($id, AuthItem::className());
        }

        if (Yii::$app->request->isAjax) {

            $post = Yii::$app->request->post("AuthItem", null);

            if (is_null($post)) {
                return $this->renderAjax('ajax-update', [
                    'model' => $model
                ]);
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $auth = Yii::$app->authManager;

                if ($id == "0") {
                    $entity = ($model->type == Item::TYPE_ROLE) ? $auth->createRole($id) : $auth->createPermission($id);
                    $entity->name = $model->name;
                    $entity->description = $model->description;
                    $entity->data = empty($model->data)?null:$model->data;
                    $entity->ruleName = ($model->rule_name != '') ? $model->rule_name : null;
                    $status = $auth->add($entity);
                } else {
                    $entity = $this->findAuthEntity($id, $model->type);
                    $entity->name = $model->name;
                    $entity->description = $model->description;
                    $entity->data = empty($model->data)?null:$model->data;
                    $entity->ruleName = ($model->rule_name != '') ? $model->rule_name : null;

                    $status = $auth->update($id, $entity);
                }

                $auth->removeChildren($model);
                foreach (Yii::$app->request->post('child_role', []) as $roleName => $val) {
                    $auth->addChild($model, $this->findAuthEntity($roleName, AuthItem::TYPE_ROLE));
                }
                foreach (Yii::$app->request->post('child_rule', []) as $roleName => $val) {
                    $auth->addChild($model, $this->findAuthEntity($roleName, AuthItem::TYPE_RULES));
                }
            } else {
                $status = false;
            }

            $res = array(
                'success' => $status,
                'statusText' => $status?Yii::t('app', 'Saved'):Yii::t('app', 'Errors'),
                'errors' => $model->errors
            );
            return $res;
        }

        throw new NotFoundHttpException('The requested page does not exist. Please Ajax.');
    }

    /**
     * @param $id
     * @param $type
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id, $type)
    {
        $className = 'modules\\users\\models\\' . $this->getModelName($type);
        $model = $this->findModel($id, $className);
        $auth = Yii::$app->authManager;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $entity = $this->findAuthEntity($id, $type);
            $entity->name = $model->name;
            $entity->description = $model->description;
            $entity->data = $model->data;
            $entity->ruleName = ($model->rule_name != '') ? $model->rule_name : null;

            if ($auth->update($id, $entity)) {
                Yii::$app->session->setFlash('success', Yii::t('users', 'SUCCESS_UPDATE', [
                    'type' => $this->getModelTypeTitle($type), 'name' => $entity->name
                ]));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('users', 'ERROR_UPDATE', [
                    'type' => $this->getModelTypeTitle($type), 'name' => $entity->name
                ]));
            }
            return $this->redirect(['index']);
        } else {
            if ($type == null) {
                $data = unserialize($model->data);
                $model->name = $data::className();
            }
            return $this->render('update', [
                'model' => $model,
                'type' => $type
            ]);
        }
    }

    /**
     * @param $id
     * @param null $type
     * @return \yii\web\Response
     */
    public function actionDelete($id, $type = NULL)
    {
        $entity = $this->findAuthEntity($id, $type);

        if (Yii::$app->authManager->remove($entity)) {
            Yii::$app->session->addFlash('success', Yii::t('users', 'SUCCESS_DELETE', ['type' => $this->getModelTypeTitle($type), 'name' => $entity->name]));
        } else {
            Yii::$app->session->addFlash('error', Yii::t('users', 'ERROR_DELETE', ['type' => $this->getModelTypeTitle($type), 'name' => $entity->name]));
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @param $type
     * @return string|\yii\web\Response
     */
    public function actionChildren($id, $type)
    {
        $modelForm = new AssignmentForm;
        $modelForm->model = $this->findModel($id, 'modules\\users\\models\\AuthItem');
        $modelForm->target = $this->findAuthEntity($id, $type);

        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->save()) {
            return $this->redirect(Yii::$app->request->url);
        }

        return $this->render('children', [
            'modelForm' => $modelForm
        ]);
    }

    /**
     * @param $id
     * @param $modelName string
     * @return AuthRule|AuthItem
     * @throws NotFoundHttpException
     */
    protected function findModel($id, $modelName)
    {
        if (($model = $modelName::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $type
     * @return string
     */
    public function getModelName($type)
    {
        return ($type === null) ? 'AuthRule' : 'AuthItem';
    }

    /**
     * @param null $type
     * @return string
     */
    public function getModelTypeTitle($type = null)
    {
        if ($type == null) {
            return Yii::t('users', 'RULE');
        } else {
            return Yii::t(
                'users',
                ($this->actionParams['type'] == Item::TYPE_PERMISSION) ? 'PERMISSION' : 'ROLE'
            );
        }
    }

    /**
     * @param $id
     * @param $type
     * @return null|\yii\rbac\Permission|\yii\rbac\Role|\yii\rbac\Rule
     * @throws NotFoundHttpException
     */
    protected function findAuthEntity($id, $type)
    {
        $auth = Yii::$app->authManager;
        if ($type == null) {
            $entity = $auth->getRule($id);
        } else {
            $entity = ($type == Item::TYPE_ROLE) ? $auth->getRole($id) : $auth->getPermission($id);
        }

        if ($entity) {
            return $entity;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
