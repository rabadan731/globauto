<?php

namespace modules\users\controllers\frontend;

use Yii;
use yii\helpers\Url;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use modules\users\models\User;
use modules\users\models\UserEmailConfirmToken;
use modules\users\models\forms\ChangeEmailForm;
use modules\users\models\forms\ChangePasswordForm;
use modules\users\models\forms\RetryConfirmEmailForm;
use modules\users\models\forms\LoginForm;
use modules\users\models\forms\PasswordResetRequestForm;
use modules\users\models\forms\ResetPasswordForm;
use modules\users\models\forms\SignupForm;

/**
 * Class UserController
 */
class UserController extends \yii\web\Controller
{
//    /**
//     * @return array
//     */
//    public function actions()
//    {
//        return [
//            'uploadPhoto' => [
//                'class' => 'modules\cropper\actions\UploadAction',
//                'url' => Yii::$app->controller->module->userPhotoUrl,
//                'path' => Yii::$app->controller->module->userPhotoPath,
//            ]
//        ];
//    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render($this->module->getCustomView('login'), [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            if ($user = $model->signup()) {
                if ($user->createEmailConfirmToken() &&
                    $user->sendEmailConfirmationMail(
                        Yii::$app->controller->module->getCustomMailView('confirmNewEmail'),
                        'new_email'
                    )
                ) {
                    Yii::$app->getSession()->setFlash(
                        'success',
                        Yii::t('users', 'CHECK_YOUR_EMAIL_FOR_FURTHER_INSTRUCTION')
                    );
                    $transaction->commit();
                    return $this->redirect(Url::toRoute('/login'));
                } else {
                    Yii::$app->getSession()->setFlash(
                        'error',
                        Yii::t('users', 'CAN_NOT_SEND_EMAIL_FOR_CONFIRMATION')
                    );
                    $transaction->rollBack();
                };
            }
            else {
                Yii::$app->getSession()->setFlash('error', Yii::t('users', 'CAN_NOT_CREATE_NEW_USER'));
                $transaction->rollBack();
            }
        }

        return $this->render($this->module->getCustomView('signup'), [
            'model' => $model,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionRetryConfirmEmail()
    {
        $model = new RetryConfirmEmailForm;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->user->sendEmailConfirmationMail(
                Yii::$app->module->getCustomMailView('confirmNewEmail'),
                'new_email')
            ) {
                Yii::$app->getSession()->setFlash(
                    'success', Yii::t('users', 'CHECK_YOUR_EMAIL_FOR_FURTHER_INSTRUCTION')
                );
                return $this->redirect(Url::toRoute('/user/user/retry-confirm-email'));
            }
        }

        return $this->render($this->module->getCustomView('retryConfirmEmail'), [
            'model' => $model
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash(
                    'success',
                    Yii::t('users', 'CHECK_YOUR_EMAIL_FOR_FURTHER_INSTRUCTION')
                );

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash(
                    'error',
                    Yii::t('users', 'CAN_NOT_SEND_EMAIL_FOR_CONFIRMATION')
                );
            }
        }

        return $this->render($this->module->getCustomView('requestPasswordResetToken'), [
            'model' => $model,
        ]);
    }

    /**
     * @param $token
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('users', 'NEW_PASSWORD_WAS_SAVED'));

            return $this->goHome();
        }

        return $this->render($this->module->getCustomView('resetPassword'), [
            'model' => $model,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionProfile()
    {
        /** @var User $model */
        $model = Yii::$app->user->identity;
        return $this->render($this->module->getCustomView('profile'), [
            'model' => $model,
        ]);
    }


    /**
     * @return string|\yii\web\Response
     */
    public function actionUpdateProfile()
    {
        /** @var User $model */
        $model = Yii::$app->user->identity;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('users', 'CHANGES_WERE_SAVED'));
            return $this->redirect(Url::toRoute('/user/user/profile'));
        }

        return $this->render($this->module->getCustomView('updateProfile'), [
            'model' => $model,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionUpdatePassword()
    {
        /** @var User $model */
        $model = Yii::$app->user->identity;
        $changePasswordForm = new ChangePasswordForm;

        if ($model->password_hash != '') {
            $changePasswordForm->scenario = 'requiredOldPassword';
        }

        if ($changePasswordForm->load(Yii::$app->request->post()) && $changePasswordForm->validate()) {
            $model->setPassword($changePasswordForm->new_password);
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('users', 'NEW_PASSWORD_WAS_SAVED'));
                return $this->redirect(Url::toRoute('/main/default/profile'));
            }
        }

        return $this->render($this->module->getCustomView('updatePassword'), [
            'model' => $model,
            'changePasswordForm' => $changePasswordForm,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionUpdateEmail()
    {
        /** @var User $model */
        $model = Yii::$app->user->identity;
        $changePasswordForm = new ChangePasswordForm;
        $changeEmailForm = new ChangeEmailForm;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('users', 'CHANGES_WERE_SAVED'));

            return $this->redirect(Url::toRoute('/user/user/profile'));
        }

        if ($model->password_hash != '') {
            $changePasswordForm->scenario = 'requiredOldPassword';
        }

        if ($changePasswordForm->load(Yii::$app->request->post()) && $changePasswordForm->validate()) {
            $model->setPassword($changePasswordForm->new_password);
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('users', 'NEW_PASSWORD_WAS_SAVED'));
                return $this->redirect(Url::toRoute('/user/user/profile'));
            }
        }

        if ($changeEmailForm->load(Yii::$app->request->post())
            && $changeEmailForm->validate()
            && $model->setEmail($changeEmailForm->new_email)
        ) {
            Yii::$app->getSession()->setFlash(
                'success',
                Yii::t('users', 'TO_YOURS_EMAILS_WERE_SEND_MESSAGES_WITH_CONFIRMATIONS')
            );
            return $this->redirect(Url::toRoute('/user/user/profile'));
        }

        return $this->render($this->module->getCustomView('updateEmail'), [
            'model' => $model,
            'changePasswordForm' => $changePasswordForm,
            'changeEmailForm' => $changeEmailForm
        ]);
    }

    /**
     * @param $token
     * @return \yii\web\Response
     */
    public function actionConfirmEmail($token)
    {
        $tokenModel = UserEmailConfirmToken::findToken($token);

        if (empty($tokenModel)) {
            Yii::$app->getSession()->setFlash(
                'error',
                Yii::t('users', 'CONFIRMATION_LINK_IS_WRONG')
            );
        } else {
            Yii::$app->getSession()->setFlash(
                'success',
                $tokenModel->confirm($token)
            );
        }

        return $this->redirect(Url::toRoute('/'));
    }
}
