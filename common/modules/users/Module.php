<?php

namespace modules\users;

use Yii;

/**
 * Users module class
 */
class Module extends \yii\base\Module
{
    public $userPhotoUrl = '';

    public $userPhotoPath = '';

    public $customViews = [];

    public $customMailViews = [];

    /**
     * @var bool
     */
    public $isBackend = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->isBackend === true) {
            $this->setViewPath('@modules/users/views/backend');
            $this->setLayoutPath('@backend/views/layouts');
            $this->controllerNamespace = $this->controllerNamespace === null ?
                '\modules\users\controllers\backend' : $this->controllerNamespace;
        } else {
            $this->setViewPath('@modules/users/views/frontend');
            $this->setLayoutPath('@frontend/views/layouts');
            $this->controllerNamespace = $this->controllerNamespace === null ?
                '\modules\users\controllers\frontend' : $this->controllerNamespace;
        }

        self::registerTranslations();
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function registerTranslations()
    {
        $translations = Yii::$app->i18n->translations;
        if (!isset($translations['users']) && !isset($translations['users/*'])) {
            Yii::$app->i18n->translations['users'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@modules/users/messages',
                'forceTranslation' => true,
                'fileMap' => [
                    'users' => 'users.php',
                ]
            ];
        }
        if (!isset($translations['user']) && !isset($translations['user/*'])) {
            Yii::$app->i18n->translations['user'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@modules/users/messages',
                'forceTranslation' => true,
                'fileMap' => [
                    'user' => 'user.php'
                ]
            ];
        }
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getCustomView($default)
    {
        if (isset($this->customViews[$default])) {
            return $this->customViews[$default];
        } else {
            return $default;
        }
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getCustomMailView($default)
    {
        if (isset($this->customMailViews[$default])) {
            return $this->customMailViews[$default];
        } else {
            return '@modules/users/mail/' . $default;
        }
    }
}
