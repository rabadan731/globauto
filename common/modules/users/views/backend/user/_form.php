<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rabadan731\cropper\Widget;
use modules\users\models\User;
use yii\helpers\Url;
use modules\users\models\AuthItem;

/* @var $this yii\web\View */
/* @var $model modules\users\models\User */
/* @var $form yii\widgets\ActiveForm */

$authManager = Yii::$app->getAuthManager();

$dataProviderRoles = new \yii\data\ActiveDataProvider([
    'query' => AuthItem::find()->andWhere(['type' => AuthItem::TYPE_ROLE]),
]);

$dataProviderRules = new \yii\data\ActiveDataProvider([
    'query' => AuthItem::find()->andWhere(['type' => AuthItem::TYPE_RULES]),
]);

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['id' => 'form-ajax']); ?>

    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-1">Общие</a>
            </li>
            <?php if (Yii::$app->user->can("userPermissions")) { ?>
            <li class="">
                <a data-toggle="tab" href="#tab-2">Права доступа</a>
            </li>
            <?php } ?>
        </ul>
        <div class="tab-content">
            <div id="tab-1" class="tab-pane active">
                <div class="panel-body">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>

                    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

                    <?= $form->field($model, 'sex')->dropDownList(User::getSexArray()); ?>

                    <?= $form->field($model, 'status')->dropDownList(User::getStatusArray()); ?>

                    <?= Html::label("Новый пароль", 'user-password'); ?>
                    <?= Html::textInput('password', null, ['class'=>'form-control', 'id'=>'user-password']); ?>
                    <p>Заполните это поле, если хотите изменить пароль</p>
                </div>
            </div>
            <?php if (Yii::$app->user->can("userPermissions")) { ?>
                <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                    <h4>Роли</h4>
                    <?= \yii\grid\GridView::widget([
                        'dataProvider' => $dataProviderRoles,
                        'summary' => false,
                        'columns' => [
                            [
                                'content' => function ($data) use ($authManager, $model) {
                                    return Html::checkbox("child_role[{$data->name}]", $authManager->getAssignment($data->name, $model->id));
                                },
                                'options' => ['style' => 'width:30px'],
                                'contentOptions' => function ($data, $key, $index, $grid) use ($authManager, $model) {
                                    if ($authManager->checkAccess($model->id, $data->name)) {
                                        return ['style' => 'background:#1ab394'];
                                    } else {
                                        return ['style' => 'background: #FF97A1'];
                                    }
                                },
                            ],
                            'description',
                        ]
                    ]);

                    ?>
                    <h4>Права Доступа</h4>
                    <?php

                    echo \yii\grid\GridView::widget([
                        'dataProvider' => $dataProviderRules,
                        'summary' => false,
                        'columns' => [
                            [
                                'content' => function ($data) use ($authManager, $model) {
                                    return Html::checkbox("child_rule[{$data->name}]", $authManager->getAssignment($data->name, $model->id));
                                },
                                'options' => ['style' => 'width:30px'],
                                'contentOptions' => function ($data, $key, $index, $grid) use ($authManager, $model) {
                                    if ($authManager->checkAccess($model->id, $data->name)) {
                                        return ['style' => 'background:#1ab394'];
                                    } else {
                                        return ['style' => 'background: #FF97A1'];
                                    }
                                },
                            ],
                            'description'
                        ]
                    ]);

                    ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>

    <br />

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
