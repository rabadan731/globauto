<?php

use yii\helpers\Html;
use yii\grid\GridView;
use modules\users\models\User;
use common\assets\AjaxFormAsset;

AjaxFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('users', 'USERS');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <div class="row">
        <div class="col-md-6">
            <p>
                <?= Html::a(Yii::t('users', 'CREATE'), "javascript:void(0);", [
                    'class' => 'btn btn-success',
                    'data-pjax' => 0,
                    'data-url' => \yii\helpers\Url::to(['ajax-update', 'id' => 0]),
                    'onclick' => '
                        $.get($(this).data("url"), function(data, status){
                            $("#editBlock").html(data);
                            $("body,html").animate({scrollTop: 186}, 300);
                        });'
                ]); ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    'id',
                    'username',
                    'email:email',
                    [
                        'attribute' => 'sex',
                        'value' => function ($data) {
                            return User::getSexArray()[$data->sex];
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'value' => function ($data) {
                            return User::getStatusArray()[$data->status];
                        }
                    ],
                    'created_at:datetime',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{ajax-update} {delete}',
                        'options' => ['style' => 'width:70px'],
                        'buttons' => [
                            'ajax-update' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-edit"></i>', "javascript:void(0);", [
                                    'class' => 'btn btn-xs btn-primary',
                                    'title' => 'Изменить',
                                    'data-pjax' => 0,
                                    'data-url' => $url,
                                    'onclick' => '
                            $.get($(this).data("url"), function(data, status){
                                $("#editBlock").html(data);
                                $("body,html").animate({scrollTop: 186}, 300);
                            });
                        '
                                ]);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-trash-o"></i>', $url, [
                                    'class' => 'btn btn-xs btn-danger', 'data-method' => 'post',
                                    'title' => 'Удалить',
                                    'data' => [
                                        'pjax' => 0,
                                        'confirm' => 'Вы уверены, что хотите удалить этот элемент?'
                                    ],
                                ]);
                            }
                        ]
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <div class="white-bg padding-10" id="editBlock">

            </div>
        </div>
    </div>
</div>