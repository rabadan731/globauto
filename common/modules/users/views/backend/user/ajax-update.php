<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \modules\users\models\User */

$this->title = "{$model->username} <span>[{$model->email}]</span>";

?>
<div class="geo-cities-update">

    <h3><?= $this->title; ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
