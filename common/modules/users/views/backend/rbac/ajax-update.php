<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\users\models\AuthRule */

?>
<div class="auth-rule-update">
    <?= $this->render('_formAuthItem', [
        'model' => $model,
        'type' => $type
    ]) ?>
</div>
