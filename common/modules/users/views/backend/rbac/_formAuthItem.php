<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\users\models\AuthRule;
use modules\users\models\AuthItem;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model modules\users\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
/* @var $type string */

$authManager = Yii::$app->getAuthManager();

$dataProviderRoles = new \yii\data\ActiveDataProvider([
    'query' => AuthItem::find()->andWhere(['type' => AuthItem::TYPE_ROLE])->andWhere(['!=', 'name', $model->name]),
]);

$dataProviderRules = new \yii\data\ActiveDataProvider([
    'query' => AuthItem::find()->andWhere(['type' => AuthItem::TYPE_RULES])->andWhere(['!=', 'name', $model->name]),
]);

?>

<div class="auth-item-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-ajax'
    ]); ?>

    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-1">Общие</a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#tab-2">Права доступа</a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="tab-1" class="tab-pane active">
                <div class="panel-body">
                    <?= $form->field($model, 'name')->textInput([
                        'maxlength' => true,
                        'readonly' => !$model->isNewRecord
                    ]) ?>

                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

                    <?php /* $form->field($model, 'data')->textarea(['rows' => 6]) */ ?>

                    <?= $form->field($model, 'rule_name')->dropDownList(ArrayHelper::map(AuthRule::find()->all(), 'name', 'name'), ['prompt' => Yii::t('users', 'SELECT...')]) ?>

                    <?= $form->field($model, 'type')->dropDownList([
                        AuthItem::TYPE_ROLE => "Роль", AuthItem::TYPE_RULES => "Правило"
                    ], ['prompt' => '-- select --']) ?>
                </div>
            </div>
            <div id="tab-2" class="tab-pane">
                <div class="panel-body">

                    <?php if ($model->type == AuthItem::TYPE_ROLE) { ?>

                        <h4>Роли</h4>
                        <?= \yii\grid\GridView::widget([
                            'dataProvider' => $dataProviderRoles,
                            'summary' => false,
                            'columns' => [
                                [
                                    'content' => function ($data) use ($authManager, $model) {
                                        return Html::checkbox(
                                            "child_role[{$data->name}]",
                                            $authManager->hasChild($model, $data)
                                        );
                                    },
                                    'options' => ['style' => 'width:30px'],
                                    'contentOptions' => function ($data, $key, $index, $grid) use ($authManager, $model) {
                                        if ($authManager->hasChild($model, $data)) {
                                            return ['style' => 'background:#1ab394'];
                                        } else {
                                            return ['style' => 'background: #FF97A1'];
                                        }
                                    },
                                ],
                                'description',
                            ]
                        ]); ?>
                    <?php } ?>

                    <h4>Права Доступа</h4>
                    <?php

                    echo \yii\grid\GridView::widget([
                        'dataProvider' => $dataProviderRules,
                        'summary' => false,
                        'columns' => [
                            [
                                'content' => function ($data) use ($authManager, $model) {
                                    return Html::checkbox("child_rule[{$data->name}]", $authManager->hasChild($model, $data));
                                },
                                'options' => ['style' => 'width:30px'],
                                'contentOptions' => function ($data, $key, $index, $grid) use ($authManager, $model) {
                                    if ($authManager->hasChild($model, $data)) {
                                        return ['style' => 'background:#1ab394'];
                                    } else {
                                        return ['style' => 'background: #FF97A1'];
                                    }
                                },
                            ],
                            'description'
                        ]
                    ]);

                    ?>
                </div>
            </div>
        </div>
    </div>

    <br/>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
