<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\assets\AjaxFormAsset;

AjaxFormAsset::register($this);
$this->params['breadcrumbs'][] = Yii::t("users", "RBAC");
$this->title = Yii::t("user", "User roles");
$this->params['breadcrumbs'][] = $this->title;

$buttons = [
    'ajax-update' => function ($url, $model, $key) {
        return Html::a('<i class="fa fa-edit"></i>', "javascript:void(0);", [
            'class' => 'btn btn-xs btn-primary',
            'title' => 'Изменить',
            'data-pjax' => 0,
            'data-url' => $url,
            'onclick' => '
                            $.get($(this).data("url"), function(data, status){
                                $("#editBlock").html(data);
                                $("body,html").animate({scrollTop: 186}, 300);
                            });
                        '
        ]);
    },
    'delete' => function ($url, $model, $key) {
        return Html::a('<i class="fa fa-trash-o"></i>', ("{$url}&type={$model->type}"), [
            'class' => 'btn btn-xs btn-danger', 'data-method' => 'post',
            'title' => 'Удалить',
            'data' => [
                'pjax' => 0,
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?'
            ],
        ]);
    }
];
?>
<div class="row">
    <div class="col-md-6">
        <?= Html::a(Yii::t('users', 'CREATE'), "javascript:void(0);", [
            'class' => 'btn btn-success',
            'data-pjax' => 0,
            'data-url' => Url::to(['ajax-update', 'id' => 0]),
            'onclick' => '
                $.get($(this).data("url"), function(data, status){
                    $("#editBlock").html(data);
                    $("body,html").animate({scrollTop: 186}, 300);
                });'
        ]); ?>
        <?= GridView::widget([
            'dataProvider' => $rolesDataProvider,
        //    'filterModel' => $rolesSearchModel,
            'columns' => [
                [
                    'attribute' => 'name',
                    'options' => ['style' => 'width:30%'],
                ],
                'description',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{ajax-update} {delete}',
                    'options' => ['style' => 'width:70px'],
                    'buttons' => $buttons,
                ]
            ],
        ]); ?>

        <?= GridView::widget([
            'dataProvider' => $rulesDataProvider,
    //        'filterModel' => $rulesSearchModel,
            'columns' => [
                [
                    'attribute' => 'name',
                    'options' => ['style' => 'width:30%'],
                ],
                'description',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{ajax-update} {delete}',
                    'options' => ['style' => 'width:70px'],
                    'buttons' => $buttons,
                ]
            ],
        ]); ?>
    </div>
    <div class="col-md-6">
        <div class="white-bg padding-10" id="editBlock">

        </div>
    </div>
</div>