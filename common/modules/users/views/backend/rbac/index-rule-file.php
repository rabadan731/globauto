<?php

use yii\helpers\Url;
use yii\grid\GridView;
use yii\rbac\Item;
use yii\helpers\Html;

$this->title = Yii::t("user", "Rules file");
$this->params['breadcrumbs'][] = Yii::t("users", "RBAC");
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-6">
        <?= GridView::widget([
            'dataProvider' => $rulesSearchModel->search(Yii::$app->request->queryParams),
            'columns' => [
                'name',
                [
                    'header' => Yii::t('users', 'PHP_CLASS'),
                    'value' => function($data) {
                        $dataUns = unserialize($data->data);
                        return $dataUns->className();
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                ],
            ],
        ]); ?>

        <?= $this->render('_formAuthRule', [
            'model' => new \modules\users\models\AuthRule(),
        ]) ?>
    </div>
</div>