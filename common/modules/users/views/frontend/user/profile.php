<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \modules\users\models\User */

$this->title = Yii::t('users', 'PROFILE');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-profile">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'email',
            'phone',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]); ?>
    <?=Html::a(Yii::t('users', 'UPDATE'), ['update-profile'], ['class' => 'btn btn-primary']); ?>
</div>
