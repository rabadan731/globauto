<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use modules\users\models\User;
use modules\users\components\AuthKeysManager;
use modules\users\assets\UsersAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \modules\users\models\User */
/* @var $changePasswordForm \modules\users\models\forms\ChangePasswordForm */

$this->title = Yii::t('users', 'UPDATE');
$this->params['breadcrumbs'][] = Yii::t('users', 'PROFILE');
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['profile']];
$this->params['breadcrumbs'][] = $this->title;
UsersAsset::register($this);
?>
<div class="container">
<div class="site-profile">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="panel panel-default">
        <div class="panel-heading"><?= Yii::t('users', 'CHANGE_PASSWORD') ?></div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(['id' => 'form-password']); ?>
            <?php if ($model->password_hash != '') : ?>
                <?= $form->field($changePasswordForm, 'old_password')->passwordInput(); ?>
            <?php endif; ?>
            <?= $form->field($changePasswordForm, 'new_password')->passwordInput(); ?>
            <?= $form->field($changePasswordForm, 'new_password_repeat')->passwordInput(); ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('users', 'SAVE'), ['class' => 'btn btn-primary', 'name' => 'password-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</div>
