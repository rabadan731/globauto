<?php

namespace modules\users\components;

use modules\users\models\forms\LoginForm;
use modules\users\Module;
use yii\base\Widget;

/**
 * The Authorization Widget.
 */
class AuthorizationWidget extends Widget
{
    /**
     * Executes the widget.
     * @return string
     */
    public function run()
    {
        $model = new LoginForm;
        Module::registerTranslations();
        return $this->render('authorizationWidget', ['model' => $model]);
    }
}