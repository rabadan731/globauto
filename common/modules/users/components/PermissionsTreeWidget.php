<?php

namespace modules\users\components;

use modules\users\models\AuthItem;
use modules\users\models\User;
use yii\base\Widget;
use Yii;

class PermissionsTreeWidget extends Widget
{
    /** @var User */
    public $user = false;

    /** @var AuthItem */
    public $item = false;

    public function run()
    {
        if ($this->user) {
            $assignedPermissions = $this->user->assignedRules;
            $defaultPermissions = AuthItem::find()
                ->where(['in', 'name', Yii::$app->authManager->defaultRoles])
                ->all();
            $permissions = array_merge($assignedPermissions, $defaultPermissions);
        } else {
            $permissions = $this->item->children;
        }

        return $this->render('permissionsTreeWidget', ['permissions' => $permissions]);
    }
}