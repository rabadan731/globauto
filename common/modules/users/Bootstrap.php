<?php

namespace modules\users;

use yii\base\BootstrapInterface;
use Yii;
use yii\base\Exception;

/**
 * Class Bootstrap
 * @package modules\users
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $rules = [];

        $rules['/login'] = '/user/user/login';
        $rules['/logout'] = '/user/user/logout';

        if (Yii::$app->id == 'app-backend') {
            $rules['/user/admin']           = 'user/user/admin';
//            $rules['/user/create']          = 'user/user/create';
//            $rules['/user/update/<id:\d+>'] = 'user/user/update';
//            $rules['/user/delete/<id:\d+>'] = 'user/user/delete';
//            $rules['/user/<id:\d+>'] = 'user/users/view';
        }

        http://glauto.ru/user/confirmEmail?token=3DvIPv0XhKIt6xAD9qad1NzurDZWUIwoyv
        if (Yii::$app->id == 'app-frontend') {
            $rules['/signup'] = '/user/user/signup';
            $rules['/user/request-password-reset'] = '/user/user/request-password-reset';
            $rules['/user/reset-password'] = '/user/user/reset-password';
          //  $rules['/user/profile'] = '/user/user/profile';
            $rules['/user/update-profile'] = '/user/user/update-profile';
            $rules['/user/update-password'] = '/user/user/update-password';
            $rules['/user/update-email'] = '/user/user/update-email';
            $rules['/user/profile-update'] = '/user/user/profile-update';
            $rules['/user/retry-confirm-email'] = '/user/user/retry-confirm-email';
            $rules['/user/confirm-email/<token>'] = '/user/user/confirm-email';
            $rules['/unbind/<id:[\w\-]+>'] = '/user/auth/unbind';
            $rules['/oauth/<authclient:[\w\-]+>'] = '/user/auth/index';
        }

        $app->urlManager->addRules($rules, false);
    }
}
